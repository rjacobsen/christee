/* 
 * File:   initialize.h
 * Author: Dana
 *
 * Created on August 27, 2016, 5:07 PM
 */

#ifndef INITIALIZE_H
#define	INITIALIZE_H

#define SYS_FREQ 80000000 // system frequency
#define CAMERA_ADDRESS 3

#ifdef	__cplusplus
extern "C" {
#endif
    int receiveArray[20];


void timers(void);
void UART(void);
void initializeBits(void);
void initializeCheck(void);
void initializeWii(void);
char getWiiInitStep();




#ifdef	__cplusplus
}
#endif

#endif	/* INITIALIZE_H */

