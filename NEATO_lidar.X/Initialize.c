#include <xc.h>
#include <sys/attribs.h>
#include "queue.h"
#include "FastTransfer.h"
#include "uart_handler.h"
#include "lidar_decoder.h"
#include "Initialize.h"
#include <stddef.h>


void initialize(void){
//     SYSTEMConfig(80000000, SYS_CFG_ALL); // sets up periferal and clock configuration
    // INTEnableSystemMultiVectoredInt();
    IOpins();
    //delay();
   
    //unsigned int temp_CP0;
    //asm volatile("di");
    //asm volatile("ehb");
    
   // _CP0_SET_EBASE(0x9FC01000);
    //OFF0 = 0xfe;
    
//    temp_CP0 = _CP0_GET_CAUSE();
//    temp_CP0 |= 0x00800000;
//    _CP0_SET_CAUSE(temp_CP0);
//    
   // INTCONSET = _INTCON_MVEC_MASK;
     
   
    timers(); 
   // asm volatile("ei");
     //delay();
    // PWM();
     //delay();
    // UART();
    
     //void begin(int * ptr, unsigned char maxSize, unsigned char givenAddress, bool error, void (*stufftosend)(unsigned char), unsigned char (*stufftoreceive)(void),int (*stuffavailable)(void), unsigned char (*stuffpeek)(void))
     ///begin(&receiveArray, sizeof(receiveArray), 1, false, Send_put, Receive_get, Receive_available, Receive_peek);
      //delay();
//     DMA();
//     delay();
    //IEC0bits.CTIE = 1;
   // IEC0bits.CS0IE = 1;
   // IEC0bits.CS1IE = 1;
   
    
    //SYS_INT_Enable();
    //EnableInterrupts;
    //INTEnableSystemMultiVectoredInt(); //enables interrupts have to do this after timers so that they don't interrupt the initialization

   // INTEnableInterrupts(); // enable interrupts have to do this after timers so that they don't interrupt the initialization
       // Initializing Global Interrupts
    //INTCON
    // 16 Single Vector is not presented with a shadow register set
    //INTCONbits.SS0 = 0;
    /*
    // 12 Interrupt Controller configured for multi vectored mode
    
    // 10 - 8 Disables interrupt proximity timer
    INTCONbits.TPC = 0;

     */
    //INTCONbits.MVEC = 1;
   // INTCONCLR = _INTCON_MVEC_MASK;  //Clear the MVEC bit
    //__asm__("EI");
    //asm volatile("ei");
    //__builtin_disable_interrupts();
   
     //beginLIDARdecoder(returned_data, &buffer_five);
}


void IOpins(void) {
//    DDPCONbits.JTAGEN = 0;  // This turns off the JTAG and allows PORTA pins to be used (Must disable the JTAG module in order to use LAT registers to set discrete outputs (LEDs)
    ANSELE= 0xFFEF;         // set analog config pin
    ANSELB = 0xFFC3;
        
    TRISBbits.TRISB2 = 0;   // set output debugging LEDs
    TRISBbits.TRISB3 = 0;  // set output debugging LEDs
    TRISBbits.TRISB4 = 0;  // set output debugging LEDs
    TRISBbits.TRISB5 = 0;  // set output debugging LEDs
    
    TRISEbits.TRISE1 = 0;   // set output debugging LEDs for PIC32
    TRISEbits.TRISE2 = 0;   // set output debugging LEDs for PIC32 
    TRISEbits.TRISE3 = 0;   // set output debugging LEDs for PIC32
    TRISEbits.TRISE4 = 0;   // set output debugging LEDs for PIC32
    
    TRISDbits.TRISD5 = 1;   // U4RX FROM LANTRONIX
    TRISDbits.TRISD4 = 0;   // U4TX TO LANTRONIX
    U4RXR = 0b0110;         // Re-mapping the UART 4 RX to pin 53
    RPD4R = 0b0010;         // Re-mapping the UART 4 TX to pin 52
    
    TRISCbits.TRISC14 = 1;  // U1RX FROM LANTRONIX
    TRISCbits.TRISC13 = 0;  // U1RX TO LANTRONIX
    RPC13R = 0b0001;        //Re-Mapping the UART 1 TX to pin 47
    U1RXR = 0b0111;         //Re-Mapping the UART 1 RX to pin 48
    
    TRISDbits.TRISD0 = 1;   // U6RX FROM THE LIDAR MODULE
   // TRISDbits.TRISD11 = 0;  // U6TX TO THE LIDAR MODULE   As Far as I know we shouldn't need to sent anything to the lidar module
    U6RXR = 0b0011;
    
    TRISGbits.TRISG8 = 0;   // Setting the pin 10 to an out put for the Lidar motor
    RPG9R = 0b1100;         // Setting the pin 10 to the output compare module 1 (See Function PWM())
    

 
}



//100ms timer (PR = 31250, prescaler = 256)
void timers(void){
 
    T3CONbits.ON = 0;
    T3CON = 0x0;
    TMR3 = 0x0;
//    T3CONbits.TCS = 0; // peripheral clock as source
    T3CONbits.TCKPS = 0b111; //256 prescalar
//    T3CONbits.TCKPS = 0b110; //64 prescalar
   // T3CONbits.TCKPS = 0b001; //2 prescalar
    T3CONbits.TCS = 0;      //Timer source select bit set to internal peripheral clock
    T3CONbits.TGATE = 0;     //Gated time accumulation is disabled
    //TPB_clock is the clock resource for peripherals on pic32MX (p.201) (look at system clock / "FPBDIV") - located in configuration bits
    //Example:
    //Desired_Period == 100ms
    //PR# = Desired_Period / [(1/Fosc)*Prescaler]
    //PR# = 0.100seconds / [(1/80MHz)*256] = 31250
    PR3 = 46980;
    
    IFS0bits.T3IF = 0;      //Clear interrupt Flag
    IPC3bits.T3IP = 0b0011;
   
   // IFS0CLR = 0x0; 
    //IPC3 = 0x1F0000; //((0x1 << _IPC3_T3IP_POSITION) | (0x1 << _IPC3_T3IS_POSITION));
   
   // IEC0bits.T3IE = 1;      //Enable Interrupt
    //IEC0 = 0x50000;
    
    //IEC0bits.
   // T3CONbits.ON = 1;
    
    
}



//Control the speed the lidar rotates (alter speed to achieve around 200rpm)
//The PWM uses Output Compare 1 (pin 46) using timer 2 (timer 1 is different than other timers)
//Fosc==80MHz
void PWM(void){
    CFGCONbits.OCACLK = 1;   //The timer sources configurations See page 310 of the data sheet
    
    /******************LIDAR PWM FOR MOTOR**********************/
    CFGCONbits.OCACLK = 1;                        
    T4CONbits.ON = 0;
    T4CONbits.T32 = 0; //16 bit mode
    T4CONbits.TCKPS = 0b001; //prescalar of 2
    T4CONbits.TGATE = 0;
    //PR# = Desired_Period / [(1/Fosc)*Prescaler]
    //PR# = (1/10.5kHz) / [(1/80MHz)*2] = 3800
    PR4 = 3800; //around 10.5kHz frequency
    OC1CONbits.ON = 0;  //Output Compare Peripheral on bit
    OC1CONbits.OCM = 0b110; //PWM mode on OCx Fault pin disabled
    OC1CONbits.OCTSEL = 0; //select Timer 4 as source for OC1 (PWM) (Timerx OCTSEL = 0 & Timery OCTSEL = 1)(see page 310 of data sheet)
    OC1R = 2100; // near 230rpm //when PWM Drops low
    OC1RS = 2100; // near 230rpm    //is the next value that will be loaded into OCxR once the pulse is complete
//    OC1R = 2300; // near 230rpm
//    OC1RS = 2300; // near 230rpm
//    OC1R = 2450; // near 260rpm
//    OC1RS = 2450; // near 260rpm
    OC1CONbits.ON = 1;
    T2CONbits.ON = 1;
    /*******************WII CAMERA PWM FOR SERVOS**************8*/
}



void UART(void){
    // uart 6 receive lidar Data (only need RX wire)
    //disable analog part of the pins
    U6MODEbits.BRGH = 0; // set to standard speed mode
    U6BRG = 42; // 115200 baud  //85; // 57600 baud
    U6MODEbits.PDSEL = 0b00; // 8-bit no parity
    U6MODEbits.STSEL = 0; // 1 stop bit
    IEC5bits.U6RXIE = 1; // enable uart recieve
    IPC47bits.U6RXIP = 1; // priority 1
    IPC47bits.U6RXIS = 2; // sub priority 2
    U6STAbits.URXEN = 1; // enable uart recieve
    U6STAbits.URXISEL = 0b00; // interrupt generated with every reception
    U6STAbits.UTXEN = 0; // enable uart transmit
    U6MODEbits.ON = 1; // enable whole uart module
    // uart 6 error
    IEC5bits.U6EIE = 1; // error interrupt enabled


    // uart 6 debug TX to computer terminal window (only need TX wire)
    U6MODEbits.BRGH = 0; // set to standard speed mode
    //TODO: check the baud rate here
    U6BRG = 21; //21;// 230400 baud   //42;// 115200 baud  //85;// 57600 baud
    U6MODEbits.PDSEL = 0b00; // 8-bit no parity
    U6MODEbits.STSEL = 0; // 1 stop bit
    IFS5bits.U6TXIF = 0;
    IEC5bits.U6TXIE = 1; // transmit interrupt enable
    IPC47bits.U6TXIP = 1; // priority 1
    IPC47bits.U6TXIS = 1; // sub priority 1
    U1STAbits.UTXISEL = 0b01; // interrupt when transmit complete
    //U1STAbits.URXISEL = 0; // interrupt generated with every reception
    U6STAbits.URXEN = 0; // DISABLE uart recieve
    U6STAbits.UTXEN = 1; // enable uart transmit
    U6MODEbits.ON = 1; // enable whole uart moduleIEC2bits.U6TXIE = 1; // transmit interrupt enable
    // uart 6 error
//    IEC0bits.U1EIE = 1; // error interrupt enabed
    //ring_buff_init(&output_buffer);
}


void DMA(void) {
    DMACONbits.ON = 1; // dma module enabled
    DCRCCON = 0; // crc module disabled

    //dma 1 for U6TX debugging
    DCH1CONbits.CHPRI = 2; // channel priority 2
    DCH1ECONbits.CHSIRQ = 72; // uart 6 tx Interrupt "IRQ" Vector Number (p.134)
    DCH1ECONbits.SIRQEN = 1; // enable cell transfer when IRQ triggered
    DCH1INT = 0; // clear all existing flags, disable all interrupts
    //DCH1SSA = (unsigned int) &dma_one_array & 0x1FFFFFFF; // physical address conversion for transmit buffer
    DCH1DSA = (unsigned int) &U6TXREG & 0x1FFFFFFF; // physical address conversion for uart send buffer
    //DCH1SSIZ=3000; // source size at most 3000 bytes
    DCH1DSIZ = 1; // dst size is 1 byte
    DCH1CSIZ = 1; // one byte per UART transfer request
    
    //DMA 1 settings
    arrayOFdmaSetting[1].dma_array = (unsigned char*) &dma_one_array;
    arrayOFdmaSetting[1].dmacon = &DCH1CON;
    arrayOFdmaSetting[1].con_busy_mask = _DCH1CON_CHBUSY_MASK;
    arrayOFdmaSetting[1].con_en_mask = _DCH1CON_CHEN_MASK;
    arrayOFdmaSetting[1].dmasize = &DCH1SSIZ;
    arrayOFdmaSetting[1].dmaecon = &DCH1ECON;
    arrayOFdmaSetting[1].econ_force_mask = _DCH1ECON_CFORCE_MASK;

    //enable the interrupt for the dma 1 after each cell transfer
    IFS4bits.DMA1IF = 0;
    IPC33bits.DMA1IP = 2;
    IPC33bits.DMA1IS = 2; 
    IEC4bits.DMA1IE = 1;
    
    //After loading the DMA settings, start using the DMA
    //queue_begin(arrayOFdmaSetting, 1);

}
