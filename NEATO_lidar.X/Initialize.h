/* 
 * File:   Initialize.h
 * Author: Seth Carpenter
 *
 * Created on October 25, 2016, 4:29 PM
 */

#ifndef INITIALIZE_H
#define	INITIALIZE_H

#define WII_INDICATOR_0 LATEbits.LATE1
#define WII_INDICATOR_1 LATEbits.LATE2
#define WII_INDICATOR_2 LATEbits.LATE3
#define WII_INDICATOR_3 LATEbits.LATE4

#define WII_INDICATOR_ON 0
#define WII_INDICATOR_OFF 1

#define LIDAR_INDICATOR_0 LATBbits.LATB2
#define LIDAR_INDICATOR_1 LATBbits.LATB3
#define LIDAR_INDICATOR_2 LATBbits.LATB4
#define LIDAR_INDICATOR_3 LATBbits.LATB5

#define LIDAR_INDICATOR_ON 1
#define LIDAR_INDICATOR_OFF 0

int receiveArray[20];
unsigned short returned_data[10];
void initialize(void);
void IOpins(void);
void timers(void);
void PWM(void);
void UART(void);
void DMA(void);


#endif	/* INITIALIZE_H */

