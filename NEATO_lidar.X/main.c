 /* File:   main.c
 * Author: Seth Carpenter
 * Created on July 3, 2015, 1:11 PM
 */



#include <xc.h>

#include <sys/attribs.h>

#include <stdbool.h>
#include <stdio.h>
#include <math.h>
#include <string.h>
#include "main.h"
#include "uart_handler.h"
#include "FastTransfer.h"
#include "lidar_decoder.h"
#include "Initialize.h"

// PIC32MZ2048EFH064 Configuration Bit Settings

// 'C' source line config statements

#include <xc.h>

// DEVCFG3
// USERID = No Setting
#pragma config FMIIEN = OFF              // Ethernet RMII/MII Enable (MII Enabled)
#pragma config FETHIO = OFF              // Ethernet I/O Pin Select (Default Ethernet I/O)
#pragma config PGL1WAY = OFF             // Permission Group Lock One Way Configuration (Allow only one reconfiguration)
#pragma config PMDL1WAY = OFF            // Peripheral Module Disable Configuration (Allow only one reconfiguration)
#pragma config IOL1WAY = ON             // Peripheral Pin Select Configuration (Allow only one reconfiguration)
#pragma config FUSBIDIO = OFF            // USB USBID Selection (Controlled by the USB Module)

// DEVCFG2
#pragma config FPLLIDIV = DIV_1         // System PLL Input Divider (1x Divider)
#pragma config FPLLRNG = RANGE_5_10_MHZ   // System PLL Input Range (21-42 MHz Input)
#pragma config FPLLICLK = PLL_POSC      // System PLL Input Clock Selection (POSC is input to the System PLL)
#pragma config FPLLMULT = MUL_4         // System PLL Multiplier (PLL Multiply by 21)
#pragma config FPLLODIV = DIV_2         // System PLL Output Clock Divider (2x Divider)
#pragma config UPLLFSEL = FREQ_24MHZ    // USB PLL Input Frequency Selection (USB PLL input is 24 MHz)

// DEVCFG1
#pragma config FNOSC = SPLL             // System PLL
#pragma config DMTINTV = WIN_127_128    // DMT Count Window Interval (Window/Interval value is 127/128 counter value)
#pragma config FSOSCEN = OFF            // Secondary Oscillator Enable (Enable SOSC)
#pragma config IESO = ON                // Internal/External Switch Over (Enabled)
#pragma config POSCMOD = EC             // Primary Oscillator Configuration (HS osc mode)
#pragma config OSCIOFNC = OFF            // CLKO Output Signal Active on the OSCO Pin (Enabled)
#pragma config FCKSM = CSECME           // Clock Switching and Monitor Selection (Clock Switch Enabled, FSCM Enabled)
#pragma config WDTPS = PS1048576        // Watchdog Timer Postscaler (1:1048576)
#pragma config WDTSPGM = STOP           // Watchdog Timer Stop During Flash Programming (WDT stops during Flash programming)
#pragma config WINDIS = NORMAL          // Watchdog Timer Window Mode (Watchdog Timer is in non-Window mode)
#pragma config FWDTEN = OFF             // Watchdog Timer Enable (WDT Disabled)
#pragma config FWDTWINSZ = WINSZ_25     // Watchdog Timer Window Size (Window size is 25%)
#pragma config DMTCNT = DMT31           // Deadman Timer Count Selection (2^31 (2147483648))
#pragma config FDMTEN = ON              // Deadman Timer Enable (Deadman Timer is enabled)

// DEVCFG0
#pragma config DEBUG = OFF              // Background Debugger Enable (Debugger is disabled)
#pragma config JTAGEN = OFF             // JTAG Enable (JTAG Disabled)
#pragma config ICESEL = ICS_PGx1        // ICE/ICD Comm Channel Select (Communicate on PGEC1/PGED1)
#pragma config TRCEN = ON               // Trace Enable (Trace features in the CPU are enabled)
#pragma config BOOTISA = MIPS32         // Boot ISA Selection (Boot code and Exception code is MIPS32)
#pragma config FECCCON = OFF_UNLOCKED   // Dynamic Flash ECC Configuration (ECC and Dynamic ECC are disabled (ECCCON bits are writable))
#pragma config FSLEEP = OFF             // Flash Sleep Mode (Flash is powered down when the device is in Sleep mode)
#pragma config DBGPER = ALLOW_PG2       // Debug Mode CPU Access Permission (Allow CPU access to Permission Group 2 permission regions)
#pragma config SMCLR = MCLR_NORM        // Soft Master Clear Enable bit (MCLR pin generates a normal system Reset)
#pragma config SOSCGAIN = GAIN_2X       // Secondary Oscillator Gain Control bits (2x gain setting)
#pragma config SOSCBOOST = ON           // Secondary Oscillator Boost Kick Start Enable bit (Boost the kick start of the oscillator)
#pragma config POSCGAIN = GAIN_2X       // Primary Oscillator Gain Control bits (2x gain setting)
#pragma config POSCBOOST = ON           // Primary Oscillator Boost Kick Start Enable bit (Boost the kick start of the oscillator)
#pragma config EJTAGBEN = NORMAL        // EJTAG Boot (Normal EJTAG functionality)

// DEVCP0
#pragma config CP = OFF                 // Code Protect (Protection Disabled)

// SEQ0

// DEVSN0

// DEVSN1              // Code Protect (Protection Disabled)





 
void main()
{
    initialize();
    PRISS = 0x76543210;                /* assign shadow set #7-#1 to priority level #7-#1 ISRs */
 
    INTCONSET = _INTCON_MVEC_MASK;    /* Configure Interrupt Controller for multi-vector mode */
 
    __builtin_enable_interrupts();    /* Set the CP0 Status register IE bit high to globally enable interrupts */
 
    T3CONbits.ON = 1;  
    
    WII_INDICATOR_0 = WII_INDICATOR_OFF;
    WII_INDICATOR_1 = WII_INDICATOR_OFF;
    WII_INDICATOR_2 = WII_INDICATOR_OFF;
    WII_INDICATOR_3 = WII_INDICATOR_OFF;
    LIDAR_INDICATOR_0 = LIDAR_INDICATOR_OFF;
    LIDAR_INDICATOR_1 = LIDAR_INDICATOR_OFF;
    LIDAR_INDICATOR_2 = LIDAR_INDICATOR_OFF;
    LIDAR_INDICATOR_3 = LIDAR_INDICATOR_OFF;
    WII_INDICATOR_0 = WII_INDICATOR_ON;
    int i = 0;
    for(i = 0; i < 600000;i++)
    {
        WII_INDICATOR_0 = WII_INDICATOR_ON;
    }
    WII_INDICATOR_0 = WII_INDICATOR_OFF;
    
    while(1)
    {  
        if(IFS0bits.T3IF == 0)
        {
           IFS0bits.T3IF = 1;
        }
     
        
        
       
    }
   
}
void __ISR(_TIMER_3_VECTOR, IPL3SRS)Timer3Handler(void)
{
    LIDAR_INDICATOR_3 ^= 1; 
   
        
        
    IFS0bits.T3IF = 0;      //clears interrupt Flag
      //  IFS0CLR = 0x01 << 14;
}