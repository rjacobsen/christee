/* 
 * File:   main.h
 * Author: John
 *
 * Created on October 25, 2016, 4:15 PM
 */

#ifndef MAIN_H
#define	MAIN_H

//#include "interrupt_handler.h"

//Mine
#define L_INDICATOR_1 0
#define L_INDICATOR_2 0
#define L_INDICATOR_3 0
#define L_INDICATOR_4 0

#define L_INDICATOR_ON 0
#define L_INDICATOR_OFF 1

//Yours
#define W_INDICATOR_1 0
#define W_INDICATOR_2 0
#define W_INDICATOR_3 0
#define W_INDICATOR_4 0

#define W_INDICATOR_ON 1
#define W_INDICATOR_OFF 0


//this variable is referenced in the Initialize.h to pass it to the function
//beginLIDARdecoder(unsigned short * _output_data, struct ringBuff* _input_data) 
//where _output_data is now a pointer to returned_data
unsigned short returned_data[10];

bool FirstTimePrinting = true;
unsigned int DATA[4];
unsigned int i = 0;
int receiveArray[20];
void delay(void);
unsigned int v = 0;
void calculate_sinLookup();

#endif	/* MAIN_H */

