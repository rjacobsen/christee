

#ifndef UART_OPERATOR_H
#define	UART_OPERATOR_H


#define Transmition_Buffer_Size 200
#define SPACE               32
#define CARRIAGE_RETURN     13
#define NEGATION            45
#define BAUD_RATE ((60000000/(115200 * 16)) - 1)

struct UART_ring_buff {
    unsigned char buf[Transmition_Buffer_Size];
    int head;
    int tail;
    int count;
};



void UartInit(void);
void MessageParsser(int,int,int);
int absol(int); 
void maxedHead();
void isNegative(int *);
void loadHundreds(int *);
void loadTens(int *);
void loadUnits(int *);



#endif	/* UART_OPERATOR_H */

