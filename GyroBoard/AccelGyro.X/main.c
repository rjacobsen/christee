#include <xc.h>
#include <stdbool.h>

//#include <libpic30.h>
#include "Configuratoin_bits.h"
#include "main.h"
#include "I2C_API.h"
#include "gyro.h"
#include "Uart_Operator.h"


int cycleCounter = 0;
bool RUN_DATAFUNCTION = false;
int main (void)
{
    // 120MHz 60MIPS
    // Configure PLL prescaler, PLL postscaler, PLL divisor
    PLLFBD = 58; // M=60
    CLKDIVbits.PLLPOST = 0; // N2=2
    CLKDIVbits.PLLPRE = 4; // N1=6

    // Initiate Clock Switch to PRI oscillator with PLL (NOSC=0b011)
    __builtin_write_OSCCONH(0x03);
    __builtin_write_OSCCONL(OSCCON | 0x01);

    // Wait for Clock switch to occur
    while (OSCCONbits.COSC != 0b011);
    //    // Wait for PLL to lock
    while (OSCCONbits.LOCK != 1);
    
    
    //TIMER INTURUPT INIT
    
    //INTCON1bits.NSTDIS = 0;       //disables the interrupt nusting
    T2CONbits.TCKPS = 0b10;     //Prescaler for timer 2
    PR2 = 9375;                 //Period Register
    //(1/(FR/Pre))*Period        =(1/(60000000/ 64)*9375
    T2CONbits.T32 = 0;          //Timer mode select bit
    IFS0bits.T2IF = 0;          //Clears the interrupt flag
    IEC0bits.T2IE = 1;          //Enables the Timer interrupt
    T2CONbits.TON = 1;          //Enables the Timer(this timer is running a 100Hz)
    
    
    INTCON2bits.GIE = 1;        //Enables global interrupts
    
            
  
    
    //initialize LEDS
    TRISEbits.TRISE5 = 0;   
    TRISEbits.TRISE6 = 0;
    TRISEbits.TRISE7 = 0;
    TRISGbits.TRISG7 = 0;
    TRISGbits.TRISG8 = 0;
    
    //Setting LEDs
    LATEbits.LATE5 = 0;
    LATEbits.LATE6 = 0;
    LATEbits.LATE7 = 1;
    LATGbits.LATG7 = 1;
    LATGbits.LATG8 = 1;
    //puts the processor in debug mode pushing data points out the uart
    GYRO_DEBUG = true;      //If set to true will run the UART outputting angle info at a 115200 baud-rate
    InitI2Cone();           //initializes the I2c
    
    if(GYRO_DEBUG) UartInit();  //initializes the UART if debug mode is on
   
    
   // SetSleepEnabled(false); 
    initialize(0x69);           //initializing the gyros(SETTING THE SLEEP BITS)

      
    get_BaseMotion();           //getting the average values from the gyro and accel
   
       
    while(1)
    {
        LATEbits.LATE5 ^= 1;
       
        if(RUN_DATAFUNCTION)    //flagged when the the timer interrupt is triggered
        {
            get_Movement_6();       //getting the data points from the GYRO
            getAcceleration();      //Calculating the acceleration vevtors
            getAngles();            //Calculating the angles on the z, y and x axis
            RUN_DATAFUNCTION = false;   //clearing the flag
        }
        
        if(GYRO_DEBUG == true)
        {
            if(cycleCounter >= 3 )//sending data through the uart every third count of the timer interrupt
            {
                MessageParsser(angleX[0], angleY[0], angleZ[0]);
                cycleCounter = 0;       //reseting the iterator
            }       
        }
    }
}

void __attribute__((interrupt, no_auto_psv)) _T2Interrupt(void)
{
    RUN_DATAFUNCTION = true;        //setting the flag that runs the critical functions in main
    if(GYRO_DEBUG) cycleCounter++;  //increments the counter for the uart debug
    LATGbits.LATG7 ^= 1;
    IFS0bits.T2IF = 0;              //Clears the interrupt flag    
}
