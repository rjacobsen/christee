#include <xc.h>
#include <stdbool.h>

//#include <libpic30.h>
#include "Configuratoin_bits.h"
#include "main.h"
#include "I2C_API.h"
#include "gyro.h"
#include "Uart_Operator.h"



int main (void)
{
    CLKDIVbits.PLLPRE = 0;
    //Setup internal clock for 72MHz/36MIPS
    //12 /2 = 6 *24 = 144 / 2 72
    CLKDIVbits.PLLPRE = 0;  //PLLPRE (N2) 0=/2
    CLKDIVbits.DOZE = 0;
    PLLFBD = 22;    //pll multiplier (M) = +2
    //Initiate Clock switch to primary oscillator with PLL (NOSC - 0b011)
    __builtin_write_OSCCONH(0x03);
    __builtin_write_OSCCONL(OSCCON | 0x01);
    // Wait for Clock switch to occur
    while(OSCCONbits.COSC != 0b011);
    while(!OSCCONbits.LOCK);
    UartInit();
    
    
    INTCON2bits.GIE = 1;
  
            
  
    
    
    TRISEbits.TRISE5 = 0;
    TRISEbits.TRISE6 = 0;
    TRISEbits.TRISE7 = 0;
    TRISGbits.TRISG7 = 0;
    TRISGbits.TRISG8 = 0;
    

    LATEbits.LATE5 = 0;
    LATEbits.LATE6 = 0;
    LATEbits.LATE7 = 1;
    LATGbits.LATG7 = 1;
    LATGbits.LATG8 = 1;
    
    InitI2Cone();
   // SetSleepEnabled(false); 
    initialize(0x69);

      
      get_BaseMotion();
  
    while(1)
    {
        LATEbits.LATE5 ^= 1;
       
      
        get_Movement_6();
        getAcceleration();
     
        getAngles();
        MessageParsser(angleZ[0]);
        //U1TXREG = 45;// - 0xC0;//'%x' + angleZ[1]; 
        __delay_ms(10);
       

        
        
    }
}
