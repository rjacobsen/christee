/* 
 * File:   main.h
 * Author: John
 *
 * Created on April 16, 2016, 12:27 PM
 */

#ifndef MAIN_H
#define	MAIN_H


#define FOSC    (72000000ULL)
#define FCY     (FOSC/2)
#include <libpic30.h>
#define LED1  LATEbits.LATE5
#define LED2  LATEbits.LATE6
#define HIGH 0
#define LOW  1




#endif	/* MAIN_H */

