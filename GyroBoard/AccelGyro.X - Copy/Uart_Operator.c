#include <xc.h>
#include <stdbool.h>
#include <stdlib.h>

#include "Uart_Operator.h"
#define FOSC    (72000000ULL)
#define FCY     (FOSC/2)
struct UART_ring_buff output_buffer;

bool UartStall = true;
 
void UartInit(void)
{
    //****************************UART*************************
    RPOR0bits.RP65R = 0b000001;
    //PPSInput(PPS_U1RX,PPS_RP20);  	
    //PPSOutput(PPS_U1TX,PPS_RP21); 
    
   // RPOR10bits.RP21R = 3;  // remap TX pin to RC5 
            //RP22R = 
    U1MODEbits.STSEL = 0; // 1-stop bit
    U1MODEbits.PDSEL = 0; // No parity, 8-data bits
    U1MODEbits.ABAUD = 0; // Auto-baud disabled
    U1BRG = BAUD_RATE; // Baud Rate setting for 57600
    U1STAbits.URXISEL = 0b01; // Interrupt after all TX character transmitted
   // U1STAbits.URXISEL = 0b00; // Interrupt after one RX character is received
    //IFS0bits.U1RXIF = 0; // Clear RX interrupt flag
    IFS0bits.U1TXIF = 0; // Clear TX interrupt flag
    //IEC0bits.U1RXIE = 1; // Enable RX interrupt
    IEC0bits.U1TXIE = 0; // Enable TX interrupt
    U1MODEbits.UARTEN = 1; // Enable UART
    U1STAbits.UTXEN = 1; // Enable UART TX
 
    //********************************************************
}
void MessageParsser(int inputNum)
{
    
    int hundreds = 0;
    int tens = 0;
    int units = 0;
    
    if(inputNum < 0)
    {
        output_buffer.buf[output_buffer.head] = NEGATION;
        output_buffer.head++;
        output_buffer.count++;
        inputNum = abs(inputNum);
        U1TXREG = NEGATION;
        //add Negative to the buffer ring
        //then take the abs value
    }
    while(inputNum > 0)
    {
        if(inputNum >= 100)
        {
            inputNum -= 100;
            hundreds++;
        }
        else if(inputNum >= 10)
        {
            inputNum -= 10;
            tens++;
        }
        else if(inputNum >= 1)
        {
            inputNum -= 1;
            units++;
        }
    
    }    
    output_buffer.buf[output_buffer.head] = hundreds + 48;
    output_buffer.head++;
    output_buffer.buf[output_buffer.head] = tens + 48;
    output_buffer.head++;
    output_buffer.buf[output_buffer.head] = units + 48;
    output_buffer.head++;
    
    //Load_Buffer();
    U1TXREG = hundreds + 48;
    U1TXREG = tens + 48;
    U1TXREG = units + 48;
    U1TXREG = CARRIAGE_RETURN;
    if(UartStall == true)
    {
        
    }
    else
    {
        
    }
    
}



void __attribute__((interrupt, no_auto_psv)) _U1TXInterrupt(void)
{
    if(output_buffer.count > 0)
    {
        U1TXREG = output_buffer.buf[output_buffer.tail];
        output_buffer.tail++;
    }
    else
    {
        //UartStall = true;
    }
    LATGbits.LATG7 ^= 1;
     IFS0bits.U1TXIF = 0; // Clear TX interrupt flag
}
int absol(int givenNum)    
{
    if (givenNum > 0)
        return givenNum;
    else 
        return -givenNum;
    
}
  


//int getAscii(int toConvert)
//{
//    switch(toConvert)
//    {
//        case 0:
//            
//            break;
//        case 1:
//            
//            break;
//        case 2:
//            
//            break;
//        case 3:
//            
//            break;
//        case 4:
//            
//            break;
//        case 5:
//            
//            break;
//        case 6:
//            
//            break;
//        case 7:
//            
//            break;
//        case 8:
//            
//            break;
//        case 9:
//            
//            break;
//            
//    }
//}
