#include <xc.h>
#include <sys/attribs.h>
#include <stdbool.h>
#include "lidar_decoder.h"

     bool ms200done = false;
    
bool timeFlagOneHundMilSec = false;
bool timeFlagFiveSec = false;
bool timeFlagOneHundMilSecObjDet = false;
bool timeFlagOneHundMilSecObjectVelocity = false;
short timeHundMillisSinceObjectMoved = 0;
//100ms timer (PR = 31250, prescaler = 256)
void __ISR(_TIMER_3_VECTOR, IPL3SRS) Timer3Handler(void)
{
    static unsigned int count = 0;
    static unsigned char wiiCount = 0;
    count++;
    wiiCount++;  
    
    LATBbits.LATB2 ^= 1;
    
    if(ms200done == false && wiiCount>2)
    {
        wiiCount = 0;
        ms200done = true;
    }
    IFS0bits.T3IF = 0;
}

//void __ISR(_UART6_RX_VECTOR, IPL6AUTO) Uart6Handler(void)
//{
//    UART_buff_put(&buffer_five, U6RXREG);
//    
//    IFS5bits.U6RXIF = 0;
//
//    if(IFS5 & _IFS5_U6EIF_MASK)
//    {
//        U6STAbits.OERR = 0;
//        IFS5CLR = _IFS5_U6EIF_MASK;
//    }
//    
//}
/*
void __ISR(_UART_4_VECTOR, IPL1AUTO) _U4TXInterrupt(void)
{

        LATBbits.LATB13^=1;
        if (UART_buff_size(&output_buffer) > 0)
        {
            U4TXREG = UART_buff_get(&output_buffer);
        }
        else
        {
            Transmit_stall = true;
            IEC5bits.U4TXIE = 0;
        }
         //INDICATOR2=OFF;

    IFS5bits.U4TXIF = 0; // Clear TX interrupt flag
    //IFS2CLR = _IFS2_U4TXIF_MASK;
}
*/
bool get200msDone()
{
    return ms200done;
}
void set200msDone(bool value)
{
    ms200done = value;
}





