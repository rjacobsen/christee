#include <xc.h>
#include "wiiCamera.h"

//variable initialization
bool pendingTrans1 = false;
bool pendingTrans2 = false;

unsigned int camera = 0xB0;
unsigned int data_address = 0x36;

unsigned char data[2];

unsigned short Ix_buffer[2][4] = {
    {0, 0, 0, 0},
    {0, 0, 0, 0}
};
unsigned short Iy_buffer[2][4] = {
    {0, 0, 0, 0},
    {0, 0, 0, 0}
};
unsigned short xBlob[2][2] = {
    {0, 0},
    {0, 0}
};

unsigned char blobsSeen[2] = {0, 0};

//sorts input from the wii cameras and throws out extraneous data to reduce space used
void SortInput(unsigned char cameraValue, unsigned char loc) {
    unsigned char i = 0;
    unsigned char s = 0;

    //the data in the data buffer has the data organized as [x][y][s] for each blob found
    short newX = data_buf[cameraValue].data_buffer[loc - 1];
    short newY = data_buf[cameraValue].data_buffer[loc];
    s = data_buf[cameraValue].data_buffer[loc + 1];

    newX += (s & 0x30) << 4;
    newY += (s & 0xC0) << 2;
    // compares the Y values to the acceptable predetermined limits (800 and 100) which are based off of pixels within the camera view
    if (newY < MAX_Y && newY > MIN_Y) {// if acceptable

        if (blobsSeen[cameraValue] > 0) { // if there have been other blobs seen besides this one

            while (newX < Ix_buffer[cameraValue][i] && blobsSeen[cameraValue] <= 4) {  // limit to 4 bc of buffer size and max amount of blobs the wii camera can see
                // look at the x values and scroll until the newX value is larger or equal to the one in the buffer
                i++;
            }
            // advance the values in the array that are larger than the xValue so there is space for it and its corresponding Y in the array
            unsigned char j = blobsSeen[cameraValue] - 1;
            while (j > i) {
                Ix_buffer[cameraValue][j + 1] = Ix_buffer[cameraValue][j];
                Iy_buffer[cameraValue][j + 1] = Iy_buffer[cameraValue][j];
                j--;
            }
            // enter the new value into the array
            Ix_buffer[cameraValue][i] = newX;
            Iy_buffer[cameraValue][i] = newY;
            blobsSeen[cameraValue]++;
        } else {
            // it is the first blob seen, so no movement of other values is needed
            Ix_buffer[cameraValue][0] = newX;
            Iy_buffer[cameraValue][0] = newY;
            blobsSeen[cameraValue]++;
        }
    }
}
// for the horizontal beacon
// deetermine which blob values are the beacon on CHRISTEE
unsigned short PickBeacon(unsigned char cameraValue) {

    switch (blobsSeen[cameraValue]) {
        case 0:
            return 0;
            break;
        case 1:
            xBlob[cameraValue][0] = 9999;
            xBlob[cameraValue][1] = 0;
            return 9999;
            break;
        case 2:
            //LATBbits.LATB10 =0;
            if ((Iy_buffer[cameraValue][0] - Iy_buffer[cameraValue][1]) < HEIGHT_VARIANCE) {
                xBlob[cameraValue][0] = Ix_buffer[cameraValue][0];
                xBlob[cameraValue][1] = Ix_buffer[cameraValue][1];
                return (Ix_buffer[cameraValue][0] + Ix_buffer[cameraValue][1]) / 2;
            }
            return 2000;
            break;
        case 3:
            LATBbits.LATB9 ^= 1;
            if ((Iy_buffer[cameraValue][0] - Iy_buffer[cameraValue][1]) < HEIGHT_VARIANCE) {
                xBlob[cameraValue][0] = Ix_buffer[cameraValue][0];
                xBlob[cameraValue][1] = Ix_buffer[cameraValue][1];
                return (Ix_buffer[cameraValue][0] + Ix_buffer[cameraValue][1]) / 2;
            }
            if ((Iy_buffer[cameraValue][1] - Iy_buffer[cameraValue][2]) < HEIGHT_VARIANCE) {
                xBlob[cameraValue][0] = Ix_buffer[cameraValue][1];
                xBlob[cameraValue][1] = Ix_buffer[cameraValue][2];
                return (Ix_buffer[cameraValue][1] + Ix_buffer[cameraValue][2]) / 2;
            }
            return 3000;
            break;
        case 4:
            if ((Iy_buffer[cameraValue][0] - Iy_buffer[cameraValue][1]) < HEIGHT_VARIANCE) {
                xBlob[cameraValue][0] = Ix_buffer[cameraValue][0];
                xBlob[cameraValue][1] = Ix_buffer[cameraValue][1];
                return (Ix_buffer[cameraValue][0] + Ix_buffer[cameraValue][1]) / 2;
            }
            if ((Iy_buffer[cameraValue][1] - Iy_buffer[cameraValue][2]) < HEIGHT_VARIANCE) {
                xBlob[cameraValue][0] = Ix_buffer[cameraValue][1];
                xBlob[cameraValue][1] = Ix_buffer[cameraValue][2];
                return (Ix_buffer[cameraValue][1] + Ix_buffer[cameraValue][2]) / 2;
            }
            if ((Iy_buffer[2] - Iy_buffer[3]) < HEIGHT_VARIANCE) {
                xBlob[cameraValue][0] = Ix_buffer[cameraValue][2];
                xBlob[cameraValue][1] = Ix_buffer[cameraValue][3];
                return (Ix_buffer[cameraValue][1] + Ix_buffer[cameraValue][2]) / 2;
            }
            return 4000;
            break;
    }
}

// case for the vertical beacon, do not care about Iy buffer
// need to comment out the y buffer all the times.
//unsigned short PickBeacon(unsigned char cameraValue) {
//    switch (blobsSeen[cameraValue]) {
//        case 0:
//            return 0;
//            break;
//        case 1:
//            // no error checking of validity
//            return Ix_buffer[cameraValue][0];
//            break;
//        case 2:
//            if ((Ix_buffer[cameraValue][0] - Ix_buffer[cameraValue][1]) < WIDTH_VARIANCE)
//                return (Ix_buffer[cameraValue][0] + Ix_buffer[cameraValue][1]) / 2;
//            return 2000;
//            break;
//        case 3:
//            if ((Ix_buffer[cameraValue][0] - Ix_buffer[cameraValue][1]) < WIDTH_VARIANCE)
//                return (Ix_buffer[cameraValue][0] + Ix_buffer[cameraValue][1]) / 2;
//            if ((Ix_buffer[cameraValue][1] - Ix_buffer[cameraValue][2]) < WIDTH_VARIANCE)
//                return (Ix_buffer[cameraValue][1] + Ix_buffer[cameraValue][2]) / 2;
//            return 3000;
//            break;
//        case 4:
//            if ((Ix_buffer[cameraValue][0] - Ix_buffer[cameraValue][1]) < WIDTH_VARIANCE)
//                return (Ix_buffer[cameraValue][0] + Ix_buffer[cameraValue][1]) / 2;
//            if ((Ix_buffer[cameraValue][1] - Ix_buffer[cameraValue][2]) < WIDTH_VARIANCE)
//                return (Ix_buffer[cameraValue][1] + Ix_buffer[cameraValue][2]) / 2;
//            if ((Ix_buffer[cameraValue][2] - Ix_buffer[cameraValue][3]) < WIDTH_VARIANCE)
//                return (Ix_buffer[cameraValue][1] + Ix_buffer[cameraValue][2]) / 2;
//            return 4000;
//            break;
//    }
//}

void read(unsigned char cameraValue) {

    if (cameraValue == 0) {
        ReceiveI2Cone(camera, data_address, &(data_buf[cameraValue].data_buffer), 16);
        while (StatusI2Cone() == 0);
    } else {
        ReceiveI2Ctwo(camera, data_address, &(data_buf[cameraValue].data_buffer), 16);

        while (StatusI2Ctwo() == 0);
    }


}

void Sort(unsigned char cameraValue) {
    // clears all the things
    blobsSeen[cameraValue] = 0;
    unsigned char k = 0;
    while (k < 4) {
        Ix_buffer[cameraValue][k] = 0;
        Iy_buffer[cameraValue][k] = 0;
        k++;
    }

    // is the data valid?
    if (data_buf[cameraValue].data_buffer[2] < 255)
        SortInput(cameraValue, 2);
    if (data_buf[cameraValue].data_buffer[5] < 255)
        SortInput(cameraValue, 5);
    if (data_buf[cameraValue].data_buffer[8] < 255)
        SortInput(cameraValue, 8);

    if (data_buf[cameraValue].data_buffer[11] < 255)
        SortInput(cameraValue, 11);


}

unsigned char getBlobsSeen(unsigned char cameraValue) {
    return blobsSeen[cameraValue];

}

short getDifferenceX(unsigned char cameraValue) {
    return abs(xBlob[cameraValue][0] - xBlob[cameraValue][1]);
}

