/* 
 * File:   calculations.h
 * Author: Dana
 *
 * Created on August 27, 2016, 5:00 PM
 */

#ifndef CALCULATIONS_H
#define	CALCULATIONS_H

#define CAMERA1 0
#define CAMERA2 1
#define COLLECTOR_BIN 157.5 // distance apart the wii cameras are in cm?
#define OFFSET_CB 110.25// distance from edge of the collection bin to the arena edge
#define DEGREE_TO_RADIAN  0.0174532925194329577

#ifdef	__cplusplus
extern "C" {
#endif

    int CalculateHeight(unsigned char state);
    int offCenter(unsigned char state);
    unsigned char determineState(int result1, int result2);



#ifdef	__cplusplus
}
#endif

#endif	/* CALCULATIONS_H */

