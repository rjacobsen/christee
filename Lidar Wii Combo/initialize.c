#include <xc.h>
#include <stdio.h>
#include <math.h>
#include <stdbool.h>
#include "initialize.h"
//#include "lidar_decoder.h"
//#include "ObjectDetection.h"
#include "wiiCamera.h"
#include "Servos.h"
#include "uart_handler.h"
#include "interrupt_handler.h"

unsigned char wiiInitStep = 0;

void initializeBits(void)
{
    //PB2DIVbits.PBDIV = 0b1111111;
    //PB2DIVbits.ON = 1;
    
     ANSELE =0xFFEF;    // set analog config pin to a digital output for the LEDs
   //  AD1PCFG=0x0004;    // enables analog pin as digital input for RB5 only
     ANSELB = 0xFFC3;
    // TRIS ? Set a pin as Input or Output
    // LAT ? output```````````````````````````````
    // PORT ? inpput
    
  
    TRISBbits.TRISB2 = 0;   // set output debugging LEDs
    TRISBbits.TRISB3 = 0;   // set output debugging LEDs
    TRISBbits.TRISB4 = 0;   // set output debugging LEDs
    TRISBbits.TRISB5 = 0;   // set output debugging LEDs
    
    TRISEbits.TRISE1 = 0;   // set output debugging LEDs for PIC32
    TRISEbits.TRISE2 = 0;   // set output debugging LEDs for PIC32 
    TRISEbits.TRISE3 = 0;   // set output debugging LEDs for PIC32
    TRISEbits.TRISE4 = 0;   // set output debugging LEDs for PIC32
    

    //Input for switch
    //TRISBbits.TRISB2=1;

    // Initializes the debugging the LEDs to Operation OFF
    WII_INDICATOR_0 = WII_INDICATOR_OFF;
    WII_INDICATOR_1 = WII_INDICATOR_OFF;
    WII_INDICATOR_2 = WII_INDICATOR_OFF;
    WII_INDICATOR_3 = WII_INDICATOR_OFF;
    
    LIDAR_INDICATOR_0 = LIDAR_INDICATOR_OFF;
    LIDAR_INDICATOR_1 = LIDAR_INDICATOR_OFF;
    LIDAR_INDICATOR_2 = LIDAR_INDICATOR_OFF;
    LIDAR_INDICATOR_3 = LIDAR_INDICATOR_OFF;
    
    /*  UART FOR DEBUGGIN COMMENTED OUT
    //UART 3 mapping pins- Debug on Servo Lines
    TRISFbits.TRISF1 = 0;   // U3TX Debugging lines
    TRISFbits.TRISF0 = 1;   // U3RX Debugging lines
    U3RXR = 0b0100;         // Re-mapping the UART 4 RX to pin 56
    RPF1R= 0b0001;          // Re-mapping the UART 4 RX to pin 57
    */
    TRISDbits.TRISD5 = 1;   // U4RX FROM LANTRONIX
    TRISDbits.TRISD4 = 0;   // U4TX TO LANTRONIX
    U4RXR = 0b0110;         // Re-mapping the UART 4 RX to pin 53
    RPD4R = 0b0010;         // Re-mapping the UART 4 TX to pin 52
    
    TRISCbits.TRISC14 = 1;  // U1RX FROM LANTRONIX
    TRISCbits.TRISC13 = 0;  // U1RX TO LANTRONIX
    RPC13R = 0b0001;        //Re-Mapping the UART 1 TX to pin 47
    U1RXR = 0b0111;         //Re-Mapping the UART 1 RX to pin 48
    
    TRISDbits.TRISD0 = 1;   // U6RX FROM THE LIDAR MODULE
   // TRISDbits.TRISD11 = 0;  // U6TX TO THE LIDAR MODULE   As Far as I know we shouldn't need to sent anything to the lidar module
    U6RXR = 0b0011;
     
    TRISGbits.TRISG8 = 0;   // Setting the pin 10 to an out put for the Lidar motor
    RPG9R = 0b1100;         // Setting the pin 10 to the output compare module 1 (See Function PWM())
    
    TRISFbits.TRISF1 = 0;   // Setting the pin 57 to an out put for the WII Servo 1 motor
    RPF1R = 0b1100;         // Setting the pin 57 to the output compare module 6 (See Function PWM())
    
    TRISFbits.TRISF0 = 0;   // Setting the pin 56 to an out put for the WII Servo 2 motor
    RPF0R =0b1011;          // Setting the pin 56 to the output compare module 4 (See Function PWM())
    

    //initialization for I2C using I2C numbers 1 and 5
    I2C1BRG = 390;
    IPC29bits.I2C1MIP = 0b110; //priority 1
    IPC29bits.I2C1MIS = 0b10; //priority 2
    IEC3bits.I2C1MIE = 1;  //enable I2C interrupt
    I2C1CONbits.ON = 1;    //enable I2C

    I2C5BRG = 390;
    IPC46bits.I2C5MIP = 0b110; //priority 1
    IPC46bits.I2C5MIS = 0b10; //priority 2
    IEC5bits.I2C5MIE = 1;  //enable I2C interrupt
    I2C5CONbits.ON = 1;    //enable I2C


    UART();
    //begin(&receiveArray, sizeof(receiveArray),FASTTRANSFER_ADDRESS,false, Send_put,Receive_get,Receive_available,Receive_peek);
    //beginLIDARdecoder(returned_data, &buffer_five);
    timers();
    PWM();
    
    //IEC0bits.CTIE = 1;  //Core timer interrupt

    
    
    asm volatile("di");     //Disable Global Interrupts
    //asm volatile("ehb");    
    INTCONSET = _INTCON_MVEC_MASK;
    //_CP0_SET_EBASE(0x9FC01000);
    __asm__("ei");          //Enable Global Interrupts
    //T2CONbits.ON = 1;       //Enabling Timers 2,3 and 4
    T3CONbits.ON = 1; 
    //T4CONbits.ON = 1; 
}


//initializes the timers
void timers(void){
    
    /*********************TIMER2 FOR PWM (SERVO MOTOR)*****/
    //we need the PR2 to be set for 20ms
    
    T2CONbits.ON = 0;           //Turn Timer OFF
    T2CONbits.T32 = 0;          //16 bit mode 
    T2CONbits.TCKPS = 0b101;    //32 prescalar
    T2CONbits.TCS = 0;          //Timer source select bit set to External clock from TxCK pin
    T2CONbits.TGATE = 0;        //Gated time accumulation is disabled
    //TPB_clock is the clock resource for peripherals on pic32MX (p.201) (look at system clock / "FPBDIV") - located in configuration bits
    //Example:
    //Desired_Period == 100ms
    //PR# = Desired_Period / [(1/Fosc)*Prescaler]
    //PR# = 0.100seconds / [(1/80MHz)*256] = 31250
    PR2 = 46980;
    IFS0bits.T2IF = 0;      //Clear interrupt Flag
    IPC2bits.T2IP = 0b0011; //((0x1 << _IPC3_T3IP_POSITION) | (0x1 << _IPC3_T3IS_POSITION));
    IPC2bits.T2IS = 0b11;
    IEC0bits.T2IE = 1;      //Enable Interrupt
    //IEC0bits.
    
    /*********************************************************/
    
    /***********************TIMER 3**************************/ 
    T3CONbits.ON = 0;

//    T3CONbits.TCS = 0; // peripheral clock as source
    T3CONbits.TCKPS = 0b111; //256 prescalar
//    T3CONbits.TCKPS = 0b110; //64 prescalar
   // T3CONbits.TCKPS = 0b001; //2 prescalar
    T3CONbits.TCS = 0;      //Timer source select bit set to External clock from TxCK pin
    T3CONbits.TGATE = 0;     //Gated time accumulation is disabled
    //TPB_clock is the clock resource for peripherals on pic32MX (p.201) (look at system clock / "FPBDIV") - located in configuration bits
    //Example:
    //Desired_Period == 100ms
    //PR# = Desired_Period / [(1/Fosc)*Prescaler]
    //PR# = 0.100seconds / [(1/80MHz)*256] = 31250
    PR3 = 46980;
    IFS0bits.T3IF = 0;      //Clear interrupt Flag
    IPC3bits.T3IP = 0b0011; //((0x1 << _IPC3_T3IP_POSITION) | (0x1 << _IPC3_T3IS_POSITION));
    IPC3bits.T3IS = 0b11;
    IEC0bits.T3IE = 1;      //Enable Interrupt    
    
    /*******************TIMER 4 FOR PWM (LIDAR MOTOR)**************/
    T4CONbits.ON = 0;
    T4CONbits.T32 = 0; //16 bit mode
    T4CONbits.TCKPS = 0b001; //prescalar of 2
    T4CONbits.TGATE = 0;
    //PR# = Desired_Period / [(1/Fosc)*Prescaler]
    //PR# = (1/10.5kHz) / [(1/80MHz)*2] = 3800
    PR4 = 46980; //around 10.5kHz frequency
    
}

void UART(void)
{
  
    U1MODEbits.ON = 0;     // Disable whole UART 1 module
    /***************UART3 CONFIGURATION************************/
    //Debug UART- On SERVO lines
    U3MODEbits.BRGH =0;    // set to standard speed mode
    U3BRG = 42;            // 115200 baud rate
    U3MODEbits.PDSEL =0b00;//8-bit no parity
    U3MODEbits.STSEL =0;   // no stop bit
    IEC4bits.U3TXIE =0;    //transmit interrupt disable
    IFS4bits.U3TXIF=0;     //interrupt flag cleared
    IPC39bits.U3TXIS = 1;  // priority 1
    IPC39bits.U3TXIP = 1;  // sub priority 1
    U3STAbits.URXEN=1;     // enable UART RX
    U3STAbits.UTXEN = 1;   // enable UART TX
    // U3STAbits.UTXISEL=0; // UART Interrupt is generated and asserted while the transmit buffer is empty
    U3MODEbits.ON =0;      // enable UART 3
    /**********************************************************/
    
    /***************UART4 CONFIGURATION************************/
    // Lantronix UART
    U4MODEbits.BRGH = 0; // set to standard speed mode
    U4BRG = 42; //21;// 230400 baud   //42;// 115200 baud  //85;// 57600 baud
    U4MODEbits.PDSEL = 0b00; // 8-bit no parity
    U4MODEbits.STSEL = 0; // 1 stop bit
    IEC5bits.U4TXIE = 0;// transmit interrupt disable
    IFS5bits.U4TXIF =0;// interrupt flag cleared
    IPC43bits.U4TXIP = 0b111; // priority 1
    IPC43bits.U4TXIS = 0b11; // sub priority 1
    //U4STAbits.UTXISEL = 0b01; // interrupt when transmit complete
    //U1STAbits.URXISEL = 0; // interrupt generated with every reception
    U4STAbits.URXEN = 0; // enable UART receive
    U4STAbits.UTXEN = 1; // enable UART transmit
    U4MODEbits.ON = 1; // enable whole UART module
    /**********************************************************/
    
    /***************UART6 CONFIGURATION************************/
    U6MODEbits.BRGH = 0;   // set to standard speed mode
    U6BRG = 42;            //21;// 230400 baud   //42;// 115200 baud  //85;// 57600 baud
    U6MODEbits.PDSEL = 0b00; // 8-bit no parity
    U6MODEbits.STSEL = 0;  // 1 stop bit
    
    IPC47bits.U6RXIP = 0b110;  // priority 6
    IPC47bits.U6RXIS = 0b11;  // sub priority 6
    //U6STAbits.UTXISEL = 0b01; // interrupt when transmit complete
    U6STAbits.URXISEL = 0b00; // interrupt generated with every reception
    U6STAbits.URXEN = 1;   // Enable UART receive
    IFS5bits.U6RXIF = 0;
    IEC5bits.U6RXIE = 1;   // receive interrupt enable
    U6STAbits.UTXEN = 0;   // DISABLED UART transmit
    U6MODEbits.ON = 1;     // enable whole UART moduleIEC2bits.U6TXIE = 1; // transmit interrupt enable
    /************************************************************/

    //UART_buff_init(&input_buffer);
    //UART_buff_init(&output_buffer);
}


// for initializing the wii cameras on their I2C
void initializeCheck(void) {
    // checks to see if the I2C has failed to send and is not trying to send another message
    if ((StatusI2Cone() == FAILED_I2C) || !pendingTrans1) {

        // sends to I2C to restart the communication
        SendI2Cone(camera, &data, 2);
        pendingTrans1 = true;
    }
    if ((StatusI2Ctwo() == FAILED_I2C) || !pendingTrans2) {

        SendI2Ctwo(camera, &data, 2);
        pendingTrans2 = true;
    }

    //printf("InitStep %d\r\n", wiiInitStep);
    //printf("I2c one %d\r\n", StatusI2Cone());
    //printf("I2c two %d\r\n", StatusI2Ctwo());

    // if the I2C line is talking then start sending the data to it
    if ((StatusI2Ctwo() == SUCCESS_I2C) && (StatusI2Cone() == SUCCESS_I2C)) {

        pendingTrans2 = false;
        pendingTrans1 = false;
        wiiInitStep++;
    }
//    if (StatusI2Ctwo() == 0) {
//        //LATBbits.LATB9^=1;
//    }
//    if (StatusI2Cone() == 0) {
//        // LATBbits.LATB10^=1;
//    }
}

void initializeWii(void) {
    //setup the camera with the specific data bytes but only after the previous ones have been sent to each I2C
    if (wiiInitStep == 0) {
        data[0] = 0x30;
        data[1] = 0x01;
        initializeCheck();
    } else if (wiiInitStep == 1) {
        data[0] = 0x30;
        data[1] = 0x08;
        initializeCheck();
    } else if (wiiInitStep == 2) {
        data[0] = 0x06;
        data[1] = 0x90;
        initializeCheck();
    } else if (wiiInitStep == 3) {
        data[0] = 0x08;
        data[1] = 0xC0;
        initializeCheck();
    } else if (wiiInitStep == 4) {
        data[0] = 0x1A;
        data[1] = 0x40;
        initializeCheck();
    } else if (wiiInitStep == 5) {
        data[0] = 0x33;
        data[1] = 0x33;
        initializeCheck();
    }


}


char getWiiInitStep()
{
    return wiiInitStep;
}
/*
void DMA(void) {
    DMACONbits.ON = 1; // dma module enabled
    DCRCCON = 0; // crc module disabled

    //dma 1 for U6TX debugging
    DCH1CONbits.CHPRI = 2; // channel priority 2
    DCH1ECONbits.CHSIRQ = 72; // uart 6 tx Interrupt "IRQ" Vector Number (p.134)
    DCH1ECONbits.SIRQEN = 1; // enable cell transfer when IRQ triggered
    DCH1INT = 0; // clear all existing flags, disable all interrupts
    DCH1SSA = (unsigned int) &dma_one_array & 0x1FFFFFFF; // physical address conversion for transmit buffer
    DCH1DSA = (unsigned int) &U6TXREG & 0x1FFFFFFF; // physical address conversion for uart send buffer
//    DCH1SSIZ=3000; // source size at most 3000 bytes
    DCH1DSIZ = 1; // dst size is 1 byte
    DCH1CSIZ = 1; // one byte per UART transfer request
    
    //DMA 1 settings
    arrayOFdmaSetting[1].dma_array = (unsigned char*) &dma_one_array;
    arrayOFdmaSetting[1].dmacon = &DCH1CON;
    arrayOFdmaSetting[1].con_busy_mask = _DCH1CON_CHBUSY_MASK;
    arrayOFdmaSetting[1].con_en_mask = _DCH1CON_CHEN_MASK;
    arrayOFdmaSetting[1].dmasize = &DCH1SSIZ;
    arrayOFdmaSetting[1].dmaecon = &DCH1ECON;
    arrayOFdmaSetting[1].econ_force_mask = _DCH1ECON_CFORCE_MASK;

    //enable the interrupt for the dma 1 after each cell transfer
    IFS4bits.DMA1IF = 0;
    IPC33bits.DMA1IP = 2;
    IPC33bits.DMA1IS = 2; 
    IEC4bits.DMA1IE = 1;
    
    //After loading the DMA settings, start using the DMA
    queue_begin(arrayOFdmaSetting, 1);

}
*/

//Control the speed the lidar rotates (alter speed to achieve around 200rpm)
//The PWM uses Output Compare 1 (pin 46) using timer 2 (timer 1 is different than other timers)
//Fosc==80MHz
void PWM(void){
    CFGCONbits.OCACLK = 1;   //The timer sources configurations See page 310 of the data sheet
    
    /******************LIDAR PWM FOR MOTOR**********************/                     
  
    OC1CONbits.ON = 0;  //Output Compare Peripheral on bit
    OC1CONbits.OCM = 0b110; //PWM mode on OCx Fault pin disabled
    OC1CONbits.OCTSEL = 0; //select Timer 4 as source for OC1 (PWM) (Timerx OCTSEL = 0 & Timery OCTSEL = 1)(see page 310 of data sheet)
    OC1R = 2100; // near 230rpm //when PWM Drops low
    OC1RS = 2100; // near 230rpm    //is the next value that will be loaded into OCxR once the pulse is complete
    //OC1R = 2300; // near 230rpm
    //OC1RS = 2300; // near 230rpm
    //OC1R = 2450; // near 260rpm
    //OC1RS = 2450; // near 260rpm
    OC1CONbits.ON = 1;
    /***********************************************************/
    
    
    /*******************WII CAMERA PWM FOR SERVOS***************/
    OC4CONbits.ON = 0;       // Output Compare Peripheral on bit
    OC4CONbits.OCM = 0b110;  // PWM mode on OCx Fault pin disabled
    OC4CONbits.OCTSEL = 0;   // sSelect Timer 2 as source for OC1 (PWM) (Timerx OCTSEL = 0 & Timery OCTSEL = 1)(see page 310 of data sheet)
    OC4R = 0;                // When PWM Drops low
    OC4RS = 0;               // Is the next value that will be loaded into OCxR once the pulse is complete
    OC4CONbits.ON = 1;
    
    OC6CONbits.ON = 0;       // Output Compare Peripheral on bit
    OC6CONbits.OCM = 0b110;  // PWM mode on OCx Fault pin disabled
    OC6CONbits.OCTSEL = 0;   // Select Timer 2 as source for OC1 (PWM) (Timerx OCTSEL = 0 & Timery OCTSEL = 1)(see page 310 of data sheet)
    OC6R = 0;                // When PWM Drops low
    OC6RS = 0;               // Is the next value that will be loaded into OCxR once the pulse is complete
    OC6CONbits.ON = 1;       
    /***********************************************************/
    
    
    
}
