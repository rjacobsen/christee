#include <xc.h>
#include <sys/attribs.h>
#include "Servos.h"

double angle [2] = {0, 0}; // decimal angle
bool clockwise[] = {true, true};
bool beaconExists[2] = {false, false};
bool beaconCentered[2] = {false, false};
unsigned int anglex1 = 2500;// angle output compare value
unsigned int anglex2 = 2500; //2.1ms 180degrees, 1.5 90degrees, 955 0degrees
bool setHigh1 = false;
bool setHigh2 = false;
double lastAngle [2]= {90,90};
double searchWidth[2]={90,90};
bool beaconLost = true;
double servoMax = SERVO_MAX;
double servoMin=SERVO_MIN;
int countLost =0;




int isAbout(int compareThis, int toThis, int range) {
    return ((compareThis > toThis - range) && (compareThis < toThis + range));
}
void seekBeacon(int cameraNumber, int result) {
    if (result >= INVALID || result <=0) {

        beaconExists[cameraNumber] = false;
        beaconCentered[cameraNumber] = false;
        //Alternative Sweep Addition
        if(countLost>(searchWidth[cameraNumber]*2)){//beacon lost if not seen within sweep then open up the range wider
            searchWidth[cameraNumber] = searchWidth[cameraNumber]+20;// increment search width
            if(searchWidth[cameraNumber]>90)// keeping it from going out of bounds
                searchWidth [cameraNumber] = 90;
            angle[cameraNumber]=lastAngle[cameraNumber]-searchWidth[cameraNumber];// setting beginning point of sweep
            clockwise[cameraNumber]=1;// going clockwise
            servoMax= lastAngle[cameraNumber]+searchWidth[cameraNumber];// set last end of sweep
            servoMin= angle[cameraNumber]; // start of sweep
            countLost =0;
        }else
        {
            countLost++;
        }

         double stepSize = (servoMax-servoMin)/15;
//         if(stepSize>SERVO_STEP_WIDE) // possibly to limit it if it takes too long
//             stepSize = SERVO_STEP_WIDE;
        //SWEEP
        if ((angle[cameraNumber] < (servoMax - stepSize)) && clockwise[cameraNumber]) {
            angle[cameraNumber] += stepSize;
//            if (cameraNumber == LEFT_CAMERA) setAngle1(angleSet[cameraNumber]);
//            else setAngle2(angleSet[cameraNumber]);
        } else if (clockwise[cameraNumber]) {
            clockwise[cameraNumber] = false;
//            numberSweeps[cameraNumber]++;
        }
        if ((angle[cameraNumber] > (servoMin + stepSize)) && !clockwise[cameraNumber]) {
            angle[cameraNumber] -= stepSize;
  // test1         angle[cameraNumber] -= MIN_INCREMENT;
//            if (cameraNumber == LEFT_CAMERA) setAngle1(angleSet[cameraNumber]);
//            else setAngle2(angleSet[cameraNumber]);
        } else if (!clockwise[cameraNumber]) {
            clockwise[cameraNumber] = true;
//            numberSweeps[cameraNumber]++;
        }

    } else {
        countLost=0;
        searchWidth[cameraNumber] =90;
        double increment;
      //test 1  increment = MIN_INCREMENT;
            if (!isAbout(result, MIDDLE_X, BUFFER_X)) {
                beaconExists[cameraNumber] = 1;
                beaconCentered[cameraNumber] = 0;
                if (result> MIDDLE_X) {
                    increment = (((result - MIDDLE_X)) / 20);// check out the different calculations
                    if (angle[cameraNumber] > (SERVO_MIN + increment)) {
                        angle[cameraNumber] -= increment;
//                        setAngle1(angleSet[cameraNumber]);
                    }
                } else {
                    increment =((MIDDLE_X - result) / 20);// check out the different calculations
                    if (angle[cameraNumber] < (SERVO_MAX - increment)) {
                        angle[cameraNumber] += increment;
//                        setAngle1(angleSet[cameraNumber]);
                    }
                }
            } else {
                beaconExists[cameraNumber] = 1;
//                beaconAngle[cameraNumber] = angleSet[cameraNumber];
                beaconCentered[cameraNumber] = 1;
            }

       
    }
}

//Servo1
void updateAngle1(double d) {
    if (d < 180 && d > 0) {
        anglex1 = (int)(d * DEGREE) + 2500;
    } else if (d >= 180) {
        anglex1 = DEGREE180;
    } else
        anglex1 = DEGREE0;
     OC6RS = anglex1;
    
}

//Servo2
void updateAngle2(double d) {
    if (d <= 180 && d >= 0) {
        anglex2 = (int)(d * DEGREE) + 2500;
    } else if (d > 180) {
        anglex2 = DEGREE180;
    } else
        anglex2 = DEGREE0;
     OC4RS = anglex2;
    
}

void setAngle1(double d) {
    angle[SERVO1]=d;
}
void setAngle2(double d) {
    angle[SERVO2]=d;
}

unsigned char getAngle1(void)// internal angle of the servo1
{
    return ((anglex1 - 2500) / DEGREE);
}

unsigned char getAngle2(void) // internal angle of the servo2
{
    return(180- ((anglex2 - 2500) / DEGREE));
}
//
//
////where does th 80000000 come from?// clock of the processor 80 MHz
//
//void __ISR(_TIMER_4_VECTOR, IPL1AUTO) Timer4Handler(void) {
//    // if off change anglex for next time
//
//
//
//    //    if(angle[0] >0)
//    //    {
//    //    LATBbits.LATB10 = 0;
//    //    }
//    //    else
//    //    {
//    //        LATBbits.LATB10 =1;
//    //    }
//
//    if (!setHigh1) {
//        updateAngle1(angle[SERVO1]);
//        //y = P-x
//        TMR4 = 65536 - (50000 - anglex1);
//        LATEbits.LATE4 = 0;
//        setHigh1 = true;
//
//
//    } else {
//        //TMR = R-x
//        TMR4 = 65536 - anglex1;
//        LATEbits.LATE4 = 1;
//        setHigh1 = false;
//    }
//
//    IFS0CLR = _IFS0_T4IF_MASK;
//}
//
//void __ISR(_TIMER_5_VECTOR, IPL1AUTO) Timer5Handler(void) {
//    // if off change anglex for next time
//
//
//    //    if(angle[0] >0)
//    //    {
//    //    LATBbits.LATB10 = 0;
//    //    }
//    //    else
//    //    {
//    //        LATBbits.LATB10 =1;
//    //    }
//
//    if (!setHigh2) {
//        setAngle2(angle[SERVO2]);
//        //y = P-x
//        TMR5 = 65536 - (50000 - anglex2);
//        LATEbits.LATE3 = 0;
//        setHigh2 = true;
//
//    } else {
//        //TMR = R-x
//        TMR5 = 65536 - anglex2;
//        LATEbits.LATE3 = 1;
//        setHigh2 = false;
//    }
//
//    IFS0CLR = _IFS0_T5IF_MASK;
//}



