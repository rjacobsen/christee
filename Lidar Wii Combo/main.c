#include <xc.h>
#include <stdio.h>
#include <math.h>
#include <stdbool.h>
#include <sys/attribs.h>
#include "initialize.h"
#include "main.h"
#include "wiiCamera.h"
#include "Servos.h"
#include "uart_handler.h"
#include "calculations.h"
#include "defs.h"

#define CAMERA1     0
#define CAMERA2     1
#define LANTRONIX   4
#define DEGREE_TO_RADIAN  0.0174532925194329577

/// PIC32MZ2048EFH064 Configuration Bit Settings

#pragma config FMIIEN = OFF              // Ethernet RMII/MII Enable (MII Enabled)
#pragma config FETHIO = OFF              // Ethernet I/O Pin Select (Default Ethernet I/O)
#pragma config PGL1WAY = OFF             // Permission Group Lock One Way Configuration (Allow only one reconfiguration)
#pragma config PMDL1WAY = OFF            // Peripheral Module Disable Configuration (Allow only one reconfiguration)
#pragma config IOL1WAY = ON             // Peripheral Pin Select Configuration (Allow only one reconfiguration)
#pragma config FUSBIDIO = OFF            // USB USBID Selection (Controlled by the USB Module)

// DEVCFG2
#pragma config FPLLIDIV = DIV_1         // System PLL Input Divider (1x Divider)
#pragma config FPLLRNG = RANGE_5_10_MHZ   // System PLL Input Range (21-42 MHz Input)
#pragma config FPLLICLK = PLL_POSC      // System PLL Input Clock Selection (POSC is input to the System PLL)
#pragma config FPLLMULT = MUL_4         // System PLL Multiplier (PLL Multiply by 21)
#pragma config FPLLODIV = DIV_2         // System PLL Output Clock Divider (2x Divider)
#pragma config UPLLFSEL = FREQ_24MHZ    // USB PLL Input Frequency Selection (USB PLL input is 24 MHz)

// DEVCFG1
#pragma config FNOSC = SPLL             // System PLL
#pragma config DMTINTV = WIN_127_128    // DMT Count Window Interval (Window/Interval value is 127/128 counter value)
#pragma config FSOSCEN = OFF            // Secondary Oscillator Enable (Enable SOSC)
#pragma config IESO = ON                // Internal/External Switch Over (Enabled)
#pragma config POSCMOD = EC             // Primary Oscillator Configuration (HS osc mode)
#pragma config OSCIOFNC = OFF            // CLKO Output Signal Active on the OSCO Pin (Enabled)
#pragma config FCKSM = CSECME           // Clock Switching and Monitor Selection (Clock Switch Enabled, FSCM Enabled)
#pragma config WDTPS = PS1048576        // Watchdog Timer Postscaler (1:1048576)
#pragma config WDTSPGM = STOP           // Watchdog Timer Stop During Flash Programming (WDT stops during Flash programming)
#pragma config WINDIS = NORMAL          // Watchdog Timer Window Mode (Watchdog Timer is in non-Window mode)
#pragma config FWDTEN = OFF             // Watchdog Timer Enable (WDT Disabled)
#pragma config FWDTWINSZ = WINSZ_25     // Watchdog Timer Window Size (Window size is 25%)
#pragma config DMTCNT = DMT31           // Deadman Timer Count Selection (2^31 (2147483648))
#pragma config FDMTEN = OFF              // Deadman Timer Enable (Deadman Timer is enabled)

// DEVCFG0
#pragma config DEBUG = ON              // Background Debugger Enable (Debugger is disabled)
#pragma config JTAGEN = OFF             // JTAG Enable (JTAG Disabled)
#pragma config ICESEL = ICS_PGx1        // ICE/ICD Comm Channel Select (Communicate on PGEC1/PGED1)
#pragma config TRCEN = ON               // Trace Enable (Trace features in the CPU are enabled)
#pragma config BOOTISA = MIPS32         // Boot ISA Selection (Boot code and Exception code is MIPS32)
#pragma config FECCCON = OFF_UNLOCKED   // Dynamic Flash ECC Configuration (ECC and Dynamic ECC are disabled (ECCCON bits are writable))
#pragma config FSLEEP = OFF             // Flash Sleep Mode (Flash is powered down when the device is in Sleep mode)
#pragma config DBGPER = ALLOW_PG2       // Debug Mode CPU Access Permission (Allow CPU access to Permission Group 2 permission regions)
#pragma config SMCLR = MCLR_NORM        // Soft Master Clear Enable bit (MCLR pin generates a normal system Reset)
#pragma config SOSCGAIN = GAIN_2X       // Secondary Oscillator Gain Control bits (2x gain setting)
#pragma config SOSCBOOST = ON           // Secondary Oscillator Boost Kick Start Enable bit (Boost the kick start of the oscillator)
#pragma config POSCGAIN = GAIN_2X       // Primary Oscillator Gain Control bits (2x gain setting)
#pragma config POSCBOOST = ON           // Primary Oscillator Boost Kick Start Enable bit (Boost the kick start of the oscillator)
#pragma config EJTAGBEN = NORMAL        // EJTAG Boot (Normal EJTAG functionality)

// DEVCP0
#pragma config CP = OFF                 // Code Protect (Protection Disabled)

// SEQ0

// DEVSN0              // Code Protect (Protection Disabled)
// DEVSN1              
// Code Protect (Protection Disabled)

//
//void __ISR_AT_VECTOR(_TIMER_3_VECTOR, IPL3SRS) Timer3Handler(void)
//{
// 
//    LIDAR_INDICATOR_0 ^= 1;
//
//    IFS0bits.T3IF = 0;
//}

bool awaitingData = false;
bool dataAvailable = false;
char I2Cdata = 0x03;
void main(void) {
    //PRISS = 0x76543210;
    initializeBits();
<<<<<<< HEAD
    //WII_INDICATOR_3 ^= 1;
    LATEbits.LATE3 = 0;
    
    while(1)
    {
       
       SendI2Cone(3,&I2Cdata,1);
       SendI2Ctwo(camera,&I2Cdata, 16);
       //IFS5bits.I2C5MIF = 1;
    }
=======

   
    while (getWiiInitStep() <= 5) {

        initializeWii();

    }
    //timers();
    //delay
    int i = 0;
    while (i < 100000) {

        i++;
    }


    while (1) {


        if (get200msDone() && !awaitingData) {//Do I need to wait until not dataAvailable as well?

            //U6TXREG=0x99;
            unsigned char state = determineState(result1, result2);
            ToSend(0, FASTTRANSFER_ADDRESS);
            ToSend(9, state);
            ToSend(10, getAngle1()); //left camera when facing, lidar side
            ToSend(11, getAngle2()); //right camera
            ToSend(12, offCenter(state));
            ToSend(13, CalculateHeight(state));
            ToSend(14, getDifferenceX(CAMERA1)); //left camera, lidar side
            ToSend(15, getDifferenceX(CAMERA2)); // right camera

            sendData(LANTRONIX);


            printf("%.1f,  %.1f ", Ix_buffer[0][0], Iy_buffer[0][0]);
            printf("%.1f, %.1f\n", Ix_buffer[1][0], Iy_buffer[1][0]);
            printf("%.1f,  %.1f", Ix_buffer[0][1], Iy_buffer[0][1]);
            printf("%.1f, %.1f\n", Ix_buffer[1][1], Iy_buffer[1][1]);
            printf("%.1f,  %.1f --", Ix_buffer[0][2], Iy_buffer[0][2]);
            printf("%.1f, %.1f\n", Ix_buffer[1][2], Iy_buffer[1][2]);
            printf("%.1f,  %.1f --", Ix_buffer[0][3], Iy_buffer[0][3]);
            printf("%.1f, %.1f\n", Ix_buffer[1][3], Iy_buffer[1][3]);

            printf("NEXT \n");

            LATEbits.LATE2 ^= 1;
            read(CAMERA1);
            read(CAMERA2);
            set200msDone(false);
            awaitingData = true;
        }
        if (awaitingData) {
            if (StatusI2Cone() == 1 && StatusI2Ctwo() == 1) {
                dataAvailable = true;
                awaitingData = false;
            } else if (StatusI2Cone() == 2 || StatusI2Ctwo() == 2) {
                awaitingData = false;
            }
        }
        if (dataAvailable) {

            dataAvailable = false;
            Sort(CAMERA1);
            Sort(CAMERA2);
            result1 = PickBeacon(CAMERA1);
            result2 = PickBeacon(CAMERA2);
            //camera and servo1
            //seekBeacon(CAMERA1, result1);
            //seekBeacon(CAMERA2, result2);
            //printf("Hi");
            //            if (getBlobsSeen(CAMERA1) > 0 && result1 < 1023) {
            //                determineAngle(true, result1, CAMERA1);
            //            } else {
            //                determineAngle(false, result1, CAMERA1);
            //
            //            }
            //            // camera and servo 2
            //            if (getBlobsSeen(CAMERA2) > 0 && result2 <= 1023) {
            //                determineAngle(true, result2, CAMERA2);
            //            } else {
            //                determineAngle(false, result2, CAMERA2);
            //            }
            //            static int counter =0;
            //            if( counter>2)
            //            {
            if (PORTBbits.RB2) {
                setAngle1(90.0);
                setAngle2(90.0);
                LATEbits.LATE1 = 0;
            } else {
                LATEbits.LATE1 = 1;
                seekBeacon(CAMERA1, result1);
                seekBeacon(CAMERA2, result2);
            }
            //                counter = 0;
            //            }
            //            counter++;
        }



    }
   

    
//    while(1)
//    {
//       
//       SendI2Cone(3,&I2Cdata,1);
//       SendI2Ctwo(camera,&I2Cdata, 16);
//       IFS5bits.I2C5MIF = 1;
//    }
>>>>>>> 52f2e25179d1aa1b24651c449381c272d4ef721f
//    //SendI2Ctwo(4,4,4);
//    while (getWiiInitStep() <= 5) {
//
//        initializeWii();
//
//    }
//    //timers();
//    //delay
//    int i = 0;
//    while (i < 100000) {
//
//        i++;
//    }
//
//
//    while (1) {
//
//
//        if (get200msDone() && !awaitingData) {//Do I need to wait until not dataAvailable as well?
//
//            //U6TXREG=0x99;
//            unsigned char state = determineState(result1, result2);
//            ToSend(0, FASTTRANSFER_ADDRESS);
//            ToSend(9, state);
//            ToSend(10, getAngle1()); //left camera when facing, lidar side
//            ToSend(11, getAngle2()); //right camera
//            ToSend(12, offCenter(state));
//            ToSend(13, CalculateHeight(state));
//            ToSend(14, getDifferenceX(CAMERA1)); //left camera, lidar side
//            ToSend(15, getDifferenceX(CAMERA2)); // right camera
//
//            sendData(LANTRONIX);
//
//
//            printf("%.1f,  %.1f ", Ix_buffer[0][0], Iy_buffer[0][0]);
//            printf("%.1f, %.1f\n", Ix_buffer[1][0], Iy_buffer[1][0]);
//            printf("%.1f,  %.1f", Ix_buffer[0][1], Iy_buffer[0][1]);
//            printf("%.1f, %.1f\n", Ix_buffer[1][1], Iy_buffer[1][1]);
//            printf("%.1f,  %.1f --", Ix_buffer[0][2], Iy_buffer[0][2]);
//            printf("%.1f, %.1f\n", Ix_buffer[1][2], Iy_buffer[1][2]);
//            printf("%.1f,  %.1f --", Ix_buffer[0][3], Iy_buffer[0][3]);
//            printf("%.1f, %.1f\n", Ix_buffer[1][3], Iy_buffer[1][3]);
//
//            printf("NEXT \n");
//
//            LATEbits.LATE2 ^= 1;
//            read(CAMERA1);
//            read(CAMERA2);
//            set200msDone(false);
//            awaitingData = true;
//        }
//        if (awaitingData) {
//            if (StatusI2Cone() == 1 && StatusI2Ctwo() == 1) {
//                dataAvailable = true;
//                awaitingData = false;
//            } else if (StatusI2Cone() == 2 || StatusI2Ctwo() == 2) {
//                awaitingData = false;
//            }
//        }
//        if (dataAvailable) {
//
//            dataAvailable = false;
//            Sort(CAMERA1);
//            Sort(CAMERA2);
//            result1 = PickBeacon(CAMERA1);
//            result2 = PickBeacon(CAMERA2);
//            //camera and servo1
//            //seekBeacon(CAMERA1, result1);
//            //seekBeacon(CAMERA2, result2);
//            //printf("Hi");
//            //            if (getBlobsSeen(CAMERA1) > 0 && result1 < 1023) {
//            //                determineAngle(true, result1, CAMERA1);
//            //            } else {
//            //                determineAngle(false, result1, CAMERA1);
//            //
//            //            }
//            //            // camera and servo 2
//            //            if (getBlobsSeen(CAMERA2) > 0 && result2 <= 1023) {
//            //                determineAngle(true, result2, CAMERA2);
//            //            } else {
//            //                determineAngle(false, result2, CAMERA2);
//            //            }
//            //            static int counter =0;
//            //            if( counter>2)
//            //            {
//            if (PORTBbits.RB2) {
//                setAngle1(90.0);
//                setAngle2(90.0);
//                LATEbits.LATE1 = 0;
//            } else {
//                LATEbits.LATE1 = 1;
//                seekBeacon(CAMERA1, result1);
//                seekBeacon(CAMERA2, result2);
//            }
//            //                counter = 0;
//            //            }
//            //            counter++;
//        }
//
//
//
//    }


}
void _cache_err_exception_handler(void)
{
    LATEbits.LATE4 ^= 1; 
}
void _nmi_handler (void)
{
  LATEbits.LATE4 ^= 1;   
}
void _simple_tlb_refill_exception_handler(void)
{
   LATEbits.LATE4 ^= 1;  
}

static enum {
    EXCEP_IRQ = 0, // interrupt
    EXCEP_AdEL = 4, // address error exception (load or ifetch)
    EXCEP_AdES, // address error exception (store)
    EXCEP_IBE, // bus error (ifetch)
    EXCEP_DBE, // bus error (load/store)
    EXCEP_Sys, // syscall
    EXCEP_Bp, // breakpoint
    EXCEP_RI, // reserved instruction
    EXCEP_CpU, // coprocessor unusable
    EXCEP_Overflow, // arithmetic overflow
    EXCEP_Trap, // trap (possible divide by zero)
    EXCEP_IS1 = 16, // implementation specfic 1
    EXCEP_CEU, // CorExtend Unuseable
    EXCEP_C2E // coprocessor 2
} _excep_code;
static unsigned int _epc_code;
static unsigned int _excep_addr;
// this function overrides the normal _weak_ generic handler

void _general_exception_handler(void) {
    asm volatile("mfc0 %0,$13" : "=r" (_excep_code));
    asm volatile("mfc0 %0,$14" : "=r" (_excep_addr));
    _excep_code = (_excep_code & 0x0000007C) >> 2;
    LATEbits.LATE4 ^= 1; //test 1
    while (1) {

        // Examine _excep_code to identify the type of exception
        // Examine _excep_addr to find the address that caused the exception
    }
}
