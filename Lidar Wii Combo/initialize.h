/* 
 * File:   initialize.h
 * Author: Dana
 *
 * Created on August 27, 2016, 5:07 PM
 */

#ifndef INITIALIZE_H
#define	INITIALIZE_H

//debugging leds on the board
#define WII_INDICATOR_0 LATEbits.LATE1
#define WII_INDICATOR_1 LATEbits.LATE2
#define WII_INDICATOR_2 LATEbits.LATE3
#define WII_INDICATOR_3 LATEbits.LATE4

#define WII_INDICATOR_ON 0
#define WII_INDICATOR_OFF 1

#define LIDAR_INDICATOR_0 LATBbits.LATB2
#define LIDAR_INDICATOR_1 LATBbits.LATB3
#define LIDAR_INDICATOR_2 LATBbits.LATB4
#define LIDAR_INDICATOR_3 LATBbits.LATB5

#define LIDAR_INDICATOR_ON 1
#define LIDAR_INDICATOR_OFF 0

#define SYS_FREQ 80000000 // system frequency
#define FASTTRANSFER_ADDRESS 3

#ifdef	__cplusplus
extern "C" {
#endif
    int receiveArray[20];


void timers(void);
void UART(void);
void initializeBits(void);
void initializeCheck(void);
void initializeWii(void);
char getWiiInitStep();
void PWM();



#ifdef	__cplusplus
}
#endif

#endif	/* INITIALIZE_H */

