#include "Macros_Gyro.h"

#ifndef SAND
//DIGGING MOTOR LIMITS
int motorLowDig       = 18;   //with sand 18 // WITHOUT SAND 8   
int motorHighDig      = 30;   //with sand 30 // WITHOUT SAND 15
//GYRO TURNING MOTOR LIMITS
int motorLowG         = 20;   //with sand 17  // WITHOUT SAND 15
int motorHighG        = 30;   //with sand 35 // WITHOUT SAND 16
#endif

#define DEADZONE_ANGLE 0.5 //0.3

extern void updateMPU();
extern void allStop();
extern void motor_unStick();
//Create timers for operational timing
Timers CommsDelayTimingGyro(2),TurnPIDTimer(20), debugTimer(250);

//PID GYRO TURN
float gyroKp = 3.4   , gyroKi = 0.001, gyroKd = 0;
//Prepare PID system
PID output(0, gyroKp, gyroKi, gyroKd, 2);

//******************GYROSCOPE TURNING MACRO******************
 bool doTurn(int setAngle)
{
  //int magnitude = 0;
  
  //internalize the number of degrees to turn
  setAngleSet(setAngle);
  
  //zero macro angle
  setMacroAngle(0);    
  
  //Prepare the PID system
  output.updateTarget(setAngle);
  output.clearSystem();
  
  //Prepare PID tiemr
  TurnPIDTimer.resetTimer();
  
  //Prepare the comms timer
  CommsDelayTimingGyro.resetTimer();
  
  //while the robot is not0 facing the angle requested within Deadzone
  while (!((getMacroAngle() < (getAngleSet() + DEADZONE_ANGLE )) && (getMacroAngle() > (getAngleSet() - DEADZONE_ANGLE))) &&  (getStoredMacroCommand() != 0))
  {
    //Update communications every timer period
    if(CommsDelayTimingGyro.timerDone()) macroCommunicationsUpdate();
    updateMPU();
    
    //update the PID system timer
    if (TurnPIDTimer.timerDone())  //Check if the timer is done
    {
      //update decision making and sending
      turnHelp(output.updateOutput(getMacroAngle()));    
    }
  }

  //freeze motors after complete
  allStop();
  delay(5);
  motor_unStick();

  //zero macro angle
  setMacroAngle(getMacroAngle()-getAngleSet());

  macroCommunicationsUpdate();
  //END TURNING
  return (getStoredMacroCommand != 0);
}

//-----------------turning helper method.-----------------------
 void turnHelp(int mag) 
{
  if (mag > 0)
    mag = constrain(mag, motorLowG, motorHighG);
  else
    mag = constrain(mag, -motorHighG, -motorLowG);

  sendMotorCommand(-mag,mag,255);
}

