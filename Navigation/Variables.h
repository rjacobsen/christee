
#ifndef VARIABLES_H
#define VARIABLES_H

#include <Arduino.h>
#include <Timers.h>

void setBumpPresent(int s);
int getBumpPresent(void);
float getMacroAngle(void);
void setMacroAngle(float s);
float getRawGyro(void);
void setRawGyro(float s);
float getAngleY(void);
void setAngleY(float s);
int getAngleSet(void);
void setAngleSet(int s);
signed long getEncoder1(void);
signed long getEncoder2(void);
int32_t getEncoderR(void);
int32_t getEncoderL(void);
void setEncoderR(int32_t s);
void setEncoderL(int32_t s);
signed long getMacroEncoderL(void);
signed long getMacroEncoderR(void);
void setMacroEncoderL(signed long s);
void setMacroEncoderR(signed long s);
int getStoredMacroCommand(void);
void setStoredMacroCommand(int s);
int getMacroSubCommand(void);
void setMacroSubCommand(int s);
int getSubCommand(void);
bool getConinuable(void);
void setContinuable(bool s);
int getContinueMacro(void);
int getMotorBucketAngle(void);
void setMotorBucketAngle(int s);
int getLM(void);
void setLM(int s);
int getRM(void);
void setRM(int s);
int getRobotSeen(void);
void setRobotSeen(int s);
int getXCoordinate(void);
void setXCoordinate(int s);
int getYCoordinate(void);
void setYCoordinate(int s);
int getLeftAngle(void);
void setLeftAngle(int s);
int getRightAngle(void);
void setRightAngle(int s);
int getDistLeft(void);
void setDistLeft(int s);
int getDistRight(void);
void setDistRight(int s);
int getRobotHeading(void);
void setRobotHeading(int s);
int getRobotDistance(void);
void setRobotDistance(int s);
int getRobotMovementDistance(void);
void setRobotMovementDistance(int s);
int getRobotAngleFromLidar(void);
void setRobotAngleFromLidar(int s);


#endif
