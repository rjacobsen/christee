#include "Motor.h"
#include <Arduino.h>
#include <Timers.h>
#include <PID.h>
#include "Variables.h"
#include "Defines.h"
#include "Comms.h"

//Definition of the amount of centimeters of variance between the two treads to effectively stop one of them, and run the other one until it catches up
#ifndef distanceForOneTreadOperation
#define distanceForOneTreadOperation 10
#endif

//PID for control of the motor
PID simpleMotorOutput(0, 1.5, 0, 0, 2); // CONCRETE - PID simpleMotorOutput(0, 0.95, 0, 0, 2);

void simpleMotorDistanceCommand(signed long commandedDistance)
{
  static int counter=0;
  //If current target internally is not equal to the received command tell the PID where to go
  if(commandedDistance!=(signed long)simpleMotorOutput.returnTarget())
  {
    counter=0;
    simpleMotorOutput.clearSystem();
    simpleMotorOutput.updateTarget((float)commandedDistance);
  }
  //for 10 we should be calculating a decision
  if(counter<10)
  {
    //Serial.println("Calculate drive");
      simpleMotorOutput.updateOutput((getMacroEncoderL()+getMacroEncoderR())/2.0);
      counter++;
  }
  //every 10 we should make a decision
  else
  {
    //Serial.println("Send Drive Calculation");
    //Send the resulting output of the PID output error to a LR differential command method
    simpleMotorDistanceLRDiffCommand(constrain(simpleMotorOutput.updateOutput((getMacroEncoderL()+getMacroEncoderR())/2.0),-MOTOR_DRIVE_MAG_LIMIT,MOTOR_DRIVE_MAG_LIMIT), commandedDistance);
    counter=0;
  }
}

//Meters a speed input into a variable turning capable differential, allows for equal distance as we go on both treads
//    WILL act as the development for diff driving with new encoders.
void simpleMotorDistanceLRDiffCommand(signed long commandedSpeed, signed long commandedDistance)
{
  //If the left and right ENCODERS are at the same value
  if(getMacroEncoderL()==getMacroEncoderR())
  {
//    Serial.print("leftSpeed: ");
//    Serial.print(commandedSpeed);
//    Serial.print(" rightSpeed: ");
//    Serial.println(commandedSpeed);
     sendMotorCommand(commandedSpeed,commandedSpeed,255); 
  }
  //If the Left encoder has traveled further than the Right
  else if(abs(getMacroEncoderL())>abs(getMacroEncoderR()))
  {
    //calculate the magnitude of the difference
    float diff=abs(getMacroEncoderL())-abs(getMacroEncoderR());
    
    //scale it for Left versus right speed command... difference is 0-1 for reduction of tread speed
    if(diff<distanceForOneTreadOperation)                                            //if the difference is less than distance offset that will drive the tread to one side full
        diff = ((distanceForOneTreadOperation-diff)/distanceForOneTreadOperation);      //As the difference approaches the full one sided drive, the difference approaches commandedSpeed
    else 
        diff = 0;
     
    //Create a commanded speed that is not an average of distances, but an average of the errors
    float newCommandedSpeed= ((abs(getMacroEncoderR()-commandedDistance)+abs(getMacroEncoderL()-commandedDistance))/3.75);    //what is 3.75?? (a terrible magic number)... figure it out
    
    //DEBUG -- FINDING THE BREAKDOWN OF THE MAGNITUDE ISSUE
//    if (abs(newCommandedSpeed) > MOTOR_DRIVE_MAG_LIMIT)
//    {
//      Serial.print("Simple Motor Control L>R New commanded speed Out of Limits: ");
//      Serial.println(newCommandedSpeed);
//    }
    
    //If the input to this function has NOT dropped below 5 ( which is too small)
    if(abs(commandedSpeed)>=5)
    {
      newCommandedSpeed=abs(commandedSpeed);
    }
   
    //Calculate the errors, to be used for direction of motor command
    int errorRight  = commandedDistance-getMacroEncoderR();
    int errorLeft   = commandedDistance-getMacroEncoderL();
//    Serial.print("errorLeft: ");
//    Serial.print(errorLeft);
//    Serial.print("  errorRight: ");
//    Serial.println(errorRight);
    //Make new motor command values from the new speed and the direction calculated
    int rightCommand= grabIntegerSign(errorRight) * (newCommandedSpeed);
    int leftCommand = grabIntegerSign(errorLeft)  * (newCommandedSpeed*diff);
  
    sendMotorCommand(leftCommand,rightCommand,255);
  }
  else
  {
    //calculate the magnitude of the difference
    float diff=abs(getMacroEncoderR())-abs(getMacroEncoderL());
     
    //scale it for Left versus right speed command... difference is 0-1 for reduction of tread speed
    if(diff<distanceForOneTreadOperation)  //if the difference is less than distance offset that will drive the tread to one side full
        diff = ((distanceForOneTreadOperation-diff) / distanceForOneTreadOperation);  //As the difference approaches the full one sided drive, the difference approaches commandedSpeed
    else 
        diff = 0;
    
    //Create a commanded speed that is not an average of distances, but an average of the errors
    float newCommandedSpeed = ((abs(getMacroEncoderR()-commandedDistance)+abs(getMacroEncoderL()-commandedDistance))/3.75);  
    
    //DEBUG -- FINDING THE BREAKDOWN OF THE MAGNITUDE ISSUE
//    if (abs(newCommandedSpeed) > MOTOR_DRIVE_MAG_LIMIT)
//    {
//      Serial.print("Simple Motor Control R>L New commanded speed Out of Limits: ");
//      Serial.println(newCommandedSpeed);
//    }
    
    
    //If the input to this function has NOT dropped below 5 ( which is too small)
    if(abs(commandedSpeed)>=5)
    {
      newCommandedSpeed=abs(commandedSpeed);
    }
    
    //Calculate the errors, to be used for direction of motor command
    int errorRight  = commandedDistance-getMacroEncoderR();
    int errorLeft   = commandedDistance-getMacroEncoderL();
    
//    Serial.print("errorLeft: ");
//    Serial.print(errorLeft);
//    Serial.print("  errorRight: ");
//    Serial.println(errorRight);
    //Make new motor command values from the new speed and the direction calculated
    int rightCommand = grabIntegerSign(errorRight) * (newCommandedSpeed*diff);
    int leftCommand  = grabIntegerSign(errorLeft)  * (newCommandedSpeed);
        
//    Serial.print("leftCommand: ");
//    Serial.print(leftCommand);
//    Serial.print("  rightCommand: ");
//    Serial.println(rightCommand);
    sendMotorCommand(leftCommand,rightCommand,255);  
  }
}

int grabIntegerSign(signed long i)
{
 if(i>=0)
  return 1;
 else
  return -1; 
}

float grabFloatSign(float i)
{
 if(i>=0)
  return 1;
 else
  return -1;   
}

