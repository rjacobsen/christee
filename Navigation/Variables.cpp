#include "Variables.h"
#include "Defines.h"

/*********************AUTO SENSOR DATA*************************/
//GYRO DATA
float macroAngle;
float rawGyro;
float angle_y;
int angleSet;
int bumpPresent;

int getBumpPresent(void)
{
  return bumpPresent;
}
void setBumpPresent(int s)
{
  bumpPresent=s;
}

float getMacroAngle(void)
{
  return macroAngle;
}
void setMacroAngle(float s)
{
  macroAngle=s;
}
float getRawGyro(void)
{
  return rawGyro;
}
void setRawGyro(float s)
{
  rawGyro=s;
}
float getAngleY(void)
{
  return angle_y;
}
void setAngleY(float s)
{
  angle_y=s;
}
int getAngleSet(void)
{
  return angleSet;
}
void setAngleSet(int s)
{
  angleSet=s;
}

//ACTUATOR DATA
int actuatorAngle;

int returnActuatorAngle(void)
{
  return actuatorAngle;
}

//ENCODER DATA
signed long encoder1, encoder2;
int32_t encoderR, encoderL;
signed long macroEncoderL, macroEncoderR;

signed long getEncoder1(void)
{
  return encoder1;
}
signed long getEncoder2(void)
{
  return encoder2;
}
int32_t getEncoderR(void)
{
  return encoderR;
}
void setEncoderR(int32_t s)
{
  encoderR=s;
}
int32_t getEncoderL(void)
{
  return encoderL;
}
void setEncoderL(int32_t s)
{
  encoderL=s;
}
signed long getMacroEncoderL(void)
{
  return macroEncoderL;
}
void setMacroEncoderL(signed long s)
{
  macroEncoderL=s;
}
signed long getMacroEncoderR(void)
{
  return macroEncoderR;
}
void setMacroEncoderR(signed long s)
{
  macroEncoderR=s;
}

//Motor DATA 
int motor_bucket_angle;
int lM, rM;

int getMotorBucketAngle(void)
{
  return motor_bucket_angle;
}
void setMotorBucketAngle(int s)
{
  motor_bucket_angle=s;
}
int getLM(void)
{
  return lM;
}
void setLM(int s)
{
  lM=s;
}
int getRM(void)
{
  return rM;
}
void setRM(int s)
{
  rM=s;
}
/*********************AUTO CONTROL DATA*************************/
int stored_macro_command;
int macro_sub_command;
int sub_command = 0;
bool continuable;
int continueMacro = 0;

int getStoredMacroCommand(void)
{
  return stored_macro_command;
}
void setStoredMacroCommand(int s)
{
  stored_macro_command=s;
}
int getMacroSubCommand(void)
{
  return macro_sub_command;
}
void setMacroSubCommand(int s)
{
  macro_sub_command=s;
}

int getSubCommand(void)
{
  return sub_command;
}

bool getConinuable(void)
{
  return continuable;
}
void setContinuable(bool s)
{
  continuable=s;
}

int getContinueMacro(void)
{
  return continueMacro;
}
/********************* CONTROL TIMERS *************************/


Timers PIDTimer2(100);
Timers sendTimer(50);


  int robotHeading, robotDistance, robotMovementDistance, robotAngleFromLidar;
int getRobotHeading(void)
{
 return robotHeading; 
}
void setRobotHeading(int s)
{
  robotHeading=s;
}
int getRobotDistance(void)
{
  return robotDistance;
}
void setRobotDistance(int s)
{
  robotDistance=s;
}
int getRobotMovementDistance(void)
{
  return robotMovementDistance;
}
void setRobotMovementDistance(int s)
{
  robotMovementDistance=s;
}
int getRobotAngleFromLidar(void)
{
  return robotAngleFromLidar;
}
void setRobotAngleFromLidar(int s)
{
  robotAngleFromLidar=s;
}

//Wii cameras beacons variables
int robotSeen=CANT_SEE_CAMERAS;
int x_coordinate, y_coordinate;
int leftAngle,rightAngle;
int distLeft, distRight;     

int getRobotSeen(void)
{
  return robotSeen;
}
void setRobotSeen(int s)
{
  robotSeen=s;
}

int getXCoordinate(void)
{
  return x_coordinate;
}
void setXCoordinate(int s)
{
  x_coordinate=s;
}
int getYCoordinate(void)
{
  return y_coordinate;
}
void setYCoordinate(int s)
{
  y_coordinate=s;
}
int getLeftAngle(void)
{
  return leftAngle;
}
void setLeftAngle(int s)
{
  leftAngle=s;
}
int getRightAngle(void)
{
  return rightAngle;
}
void setRightAngle(int s)
{
  rightAngle=s;
}
int getDistLeft(void)
{
  return distLeft;
}
void setDistLeft(int s)
{
  distLeft=s;
}
int getDistRight(void)
{
  return distRight;
}
void setDistRight(int s)
{
  distRight=s;
}

