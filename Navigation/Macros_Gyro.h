//macros_gyro.h
//#define SAND
#ifndef MACROS_GYRO_H
#define MACROS_GYRO_H
#include <Timers.h>
#include "Variables.h"
#include "Comms.h"

//-----------------turning helper method.-----------------------
 void turnHelp(int mag) ;
extern void updateMPU();
extern void allStop();
extern void motor_unStick();
//******************GYROSCOPE TURNING MACRO******************
 bool doTurn(int setAngle);

#endif
