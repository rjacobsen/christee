#include "Comms.h"
#include "Macros.h"
#include "Defines.h"
#include <FastTransfer.h>

#include "Macros_Encoders.h"

#define DEBUG_LIDAR_SCREEN_DATA

//Timers for communications systems
Timers minimumResendTimer(50), macroSetDelay(500);

//Fast transfer system variables
FastTransfer Navigation;
int navigation_receive[50];

/***************Communications variables*******************/
bool readyToSend = false;
Timers safetyTimer(2000);

void sendControlMacroEntered(void)
{
  Navigation.ToSend(LAST_BOARD_ADDRESS_RECEIVE, NAVIGATION_ADDRESS);
  Navigation.ToSend(MACRO_COMMAND_SEND, getStoredMacroCommand());
  Navigation.sendData(CONTROL_ADDRESS);
}
//STARTUP SYSTEMS
 void initializeCommunications()
{
  // USB Serial
  Serial.begin(115200);
  // Sensor PIC direct
  Serial1.begin(115200);
  // Router PIC
  Serial3.begin(115200);
  // Fast Tramsfer Communications
  Navigation.begin(Details(navigation_receive) , NAVIGATION_ADDRESS, false, &Serial3);
}

void prepManualData()
{
  Navigation.ToSend(MACRO_COMMAND_SEND, 0);
  Navigation.ToSend(MACRO_SUB_COMMAND_SEND, 0);
}

int prevLM, prevRM, prevGyro;
void prepAutoData()
{
  if(prevLM != getLM())
  {
    Navigation.ToSend(LEFT_MOTOR    , getLM());
    prevLM = getLM();
  }
  if(prevRM != getRM())
  {
    Navigation.ToSend(RIGHT_MOTOR   , getRM());
    prevRM= getRM();
  }
  if(prevGyro != getMacroAngle())
  {
    Navigation.ToSend(GYRO          , getMacroAngle());
    prevGyro = getMacroAngle();
  }
}

//MAIN UPDATE FOR COMMUNICATIONS WHILE NOT IN A MACRO
 void updateComms()
{
  updateFromControlBoard();
  //Data has been received from the Communication Board
  if (readyToSend && minimumResendTimer.timerDone())
  {
    #ifdef DEBUG_LIDAR_SCREEN_DATA
    debugLIDARscreen();
    #endif
    prepManualData();
    prepAutoData();
    Navigation.ToSend(LAST_BOARD_ADDRESS_RECEIVE, NAVIGATION_ADDRESS);
    Navigation.sendData(CONTROL_ADDRESS);
    readyToSend = false;
    minimumResendTimer.resetTimer();
  }
}

void updateFromControlBoard()
{
  //Data received from the Communications Board
  if (Navigation.receiveData())
  {
    do
    {
       pullDataFromPacket(); 
      //If sent a macro command -- do it
      if ((getStoredMacroCommand() != 0) &&  macroSetDelay.timerDone())
      {
         //navigation_receive[MACRO_COMMAND_RECEIVE]=stored_macro_command;       
         initMacroSystem();
         return;
      }  
    } while(Navigation.receiveData());
  }
  else //failed to get fresh packet
  {
    commSafety();
  }
}

 void macroCommunicationsUpdate()
{  
  static Timers LEDresend(100);
  if(LEDresend.timerDone())
  {
    sendLEDstate(MACRO);    
  }
  while (Navigation.receiveData())
  {
    //We are ending the macro system
    if ((navigation_receive[LAST_BOARD_ADDRESS_RECEIVE] == CONTROL_ADDRESS) && (getStoredMacroCommand() != navigation_receive[MACRO_COMMAND_RECEIVE]))
    {
      setStoredMacroCommand(0);
      setMacroSubCommand(0);
      macroSetDelay.resetTimer();
      pullDataFromPacket();
      Navigation.ToSend(LAST_BOARD_ADDRESS_RECEIVE, NAVIGATION_ADDRESS);
      Navigation.ToSend(MACRO_COMMAND_SEND, getStoredMacroCommand());
      Navigation.sendData(CONTROL_ADDRESS); 
      return;
    }  
    
    pullDataFromPacket();
    if(navigation_receive[LAST_BOARD_ADDRESS_RECEIVE] == CONTROL_ADDRESS)
    {
      #ifdef DEBUG_LIDAR_SCREEN_DATA
      debugLIDARscreen();
      #endif
      prepAutoData();
      Navigation.ToSend(LAST_BOARD_ADDRESS_RECEIVE, NAVIGATION_ADDRESS);
      Navigation.ToSend(MACRO_COMMAND_SEND, getStoredMacroCommand());
      Navigation.sendData(CONTROL_ADDRESS);    
    }
    
    
  }
    //commSafety();
}

_16_to_32_t _16_to_32;

int LIDARxCoordComms, LIDARyCoordComms;

void pullDataFromPacket() 
{
  static float keeper1, keeper2;
  static signed long encoderPastR=0, encoderPastL=0;
  static Timers LEDresendTimer(100);
  static bool bootupInit=false;
    
  switch ( navigation_receive[LAST_BOARD_ADDRESS_RECEIVE]) 
  {
    case BEACON_ADDRESS:
    
      digitalWrite(13,!digitalRead(13));
      
      #ifndef LIDARCODE
        
        setRobotSeen(  navigation_receive[ROBOT_SEEN_VALUE]);
        setXCoordinate(  navigation_receive[WII_X]);
        setYCoordinate (  navigation_receive[WII_Y]);
        setLeftAngle (  navigation_receive[WII_ANGLE_LEFT]);
        setRightAngle (  navigation_receive[WII_ANGLE_RIGHT]);
        setDistLeft (  navigation_receive[WII_DIST_LEFT]);
        setDistRight  (  navigation_receive[WII_DIST_RIGHT]);
        //Serial.println("Heard From the Wii System");
        
        //debugWiiCamerasBeacon();

      #else
        setRobotHeading( navigation_receive[LIDAR_OBJ_HEADING]);
        setRobotDistance(  navigation_receive[LIDAR_OBJ_MAG]);
        setRobotMovementDistance(  navigation_receive[LIDAR_OBJ_DISPLACEMENT]);
        setRobotAngleFromLidar(  navigation_receive[LIDAR_OBJ_ANGLE]);
        
//        Serial.print("Heading: ");
//        Serial.print(getRobotHeading());
//        Serial.print(" ,Distance: ");
//        Serial.print(getRobotDistance());
//        Serial.print(" ,Angle: ");
//        Serial.print(getRobotAngleFromLidar());
//        Serial.print(" ,Delta Distance: ");
//        Serial.println(getRobotMovementDistance()); 
      #endif
      break;
      
    case CONTROL_ADDRESS:
        //time stamp activity from communications board and okay a response to comm
      readyToSend = true;
      safetyTimer.resetTimer();
      
      //CONTROL RECEIVE
      setStoredMacroCommand(navigation_receive[MACRO_COMMAND_RECEIVE]);
      setMacroSubCommand(navigation_receive[MACRO_SUB_COMMAND_RECEIVE]);
      
      if (getStoredMacroCommand() == 0)
      {
         if(LEDresendTimer.timerDone())
         {    
            sendLEDstate(MANUAL);
         }
      }
      break;
     
    case MOTOR_ADDRESS:
      //----------------MOTOR BOARD RECEIVE DATA------------------
      setMotorBucketAngle( navigation_receive[ACTUATOR_ANGLE]);
      break;  
      
    case PIC_ADDRESS:
      //----------------PIC ENCODER DATA---------------------------
      //STARTUP INIT VARIABLES
      if(!bootupInit)
      {  
        _16_to_32.endian.high       = navigation_receive[ENCODER_R_H_PIC_RECEIVE];
        _16_to_32.endian.low        = navigation_receive[ENCODER_R_L_PIC_RECEIVE];
        encoderPastR                = ( _16_to_32.joined  ) ;    //IMPLIED CM*100 -> IMPLIED CM        
        _16_to_32.endian.high       = navigation_receive[ENCODER_L_H_PIC_RECEIVE];
        _16_to_32.endian.low        = navigation_receive[ENCODER_L_L_PIC_RECEIVE];
        encoderPastL                = ( _16_to_32.joined ) ;       
        bootupInit=true;
      }
      //DISTANCE PULSES
      _16_to_32.endian.high         = navigation_receive[ENCODER_R_H_PIC_RECEIVE];
      _16_to_32.endian.low          = navigation_receive[ENCODER_R_L_PIC_RECEIVE];
      setEncoderR( _16_to_32.joined  ) ;    //IMPLIED CM*100 -> IMPLIED CM      
      _16_to_32.endian.high         = navigation_receive[ENCODER_L_H_PIC_RECEIVE];
      _16_to_32.endian.low          = navigation_receive[ENCODER_L_L_PIC_RECEIVE];
      setEncoderL( _16_to_32.joined ) ;
      
//      if(encoderR==0 && encoderL==0)
//      {
//          encoderPastL = 0;
//          encoderPastR = 0;
//      }      
      if(getEncoderL()!=encoderPastL)
      {  
         //Use old value to calculate the increment
         updateMacroEncoderValueL(getEncoderL()-encoderPastL);
          //Store current value as old 
         encoderPastL                = getEncoderL();
      }
      if(getEncoderR()!=encoderPastR)
      {
         //Use old value to calculate the increment
         updateMacroEncoderValueR(getEncoderR()-encoderPastR);
         //Store current value as old 
         encoderPastR                = getEncoderR();
      }  
      
      //----------------PIC GYRO DATA---------------------------
      setAngleY( (int)navigation_receive[GYRO_Z_ANGLE_PIC]);
      setBumpPresent  ( navigation_receive[GYRO_IMPACT_PIC]);
      setRawGyro( navigation_receive[GYRO_RAW]);
      updateMPU();
      
      break;
  }
}

//DELAY TIMEOUT OCCURRED METHOD
 void packetWait()
{
  //sendMotorCommand(0, 0, 255);
  sendLEDCommand(0);
  while (navigation_receive[LAST_BOARD_ADDRESS_RECEIVE]!=CONTROL_ADDRESS)
  {
    Navigation.receiveData();
    delay(1);
  }
  sendLEDstate(MANUAL);
  readyToSend = true;      //make not we got a good one
  safetyTimer.resetTimer(); //safety system reset
}

//CHECK IF THE TIMER HAS RUN OUT BETWEEN COMMS UPDATES
 void commSafety()
{
  if (safetyTimer.timerDone())
  {
    packetWait();
  }
}

 void terminateMacroSystem()
{
  setStoredMacroCommand(0);  
  setLM ( 0);
  setRM ( 0);  
  
  Navigation.ToSend(LAST_BOARD_ADDRESS_RECEIVE, NAVIGATION_ADDRESS);
  Navigation.ToSend(MACRO_COMMAND_SEND, getStoredMacroCommand());
  Navigation.sendData(CONTROL_ADDRESS);
  macroSetDelay.resetTimer();
}

//MOTOR COMMAND HELPER COMMUNICATIONS METHODS
void sendMotorCommand(int leftMotor, int rightMotor)
{
  if(abs(leftMotor)>MOTOR_DRIVE_MAG_LIMIT)
  {
//    Serial.print("Range issue LMotor: ");
//    Serial.println(leftMotor);
    leftMotor=grabIntegerSign(leftMotor)*MOTOR_DRIVE_MAG_LIMIT;
  }
  if(abs(rightMotor)>MOTOR_DRIVE_MAG_LIMIT)
  {
//    Serial.print("Range issue RMotor: ");
//    Serial.println(leftMotor);    
    rightMotor=grabIntegerSign(rightMotor)*MOTOR_DRIVE_MAG_LIMIT;    
  }

  setLM( leftMotor);
  setRM( rightMotor);
  Navigation.ToSend(LAST_BOARD_ADDRESS_RECEIVE, NAVIGATION_ADDRESS);
  Navigation.ToSend(LEFTMOTOR_MOTOR_SEND, leftMotor);
  Navigation.ToSend(RIGHTMOTOR_MOTOR_SEND, rightMotor);
  Navigation.sendData(MOTOR_ADDRESS);
}

void sendMotorCommand(int leftMotor, int rightMotor, int actuator)
{
  setLM( leftMotor);
  setRM( rightMotor);
  Navigation.ToSend(LAST_BOARD_ADDRESS_RECEIVE, NAVIGATION_ADDRESS);
  Navigation.ToSend(LEFTMOTOR_MOTOR_SEND, leftMotor);
  Navigation.ToSend(RIGHTMOTOR_MOTOR_SEND, rightMotor);
  Navigation.ToSend(ACTUATOR_MOTOR_SEND, actuator);
  Navigation.sendData(MOTOR_ADDRESS);
}

void sendActuatorCommand(int actuator)
{
  Navigation.ToSend(LAST_BOARD_ADDRESS_RECEIVE, NAVIGATION_ADDRESS);
  Navigation.ToSend(ACTUATOR_MOTOR_SEND, actuator);
  Navigation.sendData(MOTOR_ADDRESS);
}

void sendLEDCommand(int color) 
{
//  Navigation.ToSend(LAST_BOARD_ADDRESS_RECEIVE, NAVIGATION_ADDRESS);
//  Navigation.ToSend(STATE, 6);
//  Navigation.ToSend(COLOR_PICKER, color);
//  Navigation.sendData(LED_ADDRESS);
}
void sendLEDstate(int state) 
{
  Navigation.ToSend(LAST_BOARD_ADDRESS_RECEIVE, NAVIGATION_ADDRESS);
  Navigation.ToSend(STATE, state);  
  Navigation.ToSend(COLOR_PICKER, 0);
  Navigation.sendData(LED_ADDRESS);
}



void sendLIDARPosHeading(void)
{
   #ifdef LIDARCODE
  Navigation.ToSend(LIDAR_COMMAND_INDEX,LIDAR_GET_LEAPFROG_HEADING);
  Navigation.sendData(BEACON_ADDRESS);
  #endif
}

void sendLIDARRequestDataOnly(void)
{
  Navigation.ToSend(LIDAR_COMMAND_INDEX,LIDAR_OLD_OBJ_DATA_REQUEST);
  Navigation.sendData(BEACON_ADDRESS);

  
}
void debugWiiCamerasBeacon(void)
{
        //DEBUG FOR COMMS AND DATA FROM THE WII CAMERAS
//      switch(robotSeen)
//      {
//        case CANT_SEE_CAMERAS:
//        
//          Serial.print("CANT SEE CAMERA L_angle: ");  
//          Serial.print(leftAngle);       
//          Serial.print(" ,R_angle: ");  
//          Serial.println(rightAngle);
//          break;
//        case LEFT_CAMERA_ONLY_1:
//          Serial.print("LEFT_CAMERA_ONLY_1 angle: ");     
//          Serial.print(leftAngle);  
//          Serial.print(" ,dist: ");
//          Serial.println(distLeft);       
//          break;
//        case RIGHT_CAMERA_ONLY_1:
//          Serial.print("RIGHT_CAMERA_ONLY_1 angle: ");     
//          Serial.print(rightAngle);  
//          Serial.print(" ,dist: ");
//          Serial.println(distRight);
//          break;
//        case RIGHT_CAMERA_ONLY:
//          Serial.print("RIGHT_CAMERA_ONLY angle: ");     
//          Serial.print(rightAngle);  
//          Serial.print(" ,dist: ");
//          Serial.println(distRight);     
//          break;
//       case LEFT_CAMERA_ONLY:
//          Serial.print("LEFT_CAMERA_ONLY angle: ");     
//          Serial.print(leftAngle);  
//          Serial.print(" ,dist: ");
//          Serial.println(distLeft);
//          break;
//       case BOTH_CAMERAS:
//          Serial.print("BOTH_CAMERAS L_angle: ");  
//          Serial.print(leftAngle);       
//          Serial.print(" ,R_angle: ");  
//          Serial.println(rightAngle);
//          Serial.print("distLeft: ");
//          Serial.print(distLeft);
//          Serial.print(" ,distRight: ");
//          Serial.println(distRight);
//          break;
//       case BOTH_CAMERAS_1:
//          Serial.print("BROKEN_TRIANGLE L_angle: ");  
//          Serial.print(leftAngle);       
//          Serial.print(" ,R_angle: ");  
//          Serial.println(rightAngle);
//          Serial.print("distLeft: ");
//          Serial.print(distLeft);
//          Serial.print(" ,distRight: ");
//          Serial.println(distRight);
//          Serial.print("X: ");
//          Serial.print(x_coordinate);
//          Serial.print(" ,Y: ");
//          Serial.println(y_coordinate);
//          break;
//       case XY_FOUND:
//          Serial.print("XY_FOUND L_angle: ");  
//          Serial.print(leftAngle);       
//          Serial.print(" ,R_angle: ");  
//          Serial.println(rightAngle);
//          Serial.print("distLeft: ");
//          Serial.print(distLeft);
//          Serial.print(" ,distRight: ");
//          Serial.println(distRight);
//          Serial.print("X: ");
//          Serial.print(x_coordinate);
//          Serial.print(" ,Y: ");
//          Serial.println(y_coordinate);             
//          break;       
//      }  
}

int prevDisp, prevHead, prevAngle, prevDist;

void debugLIDARscreen(void)
{
  if(getLastDispUsed() != prevDisp)
  {
    Navigation.ToSend(LIDAR_DISPLACEMENT, getLastDispUsed());
    prevDisp = getLastDispUsed();
  }
  if(getLastHeadingUsed() != prevHead)
  {
    Navigation.ToSend(LIDAR_HEADING, getLastHeadingUsed());
    prevHead = getLastHeadingUsed();
  }
  if(getLastAngleUsed() != prevAngle)
  {
    Navigation.ToSend(LIDAR_ANGLE, getLastAngleUsed());
    prevAngle = getLastAngleUsed();
  }
  if(getLastDistUsed() != prevDist)
  {
    Navigation.ToSend(LIDAR_DISTANCE, getLastDistUsed());
    prevDist = getLastDistUsed();
  }
  
}
//  int getRobotHeading(void)
//  {
//    return robotHeading;
//  }
//  
//  int getRobotDistance(void)
//  {
//    return robotDistance;
//  }
//  
//  int getRobotMovementDistance(void)
//  {
//    return robotMovementDistance;
//  }
//  
//  int getRobotAngleFromLidar(void)
//  {
//    return robotAngleFromLidar
//  }
