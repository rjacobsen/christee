#include "Macros_Actuator.h"
#include "Methods.h"

int angleSetIn;
Timers CommsDelayTimingActuator(2);
Timers sendActuatorCommandTimer(50);


bool sendActuatorPositionFeedback(int angle)
{  
  CommsDelayTimingActuator.resetTimer();
  while((getStoredMacroCommand()!=0)&&(!isAbout(getMotorBucketAngle(),angle,2)))
  {
    if(sendActuatorCommandTimer.timerDone())
      sendActuatorCommand(angle);
    if(CommsDelayTimingActuator.timerDone()) 
    {
      macroCommunicationsUpdate();
    }
  }
  sendActuatorCommand(255);

  return (getStoredMacroCommand() != 0);
}

 bool sendActuatorPositionDig(int angle)
{  
  CommsDelayTimingActuator.resetTimer();
  
  wipeEncoders();
  
  while( (getStoredMacroCommand()!=0) && (!isAbout(getMotorBucketAngle(),angle,2)  ))//&& ((encoderL<DIG_DRIVE_DISTANCE)&&(encoderR<DIG_DRIVE_DISTANCE))
  {
    
    if(sendActuatorCommandTimer.timerDone())
    {
      if(getMotorBucketAngle() < BUCKET_DIG_DRIVE_BEGIN_ANGLE)
      {
        sendMotorCommand(15,15,angle);
      }
      else 
      {
        sendActuatorCommand(angle);
      }
    }
     
    if(CommsDelayTimingActuator.timerDone()) 
    {
      macroCommunicationsUpdate();
    }
  }
  sendMotorCommand(0,0,255);

  return (getStoredMacroCommand() != 0);
}

