#ifndef MACROS_WII_H
#define MACROS_WII_H
#include "Defines.h"

#define HALFWAY_VALUE 1.89

#define TURN_DEGREE_WII 40

inline bool reverseIn(void)
{ 
  int AUTO_STATE=6;
  Timers doStuffTimer(300);
  int unwind=0;
  Timers timeOut(4000),timeOut2(4000);
  timeOut2.resetTimer();
  timeOut.resetTimer();
  wipeGyro();
  int x1=0,x2=0;
  //ADDED CONDITION THAT THE Y-coord needs to be greater than 10 if robot seen
  while(getStoredMacroCommand()!=0 && !timeOut.timerDone() && (((getRobotSeen()==XY_FOUND)&&(getYCoordinate()>50)) || (getRobotSeen()!=XY_FOUND) ))
   {
     //Wait for acquisition
     while((getRobotSeen()!=XY_FOUND)&&(getStoredMacroCommand()!=0)&& !timeOut2.timerDone())
     {                 
       macroCommunicationsUpdate();
       delay(2);
       doStuffTimer.resetTimer();
     }
     if(doStuffTimer.timerDone())
     {                 
       //Wait for both cameras to see the robot
       if((getRobotSeen()==XY_FOUND))  //(getRobotSeen()==BOTH_CAMERAS) || (getRobotSeen()==BOTH_CAMERAS_1) || 
       {         
//            Serial.println();
//            Serial.print("Y coord: ");  
//            Serial.println(y_coordinate);
           //Grab the original x coordinate of the robot
           if(x1==0)
           {
              x1=getXCoordinate();
              //Serial.print("First x coord: ");
             // Serial.println(x1);
           }   
           
           timeOut.resetTimer();
           timeOut2.resetTimer();
           //If left angle is less than right
           if(getLeftAngle()<(getRightAngle()-10))
           {
//              Serial.print("L<R Turning R: ");
//              Serial.print(rightAngle);
//              Serial.print(" L: ");
//              Serial.println(leftAngle);
              //Serial.println(" Correct +10");
              doTurn(TURN_DEGREE_WII);
              unwind+=TURN_DEGREE_WII;
//              Serial.println("Backwards 40");
              newEncoders(-40);
             
              timeOut2.resetTimer();
              timeOut.resetTimer();
              x1=0;
              x2=0;
           }
           //if left is greater than right
           else if(getLeftAngle()>(getRightAngle()+10))
           {
//               Serial.print("R<L Turning R: ");
//               Serial.print(rightAngle);
//               Serial.print(" L: ");
//               Serial.println(leftAngle);
               //Serial.println(" Correct -10");
               doTurn(-TURN_DEGREE_WII);
//               Serial.println("Backwards 40");
               newEncoders(-40);
               unwind-=TURN_DEGREE_WII;
               
               timeOut2.resetTimer();
               timeOut.resetTimer(); 
               x1=0;
               x2=0;
           }
           else
           {
//               Serial.print("Straight Movement R: ");
//               Serial.print(rightAngle);
//               Serial.print(" L: ");
//               Serial.println(leftAngle);
               //Wipe the coordinate before we do movement to ensure non-stale value after
               setXCoordinate(0);
               //doTurn(-unwind);
               newEncoders(-40);
               timeOut2.resetTimer();
               timeOut.resetTimer();
               doStuffTimer.resetTimer();
               
               while(!doStuffTimer.timerDone()&&(getStoredMacroCommand()!=0))
               {
                 macroCommunicationsUpdate();
                 
               }
               //If we move straight, record the x pos if its not zero
               if(x2==0 && getXCoordinate()!=0)
               {
                  x2=getXCoordinate();
//                  Serial.print("X2 coord: ");
//                  Serial.println(x2);
               }
               //if we collected values for both x-coordinates
               if(x1!=0 && x2!=0)
               {             
//                    Serial.print("Difference x coord: ");
//                    Serial.println(x1-x2);
                    //CORRECT THE ANGLE OF THE ROBOT IF THE VALUES ARE NOT SIMILAR
                    if(abs(x1-x2)>1)
                    {
                      sendLEDstate(MANUAL);
                      sendLEDstate(MANUAL);
//                      Serial.print("X coord Turning: ");
//                      Serial.println((int)(x1-x2)*1.5);
                      doTurn((int)(x1-x2)*1.5);  
                      sendLEDstate(AUTO);
                      sendLEDstate(AUTO);                  
                    }
                    //WIPE THE VALUES TO RESTART THE AQUISITION
                    x1=0;
                    x2=0;
               }  
               else 
               {
                    x1=0; 
                    x2=0;
               }
           }
//          Serial.print("Unwind: ");
//          Serial.println(-unwind);
           delay(50);
           doTurn(-unwind);
      
           //Reset timeout     
           timeOut2.resetTimer();
           timeOut.resetTimer();
           //Reset unwind value
           unwind=0;
           //Wipe the camera seen value
           setRobotSeen(CANT_SEE_CAMERAS);
        }
     }
     else
     {
      
     }     
       macroCommunicationsUpdate();
   }
  
//  Serial.print("Y coord finish: ");  
//  Serial.println(y_coordinate);
  return (getStoredMacroCommand() != 0);  
}

inline bool traverseOutCentered(void)
{
  Timers doStuffTimer(300);
  int unwind=0;
  Timers timeOut(4000),timeOut2(4000);
  timeOut2.resetTimer();
  timeOut.resetTimer();
  wipeGyro();
  int x1=0,x2=0;
  while(getStoredMacroCommand()!=0 && !timeOut.timerDone() && (((getRobotSeen()==XY_FOUND)&&(getYCoordinate()<150)) || (getRobotSeen()!=XY_FOUND) ))
   {
     //Wait for acquisition
     while((getRobotSeen()!=XY_FOUND)&&(getStoredMacroCommand()!=0)&& !timeOut2.timerDone())
     {                 
       macroCommunicationsUpdate();
       delay(2);
       doStuffTimer.resetTimer();
     }
     if(doStuffTimer.timerDone())
     {                 
       //Wait for both cameras to see the robot
       if((getRobotSeen()==XY_FOUND))  
       {  
//            Serial.println();
//            Serial.print("Y coord: ");  
//            Serial.println(y_coordinate);       
           //Grab the original x coordinate of the robot
           if(x1==0)
           {
              x1=getXCoordinate();
//              Serial.print("First x coord: ");
//              Serial.println(x1);
           }   
           
           timeOut.resetTimer();
           timeOut2.resetTimer();
           //If left angle is less than right
           if(getLeftAngle()>(getRightAngle()+10))
           {
//              Serial.print("L>R Turning R: ");
//              Serial.print(rightAngle);
//              Serial.print(" L: ");
//              Serial.println(leftAngle);
              doTurn(TURN_DEGREE_WII);
              unwind+=TURN_DEGREE_WII;
              newEncoders(40);
             
              timeOut2.resetTimer();
              timeOut.resetTimer();
              x1=0;
              x2=0;
           }
           //if left is greater than right
           else if(getLeftAngle()<(getRightAngle()-10))
           {
//               Serial.print("R>L Turning R: ");
//               Serial.print(rightAngle);
//               Serial.print(" L: ");
//               Serial.println(leftAngle);
               doTurn(-TURN_DEGREE_WII);
               newEncoders(40);
               unwind-=TURN_DEGREE_WII;
               
               timeOut2.resetTimer();
               timeOut.resetTimer(); 
               x1=0;
               x2=0;
           }
           else
           {
//               Serial.print("Straight Movement R: ");
//               Serial.print(rightAngle);
//               Serial.print(" L: ");
//               Serial.println(leftAngle);
               //Wipe the coordinate before we do movement to ensure non-stale value after
               setXCoordinate(0);
               
               newEncoders(40);
               timeOut2.resetTimer();
               timeOut.resetTimer();
               doStuffTimer.resetTimer();
               
               while(!doStuffTimer.timerDone()&&(getStoredMacroCommand()!=0))
               {
                 macroCommunicationsUpdate();
                 
               }
               //If we move straight, record the x pos if its not zero
               if(x2==0 && getXCoordinate()!=0)
               {
                  x2=getXCoordinate();
//                  Serial.print("X2 coord: ");
//                  Serial.println(x2);
               }
               //if we collected values for both x-coordinates
               if(x1!=0 && x2!=0)
               {             
//                    Serial.print("Difference x coord: ");
//                    Serial.println(x1-x2);
                    //CORRECT THE ANGLE OF THE ROBOT IF THE VALUES ARE NOT SIMILAR
                    if(abs(x1-x2)>1)
                    {
                      sendLEDstate(MANUAL);
                      sendLEDstate(MANUAL);
//                      Serial.print("X coord Turning: ");
//                      Serial.println((int)(x2-x1)*1.5);
                      doTurn((int)(x2-x1)*1.5);  
                      sendLEDstate(AUTO);
                      sendLEDstate(AUTO);                  
                    }
                    //WIPE THE VALUES TO RESTART THE AQUISITION
                    x1=0;
                    x2=0;
               }  
               else 
               {
                    x1=0; 
                    x2=0;
               }
           }
           delay(50);
           doTurn(-unwind);
 
           //Reset timeout     
           timeOut2.resetTimer();
           timeOut.resetTimer();
           //Reset unwind value
           unwind=0;
           //Wipe the camera seen value
           setRobotSeen(CANT_SEE_CAMERAS);
        }
     }
     else
     {
     }
       macroCommunicationsUpdate();
   }
   
//  Serial.print("Y coord finish: ");  
//  Serial.println(y_coordinate);
  return (getStoredMacroCommand() != 0);
}


#define UNCERTAIN   0
#define RIGHTSIDE   1
#define LEFTSIDE    2
#define KNOWN       1

//MODIFY THIS TO TUNE THE REQUIRED ANGLE CHANGE TO KNOW OUR DIRECTION
#define LOCALIZATION_ANGLE_CHANGE 5

inline bool orientFromStart(void)
{
  //Setup the timers
  Timers wiiCameraAcquisitionDelay(3000),wiiCameraUpdateDelay(300);
  wiiCameraAcquisitionDelay.resetTimer();
  wiiCameraUpdateDelay.resetTimer();
  
  //Make some variables to track progress
  int robotSide=UNCERTAIN,roboDirection=UNCERTAIN,cameraAngle=0;
  
  //Init gyro angles
  wipeGyro();
  
  Serial.println("Wii Orienting");
  
  //Determine which side we are on
  while(getStoredMacroCommand()!=0 && (roboDirection==UNCERTAIN))
   {
     Serial.println("Main Loop Wii");
     //We will sit in this method until the robots location is known
     while(getStoredMacroCommand()!=0 && robotSide==UNCERTAIN)
     {
       //If robot is not seen in 3 seconds
       if(wiiCameraAcquisitionDelay.timerDone())
       {
         Serial.println("Turn to be seen");
         //Reorient to attempt to see the beacons
         doTurn(15);
       }
       //Check wii data
       switch(getRobotSeen())
       {
           //If only the left camera sees us, we are on the right side
           case RIGHT_CAMERA_ONLY:
           case RIGHT_CAMERA_ONLY_1:
               //record the angle of the camera
               cameraAngle=getLeftAngle();
               //and that we are on the right
               robotSide=RIGHTSIDE;
               Serial.println("Robots on the Right");
               break;
           //If only the right camera sees us, we are on the left side
           case LEFT_CAMERA_ONLY:
           case LEFT_CAMERA_ONLY_1:
               //record the angle of the camera
               cameraAngle=getRightAngle();
               //and that we are on the left
               robotSide=LEFTSIDE;
               Serial.println("Robots on the Left");
               break;
           case CANT_SEE_CAMERAS:
               robotSide=UNCERTAIN;
               break;
       }
       macroCommunicationsUpdate();
     }
   
     //Break out of the macro if necessary
     if(getStoredMacroCommand()==0) return (getStoredMacroCommand()!=0);
     
     //Reset timers
     wiiCameraAcquisitionDelay.resetTimer();
     wiiCameraUpdateDelay.resetTimer();
     
     //Determine the orientation of the robot
     while(getStoredMacroCommand()!=0 && roboDirection==UNCERTAIN)
     {
       
         Serial.println("Do inital move forward");
        //Move to check out change in angle
         newEncoders(30);
         wiiCameraAcquisitionDelay.resetTimer();
         
         //Wait for the wii cameras to update
         while(!wiiCameraAcquisitionDelay.timerDone() && getStoredMacroCommand()!=0)
         {
             macroCommunicationsUpdate();
         }
         if(getStoredMacroCommand()==0) return (getStoredMacroCommand()!=0);
           //Check which side we are on to determine which camera to look at
           switch(robotSide)
           {
              //if on the left side
              case LEFTSIDE:
                //Make sure the robot is still only seen by the one camera
                if(getRobotSeen()==LEFT_CAMERA_ONLY || getRobotSeen()==LEFT_CAMERA_ONLY_1)
                {
                    //check if moving the robot has moved the angle up enough since before movement
                    if(cameraAngle>getLeftAngle()+LOCALIZATION_ANGLE_CHANGE)
                    {
                       //We are facing outward, we know where we are
                       roboDirection=KNOWN;              
                       Serial.println("Robots on the Left Oriented");
                    }
                    //the angle didnt change enough
                    else
                    {
                      //Record new init angle
                      cameraAngle=getLeftAngle();
                      //move in the opposite direction
                      Serial.println("Not oriented well Move Backward");
                      newEncoders(-30);
                      
                      //MAY WANT A DELAY HERE FOR WII CAMERA ACQUISITION
                      
                      //if this move was in the outward direction
                      if(cameraAngle>getLeftAngle()+LOCALIZATION_ANGLE_CHANGE)
                      {
                         Serial.println("Determined Direction - Turn Around 180");
                         //turn around since we are backward
                         doTurn(180);
                         //exit the system
                         roboDirection=KNOWN;
                         Serial.println("Robots on the Left Oriented");
                      }
                      //if the angle still didnt change enough to record a change
                      else
                      {
                        Serial.println("Not enough angle change Turn 60");
                        //rotate and try the whole thing again
                        doTurn(60);
                      }
                    }
                }
                else
                {
                   //either robot isnt seen or is seen by both 
                }
                break;
              //we are on the right side
              case RIGHTSIDE:
                //Make sure the robot is still only seen by the one camera                  
                if(getRobotSeen()==RIGHT_CAMERA_ONLY || getRobotSeen()==RIGHT_CAMERA_ONLY_1)
                {
                  //check if moving the robot has moved the angle up enough since before movement
                  if(cameraAngle>getRightAngle()+LOCALIZATION_ANGLE_CHANGE)
                  {
                     //We are facing outward, we know where we are
                     roboDirection=KNOWN;              
                     Serial.println("Robots on the Right Oriented");
                  } 
                  else
                  {
                     Serial.println("Not oriented well Move Backwards");
                     //Record new init angle
                     cameraAngle=getRightAngle();
                     //move in the opposite direction
                     newEncoders(-30);
                     
                      //MAY WANT A DELAY HERE FOR WII CAMERA ACQUISITION
                     
                     //if this move was in the outward direction
                     if(cameraAngle>getRightAngle()+LOCALIZATION_ANGLE_CHANGE)
                     {
                         Serial.println("Turn Around");
                         //turn around since we are backward
                         doTurn(180);
                         //exit the system
                         roboDirection=KNOWN;
                         Serial.println("Robots on the Right Oriented");
                     }
                     //if the angle still didnt change enough to record a change
                     else
                     {
                        Serial.println("Not enought angle change Turn 60");
                         //rotate and try the whole thing again
                         doTurn(60);
                     }
                  }
                }
                else
                {
                   //either robot isnt seen or is seen by both 
                }
                break;
          }
       macroCommunicationsUpdate();
     }
     
     //Break out of the macro if necessary
     if(getStoredMacroCommand()==0) return (getStoredMacroCommand()!=0);
   }
   
  return (getStoredMacroCommand() != 0);
}

#endif
