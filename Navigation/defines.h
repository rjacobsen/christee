#ifndef DEFINES_H
#define DEFINES_H

#define LIDARCODE
//NEW FORWARD BACKWARD DRIVE LIMITS
#define MOTOR_DRIVE_MAG_LIMIT 28   //With Sand 22  //WITHOUT SAND 18

/*************************** ADDRESSING ********************************/
#define CONTROL_ADDRESS                               5
#define NAVIGATION_ADDRESS                            4
#define PIC_ADDRESS                                   1
#define MOTOR_ADDRESS                                 6
#define LED_ADDRESS                                   2
#define BEACON_ADDRESS	                              3

/********************************** LED ********************************/
/****************** REGISTERS SEND *******************/
#define STATE                                         1
#define COLOR_PICKER                                  2
#define UPDATE_FLAG                                   3

/****************** REGISTER VALUES *******************/
#define MANUAL                                        3
#define MACRO                                         2
#define AUTO                                          2

/****************************** CONTROL BOX *****************************/
/*********************** REGISTERS SEND ***************/
#define LAST_BOARD_ADDRESS_RECEIVE                    0
#define MACRO_COMMAND_RECEIVE                         1
#define MACRO_SUB_COMMAND_RECEIVE                     2

#define LIDAR_HEADING                 22  
#define LIDAR_DISPLACEMENT            23
#define LIDAR_ANGLE                   24
#define LIDAR_DISTANCE                25

/*********************** REGISTERS RECIEVE ***********/
#define MACRO_COMMAND_SEND                            1
#define MACRO_SUB_COMMAND_SEND                        2
#define GYRO                                          4
#define LEFT_MOTOR                                    5
#define RIGHT_MOTOR                                   6

/********************************** MOTOR ********************************/
#define ACTUATOR_ANGLE                                4

/****************** REGISTERS SEND *******************/
#define COMMSPEED_MOTOR_SEND                          0
#define LEFTMOTOR_MOTOR_SEND                          1
#define RIGHTMOTOR_MOTOR_SEND                         2
#define ACTUATOR_MOTOR_SEND                           3

/********************************** PIC ********************************/
/***************** REGISTERS RECIEVE *****************/
#define ENCODER_R_L_PIC_RECEIVE                       5
#define ENCODER_L_L_PIC_RECEIVE                       6
#define ENCODER_SPEED_R_PIC_RECEIVE                   7
#define ENCODER_SPEED_L_PIC_RECEIVE                   8
#define ENCODER_R_H_PIC_RECEIVE                       23
#define ENCODER_L_H_PIC_RECEIVE                       24
#define GYRO_X_ANGLE_PIC                              25
#define GYRO_Y_ANGLE_PIC                              26
#define GYRO_Z_ANGLE_PIC                              27
#define GYRO_IMPACT_PIC                               28
#define GYRO_RAW                                      29
typedef union{
    int32_t joined;
    struct {      
      uint16_t low;
      uint16_t high;
    }endian;
}_16_to_32_t;

/********************************** BEACON ********************************/
/****************** REGISTER VALUES *******************/
#define CANT_SEE_CAMERAS                              0
#define LEFT_CAMERA_ONLY_1                            1
#define RIGHT_CAMERA_ONLY_1                           2
#define LEFT_CAMERA_ONLY                              3
#define RIGHT_CAMERA_ONLY                             4 
#define BOTH_CAMERAS_1                                5
#define BOTH_CAMERAS                                  6
#define XY_FOUND                                      7  


#define ROBOT_SEEN_VALUE                              9
#define WII_ANGLE_LEFT                                10
#define WII_ANGLE_RIGHT                               11
#define WII_X                                         12
#define WII_Y                                         13
#define WII_DIST_LEFT                                 14
#define WII_DIST_RIGHT                                15

#ifdef LIDARCODE
  /************** LIDAR REGISTER LOCATIONS ***********/
  #define LIDAR_COMMAND_INDEX                         1
  
  #define LIDAR_OBJ_ANGLE                             1
  #define LIDAR_OBJ_HEADING                           2
  #define LIDAR_OBJ_DISPLACEMENT                      3
  #define LIDAR_OBJ_MAG                               4
  #define LIDAR_OLD_OBJ_DATA_REQUEST                  5
  
/****************** REGISTER VALUES *******************/
  #define LIDAR_IDLE_STATE                            0
  #define LIDAR_GET_HEADING                           1
  #define LIDAR_GET_LEAPFROG_HEADING                  2
  #define LIDAR_CONTINUOUS_LEAPFROG_OBJ_CAPTURE       3
  #define LIDAR_CONTINUOUS_OBJ_CAPTURE                4
#endif




#endif
