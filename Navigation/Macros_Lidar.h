
void testLidarInterface(void);
void LIDARAcquireHeading(void);
void LIDARTraverseToCenter(void);
void LIDARsetHeading90(void);
void LIDARsetAngle90_2(bool collectionBin);

void LIDARApproachCollection(void);
int getLastDistUsed(void);
int getLastHeadingUsed(void);
int getLastDispUsed(void);
int getLastAngleUsed(void);
void captureDataLidarForDisplay(void);
void getLidarDataInit(void);
void getLidarDataUpdate(void);
