//Control the motor signal via feedback from the encoders

#ifndef MOTOR
#define MOTOR
#include <Arduino.h>
/*
 simpleMotorDistanceCommand(signed long commandedDistance);
 
  This method takes in a distance (CM) that the treads are supposed to move. ( IN A STRAIGHT LINE )
    It then iterates 10 times before sending the accumlation of control variables to a second method that actually sends the motor controller the command
    
    This essentially comes up with the speed for both treads to move while the cascaded method figures out if:
          they both move the same speed (meaning drive straight)
                    or
          they should be commanded different speeds (due to non equal tread positions)
*/
void simpleMotorDistanceCommand(signed long commandedDistance);

void simpleMotorDistanceLRDiffCommand(signed long commandedSpeed, signed long commandedDistance);
float grabFloatSign(float i);
int grabIntegerSign(signed long i);

void simpleMotorDistanceCommand(signed long commandedDistance);
//Meters a speed input into a variable turning capable differential, allows for equal distance as we go on both treads
//    WILL act as the development for diff driving with new encoders.
void simpleMotorDistanceLRDiffCommand(signed long commandedSpeed, signed long commandedDistance);
int grabIntegerSign(signed long i);
float grabFloatSign(float i);
#endif
