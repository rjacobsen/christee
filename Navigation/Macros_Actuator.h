//macros_actuator.h
#ifndef MACROS_ACTUATOR_H
#define MACROS_ACTUATOR_H
#include "Macros_Encoders.h"

#define BUCKET_DIG_DRIVE_BEGIN_ANGLE 15
#define BUCKET_DRIVE_ANGLE_SET   40
#define BUCKET_DIG_ANGLE_SET     0
#define BUCKET_DUMP_ANGLE_SET    91
#define DIG_DRIVE_DISTANCE       75
#define DELAY_BUCKET_EMPTY 2500

bool sendActuatorPositionFeedback(int angle);
bool sendActuatorPositionDig(int angle);

#endif

