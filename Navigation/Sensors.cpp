#include "Sensors.h"
#include "Variables.h"

float lastGyroAngle=0;
void wipeGyro()
{
  setMacroAngle(0);
}

void updateMPU()
{
    setMacroAngle(getMacroAngle()+getAngleY()-lastGyroAngle);
    lastGyroAngle=getAngleY();
}
