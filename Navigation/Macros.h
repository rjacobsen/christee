//macros.h
//3-2-14 STACEE can walk in circles (   almost :)  )
//3-25-14 STACEE can do forward/mine/backward/dump
//1-11-17 CHRISTEE can basically see her self and center herself (WII)
#ifndef MACROS_H
#define MACROS_H
#include "Macros_Encoders.h"
#include "Variables.h"
#include "Sensors.h"
#include "Macros_Wii.h"
#include "Macros_Actuator.h"
#include "Macros_Lidar.h"

//TUNING FOR FULLY AUTONOMOUS DRIVE CHARACTERISTICS
#define POST_SENSOR_FORWARD_TRAVERSAL_DISTANCE     200
#define PRE_SENSOR_REVERSE_TRAVERSAL_DISTANCE      300


//Macro_commands
#define MACRO_SNIPPIT 4
#define ENCODER_SNIPPIT 6
#define ACTUATOR_SNIPPIT 7

        
#define TRAVERSE_FORWARD      0
#define DROPPING_BUCKET       1
#define DIGGING_FORWARD       2
#define DIGGING_REVERSE       3
#define TRAVERSE_BACKWARD     4
#define DUMPING_BUCKET        5
#define BUCKET_TO_DRIVE	      6

//AUTO CHARACTERISTICS
#define DRIVE_INC_NUM_FORWARD 1  //600
#define DRIVE_DIG_FORWARD     75
#define DRIVE_DIG_REVERSE     25
#define DRIVE_INC_NUM_REVERSE 1  //550
#define ACTUATOR_DUMP_ANGLE   91

typedef enum{
  LOCALIZE_ROBOT=0,
  TRAVERSE_TO_CENTER,
  MIGRATE_TO_DIGGING_ASSISTED,
  MIGRATE_TO_DIGGING_BLIND,
  DROP_BUCKET,
  DIG_FORWARD,
  DIG_REVERSE,
  MIGRATE_TO_COLLECTION_BLIND,
  MIGRATE_TO_COLLECTION_ASSISTED,
  FINALIZE_MOVE_TO_COLLECTION_BIN,
  DUMP_BUCKET,
  NUM_AUTO_STATES  
}AUTO_STATES_t;

 bool straightPathMineDump();
 bool straightPathMineDumpStart();
 bool orientWithWii();
void fullRoutine();
void driveDigDistance();
void fiftyForwardFiftyBackward();
void squareRoutine();
 void initMacroSystem();



#endif
