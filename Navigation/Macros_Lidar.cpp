
#include "Macros_Lidar.h"
#include <Timers.h>
#include "Defines.h"
#include "Variables.h"
#include "Comms.h"
#include "Macros_Encoders.h"

#define EXTRA_DUMP_POSITION_FIXER           //Do exxtra steps during approach of the robot to collection bin
//#define USE_ANGLE_FOR_LEFT_VS_RIGHT       // Use the angle for the left vs right side identification, or use cartesian
#define POSSIBLY_SKIP_CENTERING           //If when localizing, if the x coord is close to center, it will make the system not bother correcting
//#define MOVE_AWAY_FROM_WALL           //Upon localization if the y coord is low, the robot will attempt to move away

#define TRAVERSE_FORWARD  true
#define TRAVERSE_BACKWARD false
#define ANGLE0            false
#define ANGLE180          true

#define LIDAR_LEFT_RIGHT_SIDE_ANGLE       145 //Angle that says if the robot is on the right or left side 
#define LIDAR_TRAVERSE_LOCALIZE_DISTANCE   50 //cm
#define LIDAR_X_LOCATION_OFFSET           750//mm

#define CENTERED_X_TOLERANCE 400
#define APPROACH_COLLECTION_Y_COMPLETE    500
#define APPROACH_COLLECTION_X_COMPLETE   500

double LIDARxCoord, LIDARyCoord;
int traverseAngleToTurn;
bool angleTurn;

int lastDistUsed, lastHeadingUsed, lastDispUsed, lastAngleUsed;

int getLastDistUsed(void)
{
 return lastDistUsed; 
}
int getLastHeadingUsed(void)
{
  return lastHeadingUsed;
}
int getLastDispUsed(void)
{
  return lastDispUsed;
}
int getLastAngleUsed(void)
{
  return lastAngleUsed;
}

void captureDataLidarForDisplay(void)
{
  lastDistUsed=getRobotDistance();
  lastAngleUsed=getRobotAngleFromLidar();
  lastDispUsed = getRobotMovementDistance();
  lastHeadingUsed = getRobotHeading();
}
  
Timers waitTimer(1000);
Timers commsTimerMacro(5);
Timers lidarLocalizeTimer(250);

//TEST ROUTINE FOR LOCALIZING FROM START
typedef enum{
  initStateTest=0,
  moveStateTest,
  positionHeadingTest,
  endStateTest
}state_machine_lidar_test_t;

//State Machine for LIDAR test
state_machine_lidar_test_t state_machine_lidar_test = initStateTest;

//FINAL ROUTINE FOR LOCALIZING FROM START
typedef enum{
  initState=0,
  moveState,
  positionHeading,
  endState
}state_machine_lidar_heading_acquisition_t;

state_machine_lidar_heading_acquisition_t state_machine_lidar_heading_aquisition = initState;

double x1, y1,x2,y2,head;
void getLidarDataInit(void)
{
  macroCommunicationsUpdate();
  setRobotAngleFromLidar(0);
  while(getRobotAngleFromLidar() == 0 && getStoredMacroCommand()!=0)
  {
    //Set the initial location by sending to lidar  
    sendLIDARPosHeading();
    
    waitTimer.resetTimer();      //Reset the wait timer
    while( !waitTimer.timerDone() )
    {
      if(commsTimerMacro.timerDone())
        macroCommunicationsUpdate();   //Update comms while we wait
    }  
  }
  x1  = (double)getRobotDistance() * cos(((double)getRobotAngleFromLidar()) * PI/180.0) + LIDAR_X_LOCATION_OFFSET;
  y1  = (double)getRobotDistance() * sin(((double)getRobotAngleFromLidar()) * PI/180.0);
  
}

void getLidarDataUpdate(void)
{
  macroCommunicationsUpdate();
  setRobotAngleFromLidar(0);
  while(getRobotAngleFromLidar() == 0 && getStoredMacroCommand()!=0)
  {
    //Request next location by sending to lidar  
    sendLIDARRequestDataOnly();
    
    waitTimer.resetTimer();      //Reset the wait timer
    while( !waitTimer.timerDone() )
    {
      if(commsTimerMacro.timerDone())
        macroCommunicationsUpdate();   //Update comms while we wait
    }  
  }
   x2  = (double)getRobotDistance() * cos(((double)getRobotAngleFromLidar()) * PI/180.0) + LIDAR_X_LOCATION_OFFSET;
   y2  = (double)getRobotDistance() * sin(((double)getRobotAngleFromLidar()) * PI/180.0);
   head=atan2((y2-y1),(x2-x1))*180.0/PI;
   if(head<0)
    head=head+360;
   setRobotHeading(head);
   
   captureDataLidarForDisplay();
}

typedef enum{
  takeInit,
  moveF,
  checkF,
  moveB,
  checkB
}FBStateMachine_t;

#define FORWARD_ORIENT true
#define BACKWARD_ORIENT false

FBStateMachine_t FB = takeInit;
bool determineForwardBackwardMovement(void)
{
  switch(FB)
  {
    case takeInit:
      getLidarDataInit();
      FB=moveF;
      break;
    case moveF:
      newEncoders(15);
      getLidarDataUpdate();
      FB=checkF;
      break;
    case checkF:
      if(y2>y1)
      {
        //We are moving in the forward direction (should check the x coords to determine if we need to turn to avoid wall)
          //At this point it seems like we should just do a heading calc...
        return FORWARD_ORIENT;
      }
      else
      {
        FB= moveB;
        getLidarDataInit();
      }
      break;
    case moveB:
      newEncoders(-15);
      getLidarDataUpdate();
      FB=checkB;
      break;
    case checkB:
      if(y2>y1)
      {
        //We are moving in the forward direction (should check the x coords to determine if we need to turn to avoid wall)
          //At this point it seems like we should just do a heading calc...
        
        return BACKWARD_ORIENT;
      }
      else
      {
        Serial.println("Neither moving forward or backward the dist made the heading work");
        //??? What is going on
      }

      break;
  }
}


Timers lidarTestTimer(250);
void testLidarInterface(void)
{
  //Set the initial state of system
  state_machine_lidar_test=initStateTest;
  
  //Reset the comms timer
  commsTimerMacro.resetTimer();
  
  //Reset the TestTimer
  lidarTestTimer.resetTimer();
  
  //While the system is to continue
  while(getStoredMacroCommand()!=0)
  {
    #ifdef LIDARCODE
    if(lidarTestTimer.timerDone() && getStoredMacroCommand()!=0)
    {
      switch(state_machine_lidar_test)
      {
        case initStateTest:          
          getLidarDataInit();
          state_machine_lidar_test=moveStateTest;
          lidarTestTimer.resetTimer();
          break;
        case moveStateTest:
          newEncoders(LIDAR_TRAVERSE_LOCALIZE_DISTANCE);
          state_machine_lidar_test=positionHeadingTest;
          lidarTestTimer.resetTimer();
          break;
        case positionHeadingTest:
          getLidarDataUpdate();
         
          lidarTestTimer.resetTimer();
          state_machine_lidar_test=endStateTest;//moveStateTest2;
          break;
        case endStateTest:
          setStoredMacroCommand(0);
          break;          
      }
    }
    #endif
    if(commsTimerMacro.timerDone())
      macroCommunicationsUpdate();
  }
  state_machine_lidar_test=initStateTest;
}

  bool localizing =true;
  
//MAKE UPDATES IF THE TEST METHOD CHANGES
void LIDARAcquireHeading(void)
{
  //Reset the localizing timer
  lidarLocalizeTimer.resetTimer();
  
  //Reset the comms timer
  commsTimerMacro.resetTimer();
  
  //Set the initial state of the system
  state_machine_lidar_heading_aquisition=initState;
  
  //Set that the function is localizing
  localizing =true;
  
  //Exit conditions
  while(getStoredMacroCommand()!=0 && localizing)
  {
    #ifdef LIDARCODE
    if(lidarLocalizeTimer.timerDone() && getStoredMacroCommand()!=0)
    {
      switch(state_machine_lidar_heading_aquisition)
      {
        case initState:        
          //Set the initial location by sending to lidar  
          getLidarDataInit();  
          
          //Move to next state
          state_machine_lidar_heading_aquisition=moveState;
          lidarLocalizeTimer.resetTimer();
          break;
        case moveState:
          newEncoders(LIDAR_TRAVERSE_LOCALIZE_DISTANCE);
          
          //Move to next state
          state_machine_lidar_heading_aquisition=positionHeading;
          lidarLocalizeTimer.resetTimer();
          break;
        case positionHeading:
          
          getLidarDataUpdate();
          
          captureDataLidarForDisplay();

          #ifdef MOVE_AWAY_FROM_WALL
          LIDARyCoord  = (double)getRobotDistance() * sin(((double)getRobotAngleFromLidar()) * PI/180.0);
          
          //If we are very close to the wall
          if(LIDARyCoord < 400)
          {
            if(getRobotHeading() > 180)          //We are heading toward the wall
            {
              newEncoders(-50);                    //Give us some space
            }
            else if(getRobotHeading() < 180)     //We are heading away from the wall
            {
              newEncoders(50);                     //Give us some space
            }
          }
          #endif
          //Move to next state
          state_machine_lidar_heading_aquisition=endState;
          
          //Reset timer
          lidarLocalizeTimer.resetTimer();
          break;
        case endState:
          //Tell system that we are done localizing
          localizing= false;
          
          //Reset timer
          lidarLocalizeTimer.resetTimer();
          
          //Move to next state
          state_machine_lidar_heading_aquisition=initState;
          break;          
      }
    }
    #endif
    if (commsTimerMacro.timerDone())
      macroCommunicationsUpdate();
  }
  macroCommunicationsUpdate();
}

Timers updateLocation(20);
Timers sendForLIDARUpdate(200);
Timers communicationsCheckTimer(2);
Timers doStuffLIDARTimer(200);
bool didStuff = false;
int prevX, prevY;

/* This is made to allow us to move from a position that is down in the obstacle or start area away from the collection bin
    at the end of this function, we should be within a half meter of the center(X), within half a meter of the collection bin(Y) */
void LIDARApproachCollection(void)
{
  LIDARsetAngle90_2(false);
 
  getLidarDataInit();
          
  //X coord relative to LIDAR plus the offset of the LIDAR in the arena (center of arena is x=0)
  LIDARxCoord  = (double)getRobotDistance() * cos(((double)getRobotAngleFromLidar()) * PI/180.0) + LIDAR_X_LOCATION_OFFSET;
  LIDARyCoord  = (double)getRobotDistance() * sin(((double)getRobotAngleFromLidar()) * PI/180.0);
   
  //Okay so here we really want the robot to be facing directly outward, and the x coord is actually going to be targeted at 0 based on the calculation above
  //      We should actually look for hitting a y coordinate that is not zero, then using the gyro and encoders to approach the bucket from a known position.
  
   while((getStoredMacroCommand()!=0) && (LIDARyCoord > APPROACH_COLLECTION_Y_COMPLETE || !isInRange(LIDARxCoord,0,APPROACH_COLLECTION_X_COMPLETE))) //So while x is not satisfied or y is not satisfied, continue
  {
    if (communicationsCheckTimer.timerDone())     macroCommunicationsUpdate();  //Check for incomming packets
    if (updateLocation.timerDone())       
    {
      /* An attempt to make sure that the next time that we 'doStuff' we are acting on new data */
      if(didStuff)  //If we most recently moved the robot
      {
        getLidarDataUpdate();
        LIDARxCoord  = (double)getRobotDistance() * cos(((double)getRobotAngleFromLidar()) * PI/180.0) + LIDAR_X_LOCATION_OFFSET;
        LIDARyCoord  = (double)getRobotDistance() * sin(((double)getRobotAngleFromLidar()) * PI/180.0);
        didStuff=false;
      }
    }
    
    if(doStuffLIDARTimer.timerDone() && !didStuff)
    {
      //Serial.println();
      if(LIDARyCoord > APPROACH_COLLECTION_Y_COMPLETE && !isInRange(LIDARxCoord,0,APPROACH_COLLECTION_X_COMPLETE)) // Both conditions not met
      {
        //Turn to correct the offset in x
        if(LIDARxCoord > 0)
        {
          doTurn(-45);
        }
        else 
        {
          doTurn(45);
        }
        //Move a distance to try to correct the x
        newEncoders(-abs(LIDARxCoord/10.0));
        
        //Turn back towards orig heading
        if(LIDARxCoord > 0)
        {
          doTurn(45);
        }
        else 
        {
          doTurn(-45);
        }
      }
      else if(LIDARyCoord > APPROACH_COLLECTION_Y_COMPLETE)                              //Only the y coord condition needs met
      {
        #ifndef EXTRA_DUMP_POSITION_FIXER
        newEncoders((-(LIDARyCoord-APPROACH_COLLECTION_Y_COMPLETE)/10.0)+5);  //Move backwards, the difference between Ycoord and 500 (5 less than this figure)
        LIDARyCoord = APPROACH_COLLECTION_Y_COMPLETE-1;
        #else
        newEncoders((-(LIDARyCoord-APPROACH_COLLECTION_Y_COMPLETE)/10.0)+50);  //Move backwards, the difference between Ycoord and 500 (50 less than this figure)
        LIDARyCoord = APPROACH_COLLECTION_Y_COMPLETE-1;
        #endif
      }
      else if(!isInRange(LIDARxCoord,0,APPROACH_COLLECTION_X_COMPLETE))                //Only the x coord condition needs met
      {
         //Turn to correct the offset in x
        if(LIDARxCoord > 0)
        {
          doTurn(-90);
        }
        else 
        {
          doTurn(90);
        }
        
        //Move a distance to try to correct the x
        newEncoders(-abs(LIDARxCoord/10.0));
        
        //Turn back towards orig heading
        if(LIDARxCoord > 0)
        {
          doTurn(90);
        }
        else 
        {
          doTurn(-90);
        }
        
      }
      getLidarDataInit();
      //Only the x coord condition needs met
      didStuff=true;
      //Reset the timer, so we have to wait for an update from the lidar before doing something else
      sendForLIDARUpdate.resetTimer();
      
    }//doStuffTimer   
    
  }//While stuff to do 

  #ifdef EXTRA_DUMP_POSITION_FIXER
    //Get Heading Now
    LIDARAcquireHeading();
    //Move to Center
    LIDARTraverseToCenter();
    //Set the direction to 90 degrees so we dump right
    LIDARsetAngle90_2(false);
  
    //make sure we have up to date location info
    getLidarDataInit();
  
    //Record the Y coord of the robot
    LIDARyCoord  = (double)getRobotDistance() * sin(((double)getRobotAngleFromLidar()) * PI/180.0);
    
    //Effectively move 5 cm more than LIDAR says we are out away from the dump location
    newEncoders(-(LIDARyCoord/10.0)-5);
    
  #else
    //Lastly we move to the collection bin
    newEncoders(-66);
  #endif
}//Method wrapper


typedef enum{
  initialMove=0,
  setInitialHeadingLocation,
  moveForward,
  checkHeadingF,
  correctHeadingF,
  moveForwardVerify,
  moveBackward,
  checkHeadingB,
  correctHeadingB,
  moveBackwardVerify,
  waitForDataInit,
  waitForDataUpdate
}setHeadingPerpendicular_t;

int unableToGetInRange=0;
setHeadingPerpendicular_t nextStateToRun;
setHeadingPerpendicular_t stateMachinePerpendicular;

void LIDARsetAngle90_2(bool collectionBin)
{
  //Counter to make sure we dont get stuck if angle is really wrong
  unableToGetInRange=0;
  if(collectionBin)
    //Initialize state machine
    stateMachinePerpendicular = initialMove;
  else
  {    
    //Initialize state machine
    stateMachinePerpendicular = moveBackward;
  }
  //set the next state to run
  nextStateToRun = setInitialHeadingLocation;
  //zero the heading for new data to have to come in
  setRobotHeading(0);
  
  //WHILE -- macro is running ---   AND  ( angle is not  ( close to 90 or 270 ) )
  while((getStoredMacroCommand()!=0) && !(isInRange(getRobotHeading(),90,5) || isInRange(getRobotHeading(),270,5)))
  {
      switch(stateMachinePerpendicular)
      {
         case initialMove:        //Move away from the begininng
           newEncoders(100);
           stateMachinePerpendicular = setInitialHeadingLocation;
           break;
           
         case setInitialHeadingLocation:
           stateMachinePerpendicular = waitForDataInit;  //Wait for this data to be taken in
           nextStateToRun = moveForward;              //Then we will move forward 
           break;
           
         case moveForward:
           newEncoders(50);                          //Move forward
           stateMachinePerpendicular = waitForDataUpdate;  // Send for data and 
           nextStateToRun = checkHeadingF;            //Then check the heading
           break;
           
         case checkHeadingF:
           if(unableToGetInRange < 4)                        //If we havent tried to correct too many times
           {
            
             if(isInRange(getRobotHeading(),90,25))          //If the heading is reasonably inline with 90
               stateMachinePerpendicular = correctHeadingF;  //Correct the heading
             else
             {
               stateMachinePerpendicular = waitForDataInit;    //Other wise move backward and check for that heading
               nextStateToRun = moveBackward;
               unableToGetInRange++;
             }
           }
           else
           {
               stateMachinePerpendicular = correctHeadingF;  //Correct the heading
           }
           break;
           
         case correctHeadingF:
           doTurn(90-getRobotHeading());                     //Correct the heading
           setRobotHeading(90);                              //Get us out of this method (FINISHED)
           stateMachinePerpendicular = moveBackwardVerify;   //Move again and verify heading
           break;
           
         case moveForwardVerify:
           newEncoders(50);
           stateMachinePerpendicular = waitForDataUpdate;  //Wait for data, if the data shows good, then we are done
           nextStateToRun = moveBackward;            //If it fails, then we need to move backward
           break;
           
         case moveBackward:
           newEncoders(-50);                          //Move forward
           stateMachinePerpendicular = waitForDataUpdate;  // Send for data and 
           nextStateToRun = checkHeadingB;            //Then check the heading
           break;
           
         case checkHeadingB:
           
           if(unableToGetInRange < 4)                        //If we havent tried to correct too many times
           {
        
             if(isInRange(getRobotHeading(),270,25))          //If the heading is reasonably inline with 270
               stateMachinePerpendicular = correctHeadingB;    //Correct the heading based on the heading we have
             else
             {
               stateMachinePerpendicular = waitForDataInit;        //Next we move forward (bad heading)
               nextStateToRun = moveForward;
               unableToGetInRange++;
             }
           }
           else
           {
             stateMachinePerpendicular = correctHeadingB;    //Correct the heading based on the heading we have
           }
           
           break;
           
         case correctHeadingB:
           doTurn(270-getRobotHeading());            //Correct the angle based on the heading
           setRobotHeading(270);                    //Get us out of this method (FINISHED)
           stateMachinePerpendicular = moveForwardVerify;            //If it fails, then we need to move backward
           break;
           
         case moveBackwardVerify:
           newEncoders(-50);                          //Move backward to check our new heading
           stateMachinePerpendicular = waitForDataUpdate;  //Wait for data, if the data shows good, then we are done
           nextStateToRun = moveForward;            //If it fails, then we need to move Forward
           break;
           
         case waitForDataInit:
           getLidarDataInit();          
           stateMachinePerpendicular = nextStateToRun;
           break;
         case waitForDataUpdate:
          getLidarDataUpdate();
          captureDataLidarForDisplay();
          stateMachinePerpendicular = nextStateToRun;
          break;
      } //End switch
      if (commsTimerMacro.timerDone())
               macroCommunicationsUpdate();
  }
    
}

  
  int headingFromLocalizing;
  
void LIDARTraverseToCenter(void)
{  
  headingFromLocalizing = getRobotHeading();    //Store the localizing heading (the robot may have moved away from wall before getting here)
  
  getLidarDataInit();
  
  //X coord relative to LIDAR plus the offset of the LIDAR in the arena (center of arena is x=0)
  LIDARxCoord  = (double)getRobotDistance() * cos(((double)getRobotAngleFromLidar()) * PI/180.0) + LIDAR_X_LOCATION_OFFSET;
  LIDARyCoord  = (double)getRobotDistance() * sin(((double)getRobotAngleFromLidar()) * PI/180.0);
  
  lastAngleUsed=getRobotAngleFromLidar();
  if(headingFromLocalizing < 90 || headingFromLocalizing > 270) //We are heading in Q1 or Q4 - Setting Angle to 0
  {
    if(headingFromLocalizing < 90) //Q1
    {
      traverseAngleToTurn = -headingFromLocalizing; 
      angleTurn = ANGLE0;
    }
    else //Q4 - 270-360
    {
      traverseAngleToTurn = 360 - headingFromLocalizing; 
      angleTurn = ANGLE0;
    }
  }
  else //We are heading in Q3 or Q2
  {
    if(headingFromLocalizing > 90 && headingFromLocalizing < 180) //Q2 - 90-180
    {
      traverseAngleToTurn = 180 - headingFromLocalizing ; 
      angleTurn = ANGLE180;
    }
    else //Q3 - 180-270
    {
      traverseAngleToTurn =  headingFromLocalizing - 180; 
      angleTurn = ANGLE180;
    }
  }
  
  //THIS ASSUMES THAT WE ARE ON THE RIGHT SIDE OF SENSOR (RIGHT_START_POSITION)
  #ifdef USE_ANGLE_FOR_LEFT_VS_RIGHT
    if( getRobotAngleFromLidar() <= LIDAR_LEFT_RIGHT_SIDE_ANGLE)
  #else
    if( LIDARxCoord > 0 && !isInRange(LIDARxCoord, 0 , CENTERED_X_TOLERANCE))
  #endif
  {
    doTurn(traverseAngleToTurn);      //turn towards the center
    if(angleTurn == ANGLE180)  
    {
      newEncoders(LIDARxCoord/10.0);  //drive forwards towards the center 
      doTurn(-90);                    //rotate outwards
    }   
    else
    {
      newEncoders(-LIDARxCoord/10.0);  //drive backwards towards the center 
      doTurn(90);                        //rotate outwards
    } 
  }
  #ifdef POSSIBLY_SKIP_CENTERING
    //THIS ASSUMES THAT WE ARE ON THE LEFT SIDE OF SENSOR (LEFT START POSITION)
    else if(!isInRange(LIDARxCoord, 0 , CENTERED_X_TOLERANCE))
  #else 
    else
  #endif
  { 
    doTurn(traverseAngleToTurn);      //turn towards the center
    if(angleTurn == ANGLE180)  
    {
      newEncoders(LIDARxCoord/10.0);  //drive backwards towards the center 
      doTurn(-90);                    //rotate outwards
    }   
    else
    {
      newEncoders(-LIDARxCoord/10.0);  //drive forward towards the center 
      doTurn(90);                    //rotate outwards
    } 
  }
}


//RANDOM SETS OF DEBUG PRINTOUTS

//  Serial.println();
//  Serial.print("Robot location is x: ");
//  Serial.print(LIDARxCoord);
//  Serial.print(", y: ");
//  Serial.println(LIDARyCoord);
//  Serial.print("Heading: ");
//  Serial.println(getRobotHeading());
      
//      Serial.println();
//      Serial.print("Robot location is x: ");
//      Serial.print(LIDARxCoord);
//      Serial.print(", y: ");
//      Serial.println(LIDARyCoord);          
//          Serial.println("Moved, have new values");
//          Serial.print("Oldx: ");
//          Serial.print(prevX);
//          Serial.print(" oldy: ");
//          Serial.println(prevY);
//          Serial.print("newX: ");
//          Serial.print(LIDARxCoord);
//          Serial.print(" newY: ");
//          Serial.println(LIDARyCoord);
        //Serial.print("Fixing Y moving: ");
        //Serial.println((-(LIDARyCoord-APPROACH_COLLECTION_Y_COMPLETE)/10.0)+5);

