//macros_encoders.h

#ifndef MACROS_ENCODERS
#define MACROS_ENCODERS

#include "Macros_Gyro.h"
#include <PID.h>
#include "Motor.h"
#include <Timers.h>
#include "Variables.h"

#define encoderKp 1
#define encoderKi 0
#define encoderKd 0
//Encoder acceptable steady state error (Acceptable difference between commanded value and the value that means we are done)
#define DEADZONE_ENCODER 15

void updateMacroEncoderValueR(signed long increment);
void updateMacroEncoderValueL(signed long increment);
bool isInRange(signed long checkNum, signed long target, signed long range);
void newEncoders(signed long cm);
void wipeEncoders();


#endif
