#include "Macros.h"

bool flipflop=true;

bool fullyAutonomousSystem(void);

void initMacroSystem()
{
  setContinuable(true);
  sendLEDstate(AUTO);  
  sendLEDstate(AUTO);  
  sendControlMacroEntered();
  wipeGyro();
  //Serial.println("StartingMacro");
  if (getStoredMacroCommand() == 1)
  {
    while (getStoredMacroCommand() != 0)
    {
      straightPathMineDump();
    }
  }
  else
  {
//    Serial.print("Stored Macro: ");
//    Serial.println(stored_macro_command);
//    Serial.print("Macro_sub_command: ");
//    Serial.println(macro_sub_command);
//    Serial.print("Control Macro: ");
//    Serial.println(navigation_receive[MACRO_COMMAND_RECEIVE]);
//    Serial.print("Control Macro_sub_command: ");
//    Serial.println(navigation_receive[MACRO_SUB_COMMAND_RECEIVE]);
    static Timers commTime(2);
    switch (getStoredMacroCommand())
    {
      case 2:
        newEncoders(1000);
        break;
      case 3:
        newEncoders(-1000);
        break;
      case MACRO_SNIPPIT:
        switch (getMacroSubCommand())
        {
          case 1:
             testLidarInterface();
            break;
          case 2:
            while(getStoredMacroCommand()!=0)
            {
              static Timers LIDAR_Test_timer(1000);
              if(LIDAR_Test_timer.timerDone())
              {
                sendLIDARPosHeading();
              }
              if(commTime.timerDone())
              {
                macroCommunicationsUpdate();
                captureDataLidarForDisplay();
              }
            }
            break;
          case 3:  
             LIDARAcquireHeading();
             LIDARTraverseToCenter();
            break;
          case 4:
            LIDARsetAngle90_2(true);
            break;
          case 5:
            fullyAutonomousSystem();
            break;
          case 6:
            doTurn(5);
            break;
          case 7:
          
            break;
          case 8:
          
            break;
          case 9:
          
            break;
          case 10:
            fiftyForwardFiftyBackward();
            break;
          case 11:
            squareRoutine();
            break;
          case 12:

            break;
            //CW
          case 13:
           // doTurn(90);
            break;
          case 14:
            //doTurn(-90);
            break;
          case 15:
            newEncoders(1000);
            break;
        }
        break;
      case 5:
        wipeEncoders();
        wipeGyro();
        break;
      case ENCODER_SNIPPIT:
        flipflop=true;
        while(getStoredMacroCommand()!=0)
        {
          static Timers redoTimer(2500);
          
          if(redoTimer.timerDone())
          {
            if(flipflop)
            {
            newEncoders((signed long)getMacroSubCommand());
            flipflop=false;
            }
            else
            {            
            newEncoders((signed long)-getMacroSubCommand());
            flipflop=true;
            }
            redoTimer.resetTimer();
          }
          else
          {
            macroCommunicationsUpdate();
          }
        }
        break;
      case 7:
        if (getMacroSubCommand() >= 0 && getMacroSubCommand() <= 90)
          sendActuatorPositionFeedback(getMacroSubCommand());
        else if (getMacroSubCommand() == 254)
          sendActuatorPositionFeedback(75);
        else if (getMacroSubCommand() == 253)
          sendActuatorPositionFeedback(5);
        break;
      case 8:
      
        break;
      case 9:
      
        break;
        
    }
  }
//    Serial.print("Ending Stored Macro: ");
//    Serial.println(getStoredMacroCommand());
//    Serial.print("Macro_sub_command: ");
//    Serial.println(getMacroSubCommand());

  terminateMacroSystem();
  sendLEDstate(MANUAL);
  sendLEDstate(MANUAL);  
}


Timers waitForLidar(500);
Timers dumpTimer(6000);


AUTO_STATES_t AutonomousState = LOCALIZE_ROBOT; 
bool fullyAutonomousSystem(void)
{
  // !!!! IN THE FUTURE, MAYBE RECOVER THIS STATE FROM THE EEPROM ON CHIP, IN CASE OF SYSTEM REBOOT
  AutonomousState = LOCALIZE_ROBOT; 
  
  //ZERO GYRO SYSTEMS
  wipeGyro();
  
  //MOVE BUCKET ALL THE WAY UP DURING LOCALIZATION
  sendActuatorPositionFeedback(BUCKET_DUMP_ANGLE_SET);
  
  //UNTIL WE ARE TOLD TO STOP, KEEP GOING
  while(getStoredMacroCommand()!=0)
  {
    int i =0;
    switch(AutonomousState)
    {
      case LOCALIZE_ROBOT:        /******************************************************************************************************/
      
        /******************************************************************************************************/
        //DO LOCALIZE ROUTINE TO DETERMINE POS/HEADING
        LIDARAcquireHeading();
        /******************************************************************************************************/
        
        AutonomousState = TRAVERSE_TO_CENTER;        
        break;
        
      case TRAVERSE_TO_CENTER:        /******************************************************************************************************/
      
        /******************************************************************************************************/
        //USE LOCALIZE DATA TO CENTER ROBOT IN START
         LIDARTraverseToCenter();
        /******************************************************************************************************/
        AutonomousState = MIGRATE_TO_DIGGING_ASSISTED;
        break;
        
      case MIGRATE_TO_DIGGING_ASSISTED:        /******************************************************************************************************/
        //MOVE THE BUCKET TO THE DRIVING POSITION
        sendActuatorPositionFeedback(BUCKET_DRIVE_ANGLE_SET);
         LIDARsetAngle90_2(true);
        /******************************************************************************************************/
        //DRIVE A BIT, CHECK HEADING, DRIVE A BIT, CHECK HEADING
        for(i=0;i<2;i++)
        {
            //Set initial heading
           getLidarDataInit();
           //Move a bit
           newEncoders(60);
           //Request new data
           getLidarDataUpdate();  
           
           //Correct heading if necessary
           if(isInRange(getRobotHeading(),90,5))  //If the robots heading +-5 degrees within 90
           {
             continue;
           }
           else
           {
             if(isInRange(90-getRobotHeading(),0,25)) //Is the turn within +-7 degrees?
                doTurn((90-getRobotHeading())/2);
           }
        }   
        /******************************************************************************************************/
        
        
        AutonomousState = MIGRATE_TO_DIGGING_BLIND;
        break;
        
      case MIGRATE_TO_DIGGING_BLIND:
        //CONTINUE TRAVERSE WITHOUT USE OF HEADING ASSISTING BEACON
        newEncoders(POST_SENSOR_FORWARD_TRAVERSAL_DISTANCE);
        AutonomousState = DROP_BUCKET;
        break;
        
      case DROP_BUCKET:
        //LOWER THE BUCKET TO DIG POSITION AND MOVE FORWARD WHEN BUCKET IS ABOUT TO CONTACT GROUND
        sendActuatorPositionDig(0);
        //DO THE REST OF THE DIGGING MOVEMENT FORWARD
        AutonomousState = DIG_FORWARD;
        break;
        
      case DIG_FORWARD:
	//DRIVE FORWARD WITH BUCKET DOWN TO COLLECT STUFF
	newEncoders((signed long) DRIVE_DIG_FORWARD);
        //FINISH THE DIGGING ROUTINE
        AutonomousState = DIG_REVERSE;
        break;
        
      case DIG_REVERSE:
        //REMOVE LOAD FROM BUCKET BY BACKING UP A SHORT DISTANCE
    	newEncoders((signed long) -DRIVE_DIG_REVERSE);	
    	//LIFT THE BUCKET TO THE DRIVING POSITION
    	sendActuatorPositionFeedback(BUCKET_DRIVE_ANGLE_SET);
        //BEGIN TRAVERSE TO SENSOR RANGE TO MOVE TO COLLECTION BIN
        AutonomousState = MIGRATE_TO_COLLECTION_BLIND;
        break;
        
      case MIGRATE_TO_COLLECTION_BLIND:
        //WITHOUT USE OF SENSORS MIGRATE FROM DIGGING POS TO SENSOR RANGE
        newEncoders(-PRE_SENSOR_REVERSE_TRAVERSAL_DISTANCE);        
        AutonomousState = MIGRATE_TO_COLLECTION_ASSISTED;
        break;
        
      case MIGRATE_TO_COLLECTION_ASSISTED:        /******************************************************************************************************/
        //USE SENSOR SYSTEMS TO APPROACH THE COLLECTION BIN 
        
        
        
        /******************************************************************************************************/
        //DRIVE A BIT, CHECK HEADING, DRIVE A BIT, CHECK HEADING
//        for(i=0;i<2;i++)
//        {
//           sendLIDARPosHeading();
//           newEncoders(-100);        
//           sendLIDARPosHeading();
//           waitForLidar.resetTimer();
//           while(!waitForLidar.timerDone())
//               macroCommunicationsUpdate();  
//           if(isInRange(getRobotHeading(),270,5)) //If the robot heading is about 270 within +-5 degrees
//           {
//             continue;
//           }
//           else
//           {
//             if(isInRange(getRobotHeading()-270, 0 , 7)) // is the correction turn within +-7 degrees
//                  doTurn(getRobotHeading()-270);
//           }  
//        }      
        
        /******************************************************************************************************/
        LIDARApproachCollection();
        
        //LIDARsetHeading270();
        /******************************************************************************************************/
        
        AutonomousState = FINALIZE_MOVE_TO_COLLECTION_BIN;
        break;
      
      case FINALIZE_MOVE_TO_COLLECTION_BIN:         /******************************************************************************************************/
      
      
        /******************************************************************************************************/
        //LIDARyCoord  = (double)getRobotDistance() * sin(((double)getRobotAngleFromLidar()) * PI/180.0);
        
        //MOVE BACKWARDS THE AMOUNT OF Y THAT REMAINS TO BE AT 0 distance
        //newEncoders(-(double)getRobotDistance() * sin(((double)getRobotAngleFromLidar()) * PI/180.0));
        /******************************************************************************************************/
        
        
        //NEXT DUMP WHAT WE HAVE INTO COLLECTION BIN
        AutonomousState = DUMP_BUCKET;
        break;
        
      case DUMP_BUCKET:
        //PUT BUCKET UP TO DUMP
        sendActuatorPositionFeedback(ACTUATOR_DUMP_ANGLE);
        //BEGIN TIMER SYSTEM FOR DUMP
    	dumpTimer.resetTimer();
        //EXIT THIS STATE WHEN THE TIMER IS DONE OR THE USER SAYS STOP
    	while(!dumpTimer.timerDone() && (getStoredMacroCommand()!=0))
    	{
                //LISTEN FOR KILL COMMAND
    		macroCommunicationsUpdate();
    		delay(5);			
    	} 
        //RETURN BUCKET TO DRIVE STATE
        sendActuatorPositionFeedback(BUCKET_DRIVE_ANGLE_SET);
        //RINSE, LATHER, REPEAT
        AutonomousState = MIGRATE_TO_DIGGING_ASSISTED;
        break;
        
      case NUM_AUTO_STATES:
        AutonomousState = LOCALIZE_ROBOT;
        break;       
      
    } /*  switch(AutonomousState) */
    
    macroCommunicationsUpdate();    
  }  /*  while(getStoredMacroCommand()!=0) */
  
  return (getStoredMacroCommand != 0);
}

 bool straightPathMineDumpStart()
{
    int AUTO_STATE=6;
    bool robotStart=true;
    wipeGyro();   
    sendActuatorPositionFeedback(BUCKET_DRIVE_ANGLE_SET);
				newEncoders((signed long) 100);
       traverseOutCentered();
    while(getStoredMacroCommand()!=0)
    {	
    	static Timers subDelayTimer(75), dumpTimer(6000);
        static int counter=0;
    	switch(AUTO_STATE)
    	{	
    		case TRAVERSE_FORWARD:
    			counter=0;
    			while((counter<DRIVE_INC_NUM_FORWARD)  && (getStoredMacroCommand()!=0))
    			{
                                if(robotStart)
                                {
				newEncoders((signed long) 250);
                                  robotStart=false;
                                }
                                else{
				newEncoders((signed long) 530);
                                }
				//RESET TIMER
				subDelayTimer.resetTimer();
				//WAIT UNTIL ITS FINISHED WHILE CHECKING COMMS
				while(!subDelayTimer.timerDone() && (getStoredMacroCommand()!=0)) 
				{
					macroCommunicationsUpdate();
					delay(2);
				}
				counter++;
    			}
    			AUTO_STATE++;
    			break;
    		case DROPPING_BUCKET:
    			//Lower bucket, moving forward when bucket gets low
    			sendActuatorPositionDig(0);			
    			AUTO_STATE++;
    			break;
    		case DIGGING_FORWARD:
    			//Drive forward with bucket in the dirt
    			newEncoders((signed long) DRIVE_DIG_FORWARD);			
    			AUTO_STATE++;
    			break;
    		case DIGGING_REVERSE:
    			//Free bucket by backing up
    			newEncoders((signed long) -DRIVE_DIG_REVERSE);	
    			//Lift the bucket
    			sendActuatorPositionFeedback(BUCKET_DRIVE_ANGLE_SET);		
    			AUTO_STATE++;
    			break;
    		case TRAVERSE_BACKWARD:
    			counter=0;
    			while((counter<DRIVE_INC_NUM_REVERSE) && (getStoredMacroCommand()!=0))
    			{
    				newEncoders((signed long) -350);
    				//RESET TIMER
    				subDelayTimer.resetTimer();
    				//WAIT UNTIL ITS FINISHED WHILE CHECKING COMMS
    				while(!subDelayTimer.timerDone()&&(getStoredMacroCommand()!=0)) 
    				{
    					macroCommunicationsUpdate();
    					delay(2);
    				}
    			    
    				
    				counter++;
    			}
                    //doTurn(-macroAngle);
                    reverseIn();
                    newEncoders(-60);
    			AUTO_STATE++;
    			break;
    		case DUMPING_BUCKET:
    			
                        sendActuatorPositionFeedback(ACTUATOR_DUMP_ANGLE);
    			dumpTimer.resetTimer();
    			while(!dumpTimer.timerDone() && (getStoredMacroCommand()!=0))
    			{
    				macroCommunicationsUpdate();
    				delay(5);			
    			}
    			AUTO_STATE++;
    			break;
    		case BUCKET_TO_DRIVE:
                    //Fix for wall contact angle correction :)
                    wipeGyro();
    			sendActuatorPositionFeedback(BUCKET_DRIVE_ANGLE_SET);			
    			AUTO_STATE=TRAVERSE_FORWARD;
    			break;
    	
    	}
    	
    	macroCommunicationsUpdate();
    }
  
  return (getStoredMacroCommand != 0);
}



 bool straightPathMineDump()
{
    int AUTO_STATE=6;
    wipeGyro();   
    sendActuatorPositionFeedback(BUCKET_DRIVE_ANGLE_SET);
    while(getStoredMacroCommand()!=0)
    {	
    	static Timers subDelayTimer(75), dumpTimer(6000);
        static int counter=0;
    	switch(AUTO_STATE)
    	{	
    		case TRAVERSE_FORWARD:
    			counter=0;
    			while((counter<DRIVE_INC_NUM_FORWARD)  && (getStoredMacroCommand()!=0))
    			{
				newEncoders((signed long) 530);
				//RESET TIMER
				subDelayTimer.resetTimer();
				//WAIT UNTIL ITS FINISHED WHILE CHECKING COMMS
				while(!subDelayTimer.timerDone() && (getStoredMacroCommand()!=0)) 
				{
					macroCommunicationsUpdate();
					delay(2);
				}
				counter++;
    			}
    			AUTO_STATE++;
    			break;
    		case DROPPING_BUCKET:
    			//Lower bucket, moving forward when bucket gets low
    			sendActuatorPositionDig(0);			
    			AUTO_STATE++;
    			break;
    		case DIGGING_FORWARD:
    			//Drive forward with bucket in the dirt
    			newEncoders((signed long) DRIVE_DIG_FORWARD);			
    			AUTO_STATE++;
    			break;
    		case DIGGING_REVERSE:
    			//Free bucket by backing up
    			newEncoders((signed long) -DRIVE_DIG_REVERSE);	
    			//Lift the bucket
    			sendActuatorPositionFeedback(BUCKET_DRIVE_ANGLE_SET);		
    			AUTO_STATE++;
    			break;
    		case TRAVERSE_BACKWARD:
    			counter=0;
    			while((counter<DRIVE_INC_NUM_REVERSE) && (getStoredMacroCommand()!=0))
    			{
    				newEncoders((signed long) -350);
    				//RESET TIMER
    				subDelayTimer.resetTimer();
    				//WAIT UNTIL ITS FINISHED WHILE CHECKING COMMS
    				while(!subDelayTimer.timerDone()&&(getStoredMacroCommand()!=0)) 
    				{
    					macroCommunicationsUpdate();
    					delay(2);
    				}
    			    
    				
    				counter++;
    			}
                        //doTurn(-macroAngle);
                        reverseIn();
                        newEncoders(-60);
    			AUTO_STATE++;
    			break;
    		case DUMPING_BUCKET:
                        sendActuatorPositionFeedback(ACTUATOR_DUMP_ANGLE);
    			dumpTimer.resetTimer();
    			while(!dumpTimer.timerDone() && (getStoredMacroCommand()!=0))
    			{
    				macroCommunicationsUpdate();
    				delay(5);			
    			}
    			AUTO_STATE++;
    			break;
    		case BUCKET_TO_DRIVE:
                    //Fix for wall contact angle correction :)
                    wipeGyro();
    			sendActuatorPositionFeedback(BUCKET_DRIVE_ANGLE_SET);			
    			AUTO_STATE=TRAVERSE_FORWARD;
    			break;
    	
    	}
    	
    	macroCommunicationsUpdate();
    }
  
  return (getStoredMacroCommand != 0);
}

 bool orientWithWii()
{
  return getStoredMacroCommand() != 0;
}

void fullRoutine()
{
    
}


void driveDigDistance()
{
  newEncoders(DIG_DRIVE_DISTANCE);
}



void fiftyForwardFiftyBackward()
{
  while (getStoredMacroCommand() != 0)
  {
    newEncoders(50);
    newEncoders(-50);
  }
}

void squareRoutine()
{
  while (getStoredMacroCommand() != 0)
  {
    newEncoders(50);
    doTurn(90);
  }
}

//***********LOW LEVEL ROBOT METHODS (USED INTERNALLY MOSTLY)***********
//Robot motors ALL Stop
 void allStop()
{
  sendMotorCommand(255, 255, 255);
}
//When motor board recieves 255's it appears to lock up,
// this method assures they are unstuck (as one may assume :) )
 void motor_unStick()
{
  sendMotorCommand(0, 0, 255);
}
