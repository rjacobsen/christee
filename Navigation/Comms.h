//Comms.h
#ifndef COMMS
#define COMMS

#include <Arduino.h>
#include <PID.h>

#define LIDARCODE

void sendLIDARRequestDataOnly(void);
void debugLIDARscreen(void);
int getRobotHeading(void);
int getRobotDistance(void);
int getRobotMovementDistance(void);
int getRobotAngleFromLidar(void);
//STARTUP SYSTEMS
 void initializeCommunications();

void prepManualData();
void prepAutoData();

//MAIN UPDATE FOR COMMUNICATIONS WHILE NOT IN A MACRO
 void updateComms();
 void macroCommunicationsUpdate();

void updateFromControlBoard();

void pullDataFromPacket() ;

//CHECK IF THE TIMER HAS RUN OUT BETWEEN COMMS UPDATES
 void commSafety();

//DELAY TIMEOUT OCCURRED METHOD
 void packetWait();


 void terminateMacroSystem();

//MOTOR COMMAND HELPER COMMUNICATIONS METHODS
//void sendMotorCommand(int leftMotor, int rightMotor);

void sendMotorCommand(int leftMotor, int rightMotor, int actuator);
void sendActuatorCommand(int actuator);
void sendLEDCommand(int color) ;
void sendLEDstate(int state) ;
void debugWiiCamerasBeacon(void);
void sendControlMacroEntered(void);
void sendLIDARPosHeading(void);




#endif
