
#include <Arduino.h>
#include "Macros_Encoders.h"
float macroEncoderSpeedL, macroEncoderSpeedR;


//speed calculations 
static int timeRecordedL=millis();
static int timeRecordedR=millis();

Timers CommsDelayTimingEncoders(2);
Timers PIDTimer(50);

void newEncoders(signed long cm)
{
  macroCommunicationsUpdate();
  //Zero the encoder variables in the comms system
  wipeEncoders();  
  //Timer queue up for running
  PIDTimer.resetTimer();
  while( !(isInRange(getMacroEncoderL(),cm,DEADZONE_ENCODER) && isInRange(getMacroEncoderR(),cm,DEADZONE_ENCODER)) && (getStoredMacroCommand() != 0))
  { 
    if(PIDTimer.timerDone())
    {    
      //CHECK GYRO FOR SHIFTED ANGLE
      if(abs(getMacroAngle())>3)
      {
	//IF CORRECTION IS REQUIRED DO IT
	 doTurn((int)(-grabFloatSign(getMacroAngle())*(abs(getMacroAngle()))));
         int tempDist=(getMacroEncoderL()+getMacroEncoderR())/2;
         setMacroEncoderL(tempDist);
         setMacroEncoderR(tempDist);
      }
      else
      {
        simpleMotorDistanceCommand(cm); 
      }
    }
    if(CommsDelayTimingEncoders.timerDone())
    {
       macroCommunicationsUpdate();   
    }
  }
  allStop();
  delay(5);
  motor_unStick();
  macroCommunicationsUpdate();
}

void wipeEncoders()
{
  setMacroEncoderL(0);
  setMacroEncoderR(0);
}

void updateMacroEncoderValueR(signed long increment)
{
  setMacroEncoderR(getMacroEncoderR()+increment);
}

void updateMacroEncoderValueL(signed long increment)
{
  setMacroEncoderL(getMacroEncoderL()+increment);
}

bool isInRange(signed long checkNum, signed long target, signed long range)
{
  return ((checkNum<(target+range)) && (checkNum>(target-range)));
}

