#include <xc.h>
#define _SUPPRESS_PLIB_WARNING
#include <plib.h>
#include <stdio.h>
#include <math.h>
#include <stdbool.h>
#include "initialize.h"
#include "wiiCamera.h"
#include "Servos.h"
#include "uart_handler.h"
#include "I2C_API.h"

unsigned char wiiInitStep = 0;
void initializeBits(void)
{
     SYSTEMConfig(SYS_FREQ, SYS_CFG_ALL); //memory management // sets up periferal and clock configuration

    DDPCONbits.JTAGEN = 0; // This turns off the JTAG and allows PORTA pins to be used (Must disable the JTAG module in order to use LAT registers to set discrete outputs (LEDs)
    AD1PCFG=0x0004; // enables analog input for RB5 only

    // TRIS ? Set a pin as Input or Output
    // LAT ? output```````````````````````````````
    // PORT ? inpput
    
    //LEDs for debug 1 is input 0 is output
    TRISBbits.TRISB9 = 0;
    TRISBbits.TRISB10 = 0;
    TRISBbits.TRISB11 = 0;
    TRISBbits.TRISB12 = 0;
    TRISBbits.TRISB13 = 0;
    // LEDs for initialization
    TRISDbits.TRISD4 = 0;
    TRISDbits.TRISD5 = 0;
    //Input for switch
    TRISBbits.TRISB2=1;



    // writes to the output bits 1 is off 0 is on
    LATBbits.LATB9 = 1;
    LATBbits.LATB10 = 1;
    LATBbits.LATB11 = 1;
    LATBbits.LATB12 = 1;
    LATBbits.LATB13 = 1;
    // LEDs for initialization
    LATDbits.LATD4 = 1;
    LATDbits.LATD5 = 1;

    

    //initialization for I2C using I2C numbers 3 and 5
    I2C3BRG = 390;
    IPC6bits.I2C3IP = 1; //priority 1
    IPC6bits.I2C3IS = 2; //priority 2
    IEC0bits.I2C3MIE = 1; //enable I2C interrupt
    I2C3CONbits.ON = 1; //enable I2C

    I2C5BRG = 390;
    IPC8bits.I2C5IP = 1; //priority 1
    IPC8bits.I2C5IS = 2; //priority 2
    IEC1bits.I2C5MIE = 1; //enable I2C interrupt
    I2C5CONbits.ON = 1; //enable I2C


    UART();
    begin(&receiveArray, sizeof(receiveArray),CAMERA_ADDRESS,false, Send_put,Receive_get,Receive_available,Receive_peek);

    timers();

    INTEnableSystemMultiVectoredInt(); //enables interrupts have to do this after timers so that they don't interrupt the initialization

    INTEnableInterrupts(); // enable interrupts have to do this after timers so that they don't interrupt the initialization
}

//initializes the timers
void timers(void)
{
    // timer for servo 1
    T4CONbits.ON = 0; // turn timer off
    T4CONbits.TCKPS = 0b101; // 32 prescalar
    T4CONbits.TGATE = 0;
    T4CONbits.T32 = 0;
    IFS0CLR = _IFS0_T4IF_MASK;
    IEC0SET = _IEC0_T4IE_MASK;
    IPC4SET = ((0x111 << _IPC4_T4IP_POSITION) | (0x1 << _IPC4_T4IS_POSITION));
    T4CONbits.ON = 1;  //turn timer on

    //timer for servo 2
    T5CONbits.ON = 0;// turn timer off
    T5CONbits.TCKPS = 0b101; // 32 prescalar
    T5CONbits.TGATE = 0;
    IFS0CLR = _IFS0_T5IF_MASK;
    IEC0SET = _IEC0_T5IE_MASK;
    IPC5SET = ((0x111 << _IPC5_T5IP_POSITION) | (0x1 << _IPC5_T5IS_POSITION)); // What does this do?
    T5CONbits.ON = 1; //turn timer on

    // general timing timer for everything
    T3CONbits.ON = 0;
    T3CONbits.TCKPS = 0b111; //256 prescalar
    T3CONbits.TGATE = 0;
    //TPB_clock is the clock resource for peripherals on pic32MX (p.201) (look at system clock / "FPBDIV") - located in configuration bits
    //Example:
    //Desired_Period == 100ms
    //PR# = Desired_Period / [(1/Fosc)*Prescaler]
    //PR# = 0.100seconds / [(1/80MHz)*256] = 31250
    PR3 = 31250;
    IFS0CLR = _IFS0_T3IF_MASK;
    IEC0SET = _IEC0_T3IE_MASK;
    IPC3SET = ((0x1 << _IPC3_T3IP_POSITION) | (0x1 << _IPC3_T3IS_POSITION));
    T3CONbits.ON = 1;


}

void UART(void)
{
    U6MODEbits.BRGH = 0; // set to standard speed mode
    U6BRG = 42; //21;// 230400 baud   //42;// 115200 baud  //85;// 57600 baud
    U6MODEbits.PDSEL = 0b00; // 8-bit no parity
    U6MODEbits.STSEL = 0; // 1 stop bit
    IFS2bits.U6TXIF = 0;
   // IEC2bits.U6TXIE = 1; // transmit interrupt enable
    IPC12bits.U6IP = 1; // priority 1
    IPC12bits.U6IS = 1; // sub priority 1
    U6STAbits.UTXISEL = 0b01; // interrupt when transmit complete
//    U1STAbits.URXISEL = 0; // interrupt generated with every reception
    U6STAbits.URXEN = 1; // enable uart recieve
    U6STAbits.UTXEN = 1; // enable uart transmit
    U6MODEbits.ON = 1; // enable whole uart module
    // uart 4 receive lidar Data (only need RX wire)
    //disable analog part of the pins
    //U6TXREG=0x01;
    UART_buff_init(&output_buffer6);
    UART_buff_flush(&output_buffer6,1);
    
    // uart 6 debug TX to computer terminal window (only need TX wire)
    U5MODEbits.BRGH = 0; // set to standard speed mode
    U5BRG = 42; //21;// 230400 baud   //42;// 115200 baud  //85;// 57600 baud
    U5MODEbits.PDSEL = 0b00; // 8-bit no parity
    U5MODEbits.STSEL = 0; // 1 stop bit
    IFS2bits.U5TXIF = 0;
    //IEC2bits.U6TXIE = 1; // transmit interrupt enable
    IPC12bits.U5IP = 1; // priority 1
    IPC12bits.U5IS = 1; // sub priority 1
    U5STAbits.UTXISEL = 0b01; // interrupt when transmit complete
//    U1STAbits.URXISEL = 0; // interrupt generated with every reception
    U5STAbits.URXEN = 1; // enable uart recieve
    U5STAbits.UTXEN = 1; // enable uart transmit
    U5MODEbits.ON = 1; // enable whole uart module
    // uart 6 error
//    IEC0bits.U1EIE = 1; // error interrupt enabed
    UART_buff_init(&input_buffer);
    UART_buff_init(&output_buffer);
}


// for initializing the wii cameras on their I2C
void initializeCheck(void) 
{
    // checks to see if the I2C has failed to send and is not trying to send another message
    if ((StatusI2Cone() == FAILED_I2C) || !pendingTrans1) 
    {
        // sends to I2C to restart the communication
        SendI2Cone(camera, data, 2);
        pendingTrans1 = true;
    }
    if ((StatusI2Ctwo() == FAILED_I2C) || !pendingTrans2) 
    {

        SendI2Ctwo(camera, data, 2);
        pendingTrans2 = true;
    }

    //printf("InitStep %d\r\n", wiiInitStep);
    //printf("I2c one %d\r\n", StatusI2Cone());
    //printf("I2c two %d\r\n", StatusI2Ctwo());

    // if the I2C line is talking then start sending the data to it
    if ((StatusI2Ctwo() == SUCCESS_I2C) && (StatusI2Cone() == SUCCESS_I2C)) 
    {

        pendingTrans2 = false;
        pendingTrans1 = false;
        wiiInitStep++;
    }
//    if (StatusI2Ctwo() == 0) {
//        //LATBbits.LATB9^=1;
//    }
//    if (StatusI2Cone() == 0) {
//        // LATBbits.LATB10^=1;
//    }
}

void initializeWii(void) {
    //setup the camera with the specific data bytes but only after the previous ones have been sent to each I2C
    if (wiiInitStep == 0) {
        data[0] = 0x30;
        data[1] = 0x01;
        initializeCheck();
    } else if (wiiInitStep == 1) {
        data[0] = 0x30;
        data[1] = 0x08;
        initializeCheck();
    } else if (wiiInitStep == 2) {
        data[0] = 0x06;
        data[1] = 0x90;
        initializeCheck();
    } else if (wiiInitStep == 3) {
        data[0] = 0x08;
        data[1] = 0xC0;
        initializeCheck();
    } else if (wiiInitStep == 4) {
        data[0] = 0x1A;
        data[1] = 0x40;
        initializeCheck();
    } else if (wiiInitStep == 5) {
        data[0] = 0x33;
        data[1] = 0x33;
        initializeCheck();
    }


}


char getWiiInitStep()
{
    return wiiInitStep;
}

