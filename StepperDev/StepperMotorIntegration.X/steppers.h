/* Microchip Technology Inc. and its subsidiaries.  You may use this software 
 * and any derivatives exclusively with Microchip products. 
 * 
 * THIS SOFTWARE IS SUPPLIED BY MICROCHIP "AS IS".  NO WARRANTIES, WHETHER 
 * EXPRESS, IMPLIED OR STATUTORY, APPLY TO THIS SOFTWARE, INCLUDING ANY IMPLIED 
 * WARRANTIES OF NON-INFRINGEMENT, MERCHANTABILITY, AND FITNESS FOR A 
 * PARTICULAR PURPOSE, OR ITS INTERACTION WITH MICROCHIP PRODUCTS, COMBINATION 
 * WITH ANY OTHER PRODUCTS, OR USE IN ANY APPLICATION. 
 *
 * IN NO EVENT WILL MICROCHIP BE LIABLE FOR ANY INDIRECT, SPECIAL, PUNITIVE, 
 * INCIDENTAL OR CONSEQUENTIAL LOSS, DAMAGE, COST OR EXPENSE OF ANY KIND 
 * WHATSOEVER RELATED TO THE SOFTWARE, HOWEVER CAUSED, EVEN IF MICROCHIP HAS 
 * BEEN ADVISED OF THE POSSIBILITY OR THE DAMAGES ARE FORESEEABLE.  TO THE 
 * FULLEST EXTENT ALLOWED BY LAW, MICROCHIP'S TOTAL LIABILITY ON ALL CLAIMS 
 * IN ANY WAY RELATED TO THIS SOFTWARE WILL NOT EXCEED THE AMOUNT OF FEES, IF 
 * ANY, THAT YOU HAVE PAID DIRECTLY TO MICROCHIP FOR THIS SOFTWARE.
 *
 * MICROCHIP PROVIDES THIS SOFTWARE CONDITIONALLY UPON YOUR ACCEPTANCE OF THESE 
 * TERMS. 
 */

/* 
 * File:   
 * Author: 
 * Comments:
 * Revision history: 
 */

// This is a guard condition so that contents of this file are not included
// more than once.  
#ifndef STEPPERS_H
#define	STEPPERS_H

#include <xc.h> // include processor files - each processor file is guarded.  
#include "stdbool.h"

//ADDRESS OF THE STEPPER MOTOR DRIVER CHIP
#define CHIPADDRESS 0b01000110 

//REGISTERS OF THE STEPPER MOTOR DRIVER CHIP
#define MODE 0x00
#define WDTOI 0x01 //watch dog timeout
#define WDCNTL 0x02 //watch dog control
#define IO_CFG 0x03 //io config
#define INTMODE 0x04 //interrupt mode
#define MSK 0x05 //mask interrupt
#define INTSTAT 0x06 //interrupt status
#define IP 0x07 //input port
#define INT_MTR_ACT 0x08 //interupt motor action
#define EXTRASTEPS0 0x09 
#define EXTRASTEPS1 0x0A
#define OP_CFG_PHS 0x0B //phase control
#define OP_STAT_TO 0x0C //timeout
#define RUCNTL 0x0D //ramp up control
#define RDCNTL 0x0E //ramp down control
#define PMA 0x0F //multiple actions control
#define LOOPDLY_CW 0x10
#define LOOPDLY_CCW 0x11
#define CWSCOUNTL 0x12
#define CWSCOUNTH 0x13
#define CCWSCOUNTL 0x14
#define CCWSCOUNTH 0x15
#define CWPWL 0x16
#define CWPWH 0x17
#define CCWPWL 0x18
#define CCWPWH 0x19
#define MCNTL 0x1A
#define SUBADR1 0x1B
#define SUBADR2 0x1C
#define SUBADR3 0x1D
#define ALLCALLADR 0x1E
#define CW true
#define CCW false

/**
  <p><b>Function: initStepper(int stepperNumber)</b></p>

  <p><b>Summary: Starts the I2C driven stepper driver of the given bus</b></p>

  <p><b>Description: Uses the init data container to send sequence to init driver for use</b></p>

  <p><b>Remarks:</b></p>
 */
void initStepper(int stepperNumber);
/**
  <p><b>Function: testStepper(unsigned char stepperNumber, unsigned int steps, bool clockwise)</b></p>

  <p><b>Summary: Sets the stepper given to start a CW or CCW rotation of given number of steps</b></p>

  <p><b>Description: Chooses the appropriate I2C and sends commands to make stepper rotate given number of steps</b></p>

  <p><b>Remarks:</b></p>
 */
void testStepper(unsigned char stepperNumber, unsigned int steps, bool clockwise);

// TODO Insert declarations or function prototypes (right here) to leverage 
// live documentation

#ifdef	__cplusplus
extern "C" {
#endif /* __cplusplus */

#ifdef	__cplusplus
}
#endif /* __cplusplus */

#endif	/* STEPPERS_H */

