/* 
 * File: steppers.c  
 * Author: Zac Kilburn
 * Comments: Library to init and use stepper motor driver PCA9629A
 */

// *****************************************************************************
// *****************************************************************************
// Section: Included Files 
// *****************************************************************************
// *****************************************************************************

#include "I2C_API.h"
#include "steppers.h"

// *****************************************************************************
// *****************************************************************************
// Section: Global Data Definitions
// *****************************************************************************
// *****************************************************************************

//Initialize data for stepper motor driver
unsigned char initData[20]=
{
    MODE,0x21,
    IO_CFG,0x0D,
    OP_CFG_PHS,0x10,
    CWPWL,0x50,
    CWPWH,0x60,
    CCWPWL,0x50,
    CCWPWH,0x60,
    INTMODE,0x00,
    INT_MTR_ACT,0x01,
    MSK,0x1E
};

//Container to send to I2C functions, must be persistent for passing by ref
unsigned char sendable[2];

// *****************************************************************************
// *****************************************************************************
// Section: User Functions
// *****************************************************************************
// *****************************************************************************

/**
  <p><b>Function: initStepper(int stepperNumber)</b></p>

  <p><b>Summary: Starts the I2C driven stepper driver of the given bus</b></p>

  <p><b>Description: Uses the init data container to send sequence to init driver for use</b></p>

  <p><b>Remarks:</b></p>
 */

void initStepper(int stepperNumber)
{    
    int stepperInitCounter=0;
    switch(stepperNumber)
    {
        case 0:
            while(stepperInitCounter<10)
            {
                    if (((StatusI2Cone() == FAILED) || StatusI2Cone()==SUCCESS) )
                    {
                        sendable[0]=initData[stepperInitCounter*2];
                        sendable[1]=initData[stepperInitCounter*2+1];
                        switch(stepperInitCounter)
                        {
                            case 0:
                                SendI2Cone(CHIPADDRESS,sendable,2);
                                break;
                            case 1:      
                                SendI2Cone(CHIPADDRESS,sendable,2);
                                break;
                            case 2:
                                SendI2Cone(CHIPADDRESS,sendable,2); //steup for full steps, OUT[3:0] configured as motor outputs
                                break;
                            case 3:
                                SendI2Cone(CHIPADDRESS,sendable,2);    
                                break;
                            case 4:
                                SendI2Cone(CHIPADDRESS,sendable,2);
                                break;
                            case 5:
                                SendI2Cone(CHIPADDRESS,sendable,2);
                                break;
                            case 6:
                                SendI2Cone(CHIPADDRESS,sendable,2);
                                break;
                            case 7:
                                SendI2Cone(CHIPADDRESS,sendable,2);
                                break;
                            case 8:
                                SendI2Cone(CHIPADDRESS,sendable,2);
                                break;
                            case 9:
                                SendI2Cone(CHIPADDRESS,sendable,2);
                                break;
                        }  
                    stepperInitCounter++;
                    }  
            }
            break;
        case 1:
            while(stepperInitCounter<10)
            {
                    if (((StatusI2Ctwo() == FAILED) || StatusI2Ctwo()==SUCCESS) )
                    {
                        sendable[0]=initData[stepperInitCounter*2];
                        sendable[1]=initData[stepperInitCounter*2+1];
                        switch(stepperInitCounter)
                        {
                            case 0:
                                SendI2Ctwo(CHIPADDRESS,sendable,2);
                                break;
                            case 1:      
                                SendI2Ctwo(CHIPADDRESS,sendable,2);
                                break;
                            case 2:
                                SendI2Ctwo(CHIPADDRESS,sendable,2); //steup for full steps, OUT[3:0] configured as motor outputs
                                break;
                            case 3:
                                SendI2Ctwo(CHIPADDRESS,sendable,2);    
                                break;
                            case 4:
                                SendI2Ctwo(CHIPADDRESS,sendable,2);
                                break;
                            case 5:
                                SendI2Ctwo(CHIPADDRESS,sendable,2);
                                break;
                            case 6:
                                SendI2Ctwo(CHIPADDRESS,sendable,2);
                                break;
                            case 7:
                                SendI2Ctwo(CHIPADDRESS,sendable,2);
                                break;
                            case 8:
                                SendI2Ctwo(CHIPADDRESS,sendable,2);
                                break;
                            case 9:
                                SendI2Ctwo(CHIPADDRESS,sendable,2);
                                break;
                        }  
                    stepperInitCounter++;
                    }  
            }
            break;
    }
}
/**
  <p><b>Function: testStepper(unsigned char stepperNumber, unsigned int steps, bool clockwise)</b></p>

  <p><b>Summary: Sets the stepper given to start a CW or CCW rotation of given number of steps</b></p>

  <p><b>Description: Chooses the appropriate I2C and sends commands to make stepper rotate given number of steps</b></p>

  <p><b>Remarks:</b></p>
 */
void testStepper(unsigned char stepperNumber, unsigned int steps, bool clockwise)
{
   //SEPARATE THE INTEGER (16bit) INTO 2 BYTES TO SEND
   unsigned char high=0,low=0;
   high=steps>>8;
   low=steps;
    
   //CHOOSE WHICH I2C TO TALK TO
   switch(stepperNumber)
   { 
        case 0:
            //SEND LOW BYTE FOR COUNTS
            while( StatusI2Cone()==PENDING ); 
            if(clockwise)
                sendable[0]=CWSCOUNTL;
            else
                sendable[0]=CCWSCOUNTL;
            sendable[1]=low;  
            SendI2Cone(CHIPADDRESS,sendable,2);

            //SEND HIGH BYTE FOR COUNTS
            while( StatusI2Cone()==PENDING ); 
            if(clockwise)
                sendable[0]=CWSCOUNTH;
            else
                sendable[0]=CCWSCOUNTH;
            sendable[1]=high;            
            SendI2Cone(CHIPADDRESS,sendable,2);

            //INSTRUCT MOTOR TO START
            while( StatusI2Cone()==PENDING );              
            sendable[0]=MCNTL;
            if(clockwise)
                sendable[1]=0x80; //REGISTER VALUE FOR CW START   
            else
                sendable[1]=0x81; //REGISTER VALUE FOR CCW START             
            SendI2Cone(CHIPADDRESS,sendable,2);
            break;
        case 1:
            //SEND LOW BYTE FOR COUNTS
            while( StatusI2Ctwo()==PENDING ); 
            if(clockwise)
                sendable[0]=CWSCOUNTL;
            else
                sendable[0]=CCWSCOUNTL;
            sendable[1]=low;  
            SendI2Ctwo(CHIPADDRESS,sendable,2);

            //SEND HIGH BYTE FOR COUNTS
            while( StatusI2Ctwo()==PENDING );  
            if(clockwise)
                sendable[0]=CWSCOUNTH;
            else
                sendable[0]=CCWSCOUNTH;
            sendable[1]=high;            
            SendI2Ctwo(CHIPADDRESS,sendable,2);
            
            //INSTRUCT MOTOR TO START
            while( StatusI2Ctwo()==PENDING );   
            sendable[0]=MCNTL;
            if(clockwise)
                sendable[1]=0x80; //REGISTER VALUE FOR CW START
            else
                sendable[1]=0x81; //REGISTER VALUE FOR CCW START        
            SendI2Ctwo(CHIPADDRESS,sendable,2);
            break;
        
   }
}