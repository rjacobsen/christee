/* 
 * File:   Servos.h
 * Author: Dana
 *
 * Created on May 12, 2016, 7:54 PM
 */
#include <stdbool.h>



#ifndef SERVOS_H
#define	SERVOS_H

#ifdef	__cplusplus
extern "C" {
#endif
#define MIDDLE_X      511
#define BUFFER_X      20    //100
#define MIN_INCREMENT 0.18
#define DEGREE        13.8888888888888889
#define DEGREE180     4986 // 5000- buffer of 14
#define DEGREE0       2514 // 2500+ buffer of 14
#define SERVO1        0
#define SERVO2        1
#define SERVO_STEP_WIDE 23
#define SERVO_MAX 180
#define SERVO_MIN 0
#define INVALID 1023


//  unsigned char angle[2]; // 0 degrees to start for pwm
  bool clockwise[2]; //false = left, true = right
  bool beaconExists[2] ;
  bool setHigh1;
  bool setHigh2;

//  void determineAngle(bool haveBlob, short blobX, unsigned char servo);
  int isAbout(int compareThis, int toThis, int range);
  void seekBeacon(int cameraNumber, int result);
  unsigned char getAngle1(void);
  unsigned char getAngle2(void);
  void setAngle1(double d);
  void setAngle2(double d);
  void changeAngle1(double d);
  void changeAngle2(double d);
#ifdef	__cplusplus
}
#endif

#endif	/* SERVOS_H */

