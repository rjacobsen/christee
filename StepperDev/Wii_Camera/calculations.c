#include <stdbool.h>
#include <math.h>
#include "calculations.h"

// calculates the distace from the collection bin the robot is based off of the angles of the servos
int CalculateHeight(unsigned char state)
{
    // if don't see both blobs
    if(state < 6)
        return 0;
    // if see both blobs (either one each for the servos, or both servos see both)
    return COLLECTOR_BIN * sin(getAngle1()*DEGREE_TO_RADIAN)*sin(getAngle2()*DEGREE_TO_RADIAN)/sin((180-getAngle1()-getAngle2())*DEGREE_TO_RADIAN);
}

// based off of the servo angles the distance to the left or the right of center is calculated
int offCenter(unsigned char state) // from right side
{
    // if don't see both blobs
    if(state < 6)
        return 0;
    // if see both blobs (either one each for the servos, or both servos see both)
    return COLLECTOR_BIN*sin(getAngle1()*DEGREE_TO_RADIAN)*cos(getAngle2()*DEGREE_TO_RADIAN)/ sin((180-getAngle1()- getAngle2())*DEGREE_TO_RADIAN) + OFFSET_CB;
}

//determines how many blobs the cameras can see total based on their average readings
unsigned char determineState(int result1, int result2)
{
    bool goodr1 = false;
    bool goodr2 = false;
    //checks to see if the results are plausible
    if(result1<1023)
    {
        goodr1 = true;
    }
    if(result2<1023)
    {
        goodr2 = true;
    }
    

    
        if(goodr1 && goodr2)
        { // good r values from the blobs
        if(getBlobsSeen(CAMERA1)<=0 && getBlobsSeen(CAMERA2)<=0)// can't see any blobs
            return NOBLOBS;
        if((getBlobsSeen(CAMERA1)>1 && getBlobsSeen(CAMERA2)>1))// both see both blobs
            return BOTHSEEBOTHBLOBS;
        if(getBlobsSeen(CAMERA1)==1 && getBlobsSeen(CAMERA2)<=0)//camera1 sees one blob only
            return CAMERA1ONEBLOB;
        if(getBlobsSeen(CAMERA1)<=0 && getBlobsSeen(CAMERA2)==1) // camera2 sees one blob only
            return CAMERA2ONEBLOB;
        if(getBlobsSeen(CAMERA1)>1 && getBlobsSeen(CAMERA2)<=0) // camera1 sees both blobs only
            return CAMERA1BOTHBLOBS;
        if(getBlobsSeen(CAMERA1)<=0 && getBlobsSeen(CAMERA2)>1) // camera2 sees both blobs only
           return CAMERA2BOTHBLOBS;
        if(getBlobsSeen(CAMERA1)==1 && getBlobsSeen(CAMERA2)==1) // both cameras see one blob
        {
            if((180-getAngle1()-getAngle2())<0) //cameras each see different blobs
                return BOTHSEEDIFFBLOB;
         return BOTHSEESAMEBLOB; // cameras see same blob
        }
    }
    else if(goodr1&&!goodr2) // no good blobs from camera2
    {
      if(getBlobsSeen(CAMERA1)<=0)// can't see any blobs camera1
        return NOBLOBS;
      if(getBlobsSeen(CAMERA1)==1)//camera1 sees one blob only
        return CAMERA1ONEBLOB;
      if(getBlobsSeen(CAMERA1)>1) // camera1 sees both blobs only
        return CAMERA1BOTHBLOBS;
    }
    else if (!goodr1&& goodr2)// no good blobs from camera1
    {
      if(getBlobsSeen(CAMERA2)<=0)// can't see any blobs camera2
        return NOBLOBS;
      if(getBlobsSeen(CAMERA2)==1) // camera2 sees one blob only
        return CAMERA2ONEBLOB;
      if(getBlobsSeen(CAMERA2)>1) // camera2 sees both blobs only
       return CAMERA2BOTHBLOBS;
    }
    else
    {
        return NOBLOBS;
    }

}