#include <xc.h>
#define _SUPPRESS_PLIB_WARNING
#include <plib.h>
#include <stdio.h>
#include <math.h>
#include <stdbool.h>
#include "initialize.h"
#include "main.h"
#include "wiiCamera.h"
#include "Servos.h"
#include "uart_handler.h"
#include "calculations.h"

#define INDICATOR1  LATBbits.LATB13
#define INDICATOR2  LATBbits.LATB12
#define INDICATOR3  LATBbits.LATB11
#define INDICATOR4  LATBbits.LATB10
#define INDICATOR5  LATBbits.LATB9

#define CAMERA1 0
#define CAMERA2 1
#define LANTRONIX   4
#define DEGREE_TO_RADIAN  0.0174532925194329577

// DEVCFG3
// USERID = No Setting
#pragma config FSRSSEL = PRIORITY_7     // SRS Select (SRS Priority 7)
#pragma config FCANIO = OFF             // CAN I/O Pin Select (Alternate CAN I/O)
#pragma config FUSBIDIO = OFF           // USB USID Selection (Controlled by Port Function)
#pragma config FVBUSONIO = OFF          // USB VBUS ON Selection (Controlled by Port Function)

// DEVCFG2
#pragma config FPLLIDIV = DIV_6         // PLL Input Divider (6x Divider) (Phase Lock Loop- stepping up the frequency)
#pragma config FPLLMUL = MUL_20         // PLL Multiplier (20x Multiplier)
#pragma config UPLLIDIV = DIV_6         // USB PLL Input Divider (6x Divider)
#pragma config UPLLEN = ON              // USB PLL Enable (Enabled)
#pragma config FPLLODIV = DIV_1         // System PLL Output Clock Divider (PLL Divide by 1)

// DEVCFG1
#pragma config FNOSC = PRIPLL           // Oscillator Selection Bits (Primary Osc w/PLL (XT+,HS+,EC+PLL))
#pragma config FSOSCEN = OFF            // Secondary Oscillator Enable (Disabled)
#pragma config IESO = OFF               // Internal/External Switch Over (Disabled)
#pragma config POSCMOD = HS             // Primary Oscillator Configuration (HS osc mode)
#pragma config OSCIOFNC = OFF           // CLKO Output Signal Active on the OSCO Pin (Disabled)
#pragma config FPBDIV = DIV_1           // Peripheral Clock Divisor (Pb_Clk is Sys_Clk/1)
#pragma config FCKSM = CSDCMD           // Clock Switching and Monitor Selection (Clock Switch Disable, FSCM Disabled)
#pragma config WDTPS = PS1048576        // Watchdog Timer Postscaler (1:1048576)
#pragma config FWDTEN = OFF             // Watchdog Timer Enable (WDT Disabled (SWDTEN Bit Controls))

// DEVCFG0
#pragma config DEBUG = ON               // Background Debugger Enable (Debugger is enabled)
#pragma config ICESEL = ICS_PGx1        // ICE/ICD Comm Channel Select (ICE EMUC1/EMUD1 pins shared with PGC1/PGD1)
#pragma config PWP = OFF                // Program Flash Write Protect (Disable)
#pragma config BWP = OFF                // Boot Flash Write Protect bit (Protection Disabled)
#pragma config CP = OFF                 // Code Protect (Protection Disabled)

#define DEBUG
//#define DEBUG_HEIGHT

bool awaitingData = false;
bool dataAvailable = false;

void main(void) 
{
    initializeBits();
    while(getWiiInitStep()<=5)
    {
        initializeWii();
    }
//  timers();
        
    //Startup delay
    int i = 0;
    while (i < 100000) 
    {
        i++;
    }

    //Main loop of code
    while (1) 
    {
        //Do I need to wait until not dataAvailable as well?
        if (get200msDone() && !awaitingData) 
        {
            //printf("DataIn/Out\r\n");
            //U6TXREG=0x99;
            //Send data to the control system (LANTRONIX)
            unsigned char state = determineState(result1, result2);
            
#ifdef DEBUG
            static unsigned char prevState=0;
            static unsigned char prevAngle1=0,prevAngle2=0;
            if(prevState!=state)
            {               
                printf("\nState: %d\n",state);
                prevState=state;
                switch(state)
                {
                    case NOBLOBS:
                        printf("No Blobs Seen\n");
                        printf("C1 Angle: %d\n",getAngle1());
                        printf("C2 Angle: %d\n",getAngle2());
                        break;
                    case CAMERA1ONEBLOB:
                        printf("Camera 1 Sees One Blob\n");
                        printf("C1 Angle: %d\n",getAngle1());
                        printf("C2 Angle: %d\n",getAngle2());
                        break;
                    case CAMERA2ONEBLOB:
                        printf("Camera 2 Sees One Blob\n");
                        printf("C1 Angle: %d\n",getAngle1());
                        printf("C2 Angle: %d\n",getAngle2());
                        break;
                    case CAMERA1BOTHBLOBS:
                        printf("Camera 1 Sees Both Blobs\n");
                        printf("C1 Angle: %d\n",getAngle1());
                        printf("C2 Angle: %d\n",getAngle2());
                        break;
                    case CAMERA2BOTHBLOBS:
                        printf("Camera 2 Sees Both Blobs\n");
                        printf("C1 Angle: %d\n",getAngle1());
                        printf("C2 Angle: %d\n",getAngle2());
                        break;
                    case BOTHSEEDIFFBLOB:
                        printf("Both Cameras See Diff Blob\n");
                        printf("C1 Angle: %d\n",getAngle1());
                        printf("C2 Angle: %d\n",getAngle2());
                        break;
                    case BOTHSEESAMEBLOB:
                        printf("Both Cameras See Same Blob\n");
                        printf("C1 Angle: %d\n",getAngle1());
                        printf("C2 Angle: %d\n",getAngle2());
                        break;
                    case BOTHSEEBOTHBLOBS:
                        printf("Both Cameras See Both Blobs\n");
                        printf("C1 Angle: %d\n",getAngle1());
                        printf("C2 Angle: %d\n",getAngle2());
                        printf("X coord: %d \n",offCenter(state));
                        printf("Y coord: %d \n",CalculateHeight(state));
                        printf("Creates Angle %d\n", 180-getAngle1()-getAngle2());
                        break;
                }
            }
            else
            {
                if(prevAngle1!=getAngle1())
                {
                    printf("\nC1 Angle: %d\n",getAngle1());
                    prevAngle1=getAngle1();
                    if(state==7)
                    {
                        printf("X coord: %d \n",offCenter(state));
                        printf("Y coord: %d \n",CalculateHeight(state));
                        printf("Creates Angle %d\n", 180-getAngle1()-getAngle2());
                    }
                }
                if(prevAngle2!=getAngle2())
                {
                    printf("\nC2 Angle: %d\n",getAngle2());
                    prevAngle2=getAngle2();
                    if(state==7)
                    {
                        printf("X coord: %d \n",offCenter(state));
                        printf("Y coord: %d \n",CalculateHeight(state));
                        printf("Creates Angle %d\n", 180-getAngle1()-getAngle2());
                    }
                }
            }
#endif
            ToSend(0,CAMERA_ADDRESS);
            ToSend(9,state);
            ToSend(10,getAngle1());//left camera when facing, lidar side
            ToSend(11,getAngle2());//right camera
            ToSend(12,offCenter(state));
            ToSend(13,CalculateHeight(state));
            ToSend(14,getDifferenceX(CAMERA1));//left camera, lidar side
            ToSend(15,getDifferenceX(CAMERA2)); // right camera

            sendData(LANTRONIX);
            
            //Attempt to read the new data from the I2c
            read(CAMERA1);
            read(CAMERA2);
            
            //Reset the timer for the data to be read out
            set200msDone(false);
            
            //Mark that we are waiting for new data to come in
            awaitingData = true;
        }
        
        //If we are awaiting the data to be ready from the I2c's
        if (awaitingData) 
        {
            //If both I2c's are ready for data processing
            if (StatusI2Cone() == 1 && StatusI2Ctwo()==1) 
            {
                dataAvailable = true;
                awaitingData = false;
            }
            else if(StatusI2Cone() == 2 || StatusI2Ctwo()==2)
            {
                awaitingData=false;
            }
        }
        
        //if the data is said to be ready to read
        if (dataAvailable) 
        {
            //Mark that we have processed the data
            dataAvailable = false;
            
            //Sort the camera data
            Sort(CAMERA1);
            Sort(CAMERA2);
            
            //Store the info about what the sorted data said about the environment
            result1 = PickBeacon(CAMERA1);
            result2 = PickBeacon(CAMERA2);
#ifdef DEBUG_HEIGHT
            switch(result1)
            {
                case 2000:
                    printf("\n");
                    printf("C1 Height Variance Issue: 2 Blob \n");
                    printf("C1 has %d blobs\n",getBlobsSeen(0));
                    printf("X1: %d, X2: %d, X3: %d, X4: %d\n",Ix_buffer[0][0],Ix_buffer[0][1],Ix_buffer[0][2],Ix_buffer[0][3]);
                    printf("Y1: %d, Y2: %d, Y3: %d, Y4: %d\n",Iy_buffer[0][0],Iy_buffer[0][1],Iy_buffer[0][2],Iy_buffer[0][3]);
                    printf("Height: %d\n",abs((short)Iy_buffer[0][0] - (short)Iy_buffer[0][1]));
                    break;
                case 3000:
                    printf("\n");
                    printf("C1 Height Variance Issue: 3 Blob \n");
                    printf("C1 has %d blobs\n",getBlobsSeen(0));
                    printf("X1: %d, X2: %d, X3: %d, X4: %d\n",Ix_buffer[0][0],Ix_buffer[0][1],Ix_buffer[0][2],Ix_buffer[0][3]);
                    printf("Y1: %d, Y2: %d, Y3: %d, Y4: %d\n",Iy_buffer[0][0],Iy_buffer[0][1],Iy_buffer[0][2],Iy_buffer[0][3]);
                    printf("Height1: %d\n",(Iy_buffer[0][0] - Iy_buffer[0][1]));
                    printf("Height2: %d\n",(Iy_buffer[0][1] - Iy_buffer[0][2]));
                    break;
                case 4000:
                    printf("\n");
                    printf("C1 Height Variance Issue: 4 Blob \n");
                    printf("C1 has %d blobs\n",getBlobsSeen(0));
                    printf("X1: %d, X2: %d, X3: %d, X4: %d\n",Ix_buffer[0][0],Ix_buffer[0][1],Ix_buffer[0][2],Ix_buffer[0][3]);
                    printf("Y1: %d, Y2: %d, Y3: %d, Y4: %d\n",Iy_buffer[0][0],Iy_buffer[0][1],Iy_buffer[0][2],Iy_buffer[0][3]);
                    printf("Height1: %d\n",(Iy_buffer[0][0] - Iy_buffer[0][1]));
                    printf("Height2: %d\n",(Iy_buffer[0][1] - Iy_buffer[0][2]));
                    printf("Height3: %d\n",(Iy_buffer[0][2] - Iy_buffer[0][3]));
                    break;
            }
            switch(result2)
            {
                case 1000:
                    printf("C2 Height Variance Issue: 1 Blob \n");
                    break;
                case 2000:
                    printf("\n");
                    printf("C2 Height Variance Issue: 2 Blob \n");
                    printf("C2 has %d blobs\n",getBlobsSeen(1));
                    printf("X1: %d, X2: %d, X3: %d, X4: %d\n",Ix_buffer[1][0],Ix_buffer[1][1],Ix_buffer[1][2],Ix_buffer[1][3]);
                    printf("Y1: %d, Y2: %d, Y3: %d, Y4: %d\n",Iy_buffer[1][0],Iy_buffer[1][1],Iy_buffer[1][2],Iy_buffer[1][3]);
                    printf("Height1: %d\n",abs((short)Iy_buffer[1][0] - (short)Iy_buffer[1][1]));
                    break;
                case 3000:
                    printf("\n");
                    printf("C2 Height Variance Issue: 3 Blob \n");
                    printf("C2 has %d blobs\n",getBlobsSeen(1));
                    printf("X1: %d, X2: %d, X3: %d, X4: %d\n",Ix_buffer[1][0],Ix_buffer[1][1],Ix_buffer[1][2],Ix_buffer[1][3]);
                    printf("Y1: %d, Y2: %d, Y3: %d, Y4: %d\n",Iy_buffer[1][0],Iy_buffer[1][1],Iy_buffer[1][2],Iy_buffer[1][3]);
                    printf("Height1: %d\n",(Iy_buffer[1][0] - Iy_buffer[1][1]));
                    printf("Height2: %d\n",(Iy_buffer[1][1] - Iy_buffer[1][2]));
                    break;
                case 4000:
                    printf("\n");
                    printf("C2 Height Variance Issue: 4 Blob \n");
                    printf("C2 has %d blobs\n",getBlobsSeen(1));
                    printf("X1: %d, X2: %d, X3: %d, X4: %d\n",Ix_buffer[1][0],Ix_buffer[1][1],Ix_buffer[1][2],Ix_buffer[1][3]);
                    printf("Y1: %d, Y2: %d, Y3: %d, Y4: %d\n",Iy_buffer[1][0],Iy_buffer[1][1],Iy_buffer[1][2],Iy_buffer[1][3]);
                    printf("Height1: %d\n",(Iy_buffer[1][0] - Iy_buffer[1][1]));
                    printf("Height2: %d\n",(Iy_buffer[1][1] - Iy_buffer[1][2]));
                    printf("Height3: %d\n",(Iy_buffer[1][2] - Iy_buffer[1][3]));
                    break;
            }
#endif
            //Set the angle to 90 when the external switch is switched (DEBUG/CALIBRATE 90)
            if(PORTBbits.RB2)
            {
                //Set the angles to what we believe to be 90 degrees
                changeAngle1(90.0);
                changeAngle2(90.0);
                //Indicate with LED that switch is on
                LATBbits.LATB9 = 0;
            }
            else
            {
                //Indicate that the system is operating normally
                LATBbits.LATB9 = 1;
                //Search for the beacons with the Wii cameras
                seekBeacon(CAMERA1, result1);
                seekBeacon(CAMERA2, result2);
            }
        } 
    }    
}

static enum {
    EXCEP_IRQ = 0, // interrupt
    EXCEP_AdEL = 4, // address error exception (load or ifetch)
    EXCEP_AdES, // address error exception (store)
    EXCEP_IBE, // bus error (ifetch)
    EXCEP_DBE, // bus error (load/store)
    EXCEP_Sys, // syscall
    EXCEP_Bp, // breakpoint
    EXCEP_RI, // reserved instruction
    EXCEP_CpU, // coprocessor unusable
    EXCEP_Overflow, // arithmetic overflow
    EXCEP_Trap, // trap (possible divide by zero)
    EXCEP_IS1 = 16, // implementation specfic 1
    EXCEP_CEU, // CorExtend Unuseable
    EXCEP_C2E // coprocessor 2
} _excep_code;
static unsigned int _epc_code;
static unsigned int _excep_addr;
// this function overrides the normal _weak_ generic handler

void _general_exception_handler(void) {
    asm volatile("mfc0 %0,$13" : "=r" (_excep_code));
    asm volatile("mfc0 %0,$14" : "=r" (_excep_addr));
    _excep_code = (_excep_code & 0x0000007C) >> 2;
    LATBbits.LATB10 = 0; //test 1
    while (1) {

        // Examine _excep_code to identify the type of exception
        // Examine _excep_addr to find the address that caused the exception
    }
}
