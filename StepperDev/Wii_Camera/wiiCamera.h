/* 
 * File:   wiiCamera.h
 * Author: Dana
 *
 * Created on May 12, 2016, 8:42 PM
 */


#include <stdbool.h>


#ifndef WIICAMERA_H
#define	WIICAMERA_H

#ifdef	__cplusplus
extern "C" {
#endif

// bit flags for blobs
#define BLOB1 0x01
#define BLOB2 0x02
#define BLOB3 0x04
#define BLOB4 0x08
#define HEIGHT_VARIANCE 50  //25  //50  //100
#define MAX_Y   900
#define MIN_Y   75

#define FAILED_I2C 2
#define SUCCESS_I2C 1


struct DataBuffer {
    unsigned char data_buffer[16];
};

  struct DataBuffer data_buf[2];

  unsigned char wiiInitStep;
  bool pendingTrans1;
  bool pendingTrans2;

  unsigned short result2, result1;

  unsigned int camera;
  unsigned int data_address;

  unsigned char data[2];
  unsigned short Ix_buffer[2][4];
  unsigned short Iy_buffer[2][4];
  unsigned short average[2];
  unsigned char blobsSeen[2];
  unsigned short xBlob[2][2];


  void SortInput(unsigned char cameraValue, unsigned char loc);
  unsigned short PickBeacon(unsigned char cameraValue);
  unsigned char getBlobsSeen(unsigned char cameraValue);
  void read(unsigned char cameraValue);
  void Sort(unsigned char cameraValue);
  short getDifferenceX(unsigned char cameraValue);


#ifdef	__cplusplus
}
#endif

#endif	/* WIICAMERA_H */

