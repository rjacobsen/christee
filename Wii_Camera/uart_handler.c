#include <xc.h>
#include <stdbool.h>
#include <sys/attribs.h>
#include "uart_handler.h"



bool Transmit_stall = true;
bool Transmit_stall6 = true;



//Called when Printf() is used
void _mon_putc(char c)
{
    UART_buff_put(&output_buffer6, c);
    if((Transmit_stall6 == true) && (UART_buff_size(&output_buffer6) > 2))
    {
        Transmit_stall6 = false;
        IEC2bits.U6TXIE = 1;
    }
}



void UART_buff_init(struct UART_ring_buff* _this)
{
    /*****
      The following clears:
        -> buf
        -> head
        -> tail
        -> count
      and sets head = tail
     ***/
    memset(_this, 0, sizeof (*_this));
}

void UART_buff_put(struct UART_ring_buff* _this, const unsigned char c)
{
    if (_this->count < UART_BUFFER_SIZE)
    {
        _this->buf[_this->head] = c;
        _this->head = UART_buff_modulo_inc(_this->head, UART_BUFFER_SIZE);
        ++_this->count;
    } else
    {
        _this->buf[_this->head] = c;
        _this->head = UART_buff_modulo_inc(_this->head, UART_BUFFER_SIZE);
        _this->tail = UART_buff_modulo_inc(_this->tail, UART_BUFFER_SIZE);

    }
}

unsigned char UART_buff_get(struct UART_ring_buff* _this)
{
    unsigned char c;
    if (_this->count > 0)
    {
        c = _this->buf[_this->tail];
        _this->tail = UART_buff_modulo_inc(_this->tail, UART_BUFFER_SIZE);
        --_this->count;
    } else
    {
        c = 0;
    }
    return (c);
}

void UART_buff_flush(struct UART_ring_buff* _this, const int clearBuffer)
{
    _this->count = 0;
    _this->head = 0;
    _this->tail = 0;
    if (clearBuffer)
    {
        memset(_this->buf, 0, sizeof (_this->buf));
    }
}

int UART_buff_size(struct UART_ring_buff* _this)
{
    return (_this->count);
}

unsigned int UART_buff_modulo_inc(const unsigned int value, const unsigned int modulus)
{
    unsigned int my_value = value + 1;
    if (my_value >= modulus)
    {
        my_value = 0;
    }
    return (my_value);
}

unsigned char UART_buff_peek(struct UART_ring_buff* _this)
{
    return _this->buf[_this->tail];
}

unsigned char Receive_peek(void)
{
    return UART_buff_peek(&input_buffer);
}

int Receive_available(void)
{
    return UART_buff_size(&input_buffer);
}

unsigned char Receive_get(void)
{
    return UART_buff_get(&input_buffer);
}

void Send_put(unsigned char _data)
{
    UART_buff_put(&output_buffer, _data);
    if((Transmit_stall == true) )//&& (UART_buff_size(&output_buffer) > 5))
    {
        Transmit_stall = false;
        IEC2bits.U5TXIE = 1;
    }
}

//void __attribute__((interrupt, no_auto_psv)) _U1RXInterrupt(void) //<----- DO NOT USE THIS CALL ... IT IS FOR dsPIC33  16bit
//{
//
//    //INDICATOR1=ON;
//    unsigned char data = U1RXREG;
//    UART_buff_put(&input_buffer, data);
//    IFS0bits.U1RXIF = 0; // Clear RX interrupt flag
//    //INDICATOR1=OFF;
//}
void __ISR(_UART_6_VECTOR, IPL1AUTO) _U6TXInterrupt(void)
{
        if (UART_buff_size(&output_buffer6) > 0)
        {
            U6TXREG = UART_buff_get(&output_buffer6);
        }
        else
        {
            Transmit_stall6 = true;
            IEC2bits.U6TXIE = 0;
        }
    IFS2CLR = _IFS2_U6TXIF_MASK;
}

void __ISR(_UART_5_VECTOR, IPL1AUTO) _U5TXInterrupt(void)
{

         //LATBbits.LATB10^=1;
        if (UART_buff_size(&output_buffer) > 0)
        {
            U5TXREG = UART_buff_get(&output_buffer);
        }
        else
        {
            Transmit_stall = true;
            IEC2bits.U5TXIE = 0;
        }
         //INDICATOR2=OFF;

   // IFS2bits.U6TXIF = 0; // Clear TX interrupt flag
    IFS2CLR = _IFS2_U5TXIF_MASK;
}
